$(document).ready(function(){
    $('body').on('click', '.password-checkbox', function(){
        if ($(this).is(':checked')){
            $('#password-input').attr('type', 'text');
        } else {
            $('#password-input').attr('type', 'password');
        }
    });


    if ($('#f_status_nesvaska').val() == 's1') {
        $("#reformat_print_nesvaska_request").prop("disabled", false);
        $("#save_edit_nesvaska_request").prop("disabled", false);

    }
    else {
        $("#reformat_print_nesvaska_request").prop("disabled", true);
        $("#save_edit_nesvaska_request").prop("disabled", true);
    }

    if ($('#removed_condition').val() == 'True') {
        $("#deny-click-request-report").hide();
        $("#send_report_request").hide();
    }

    $('#information_nesvaska').change(function (e){
	    e.preventDefault();
        if ((Number.parseFloat($('#cereals').val()) == 0) &&
            (Number.parseFloat($('#oilseeds').val()) == 0) &&
            (Number.parseFloat($('#potato').val()) == 0) &&
            (Number.parseFloat($('#vegetables').val()) == 0)) {
           $("#save_nesvaska_report").prop("disabled", true);
        }
        else {
            $("#save_nesvaska_report").prop("disabled", false);
        }
	})

    if ((Number.parseFloat($('#cereals').val()) == 0) &&
        (Number.parseFloat($('#oilseeds').val()) == 0) &&
        (Number.parseFloat($('#potato').val()) == 0) &&
        (Number.parseFloat($('#vegetables').val()) == 0)) {
       $("#save_nesvaska_report").prop("disabled", true);
    }
    else {
        $("#save_nesvaska_report").prop("disabled", false);
    }

    $('#data_start_recive_region').val()

    if ($('#status_request').val() == 'Новая') {
         if ($('#deadline_admission').val() == 'possible') {
            if ($('#ability_to_send').val() == 'False') {
                $("#send_request").prop("disabled", true);
                $("#deadline_label").hide();
            }
            else {
                $("#send_request").prop("disabled", false);
                $("#deadline_label").hide();
            }
         }
         else {
            $("#send_request").prop("disabled", true);
            $("#deadline_label").show();
         }
    }

    if ($('#status_request_report').val() == 'Новая' && $('#removed_condition').val() == "True") {
         if ($('#deadline_admission_report').val() == 'possible') {
            $("#send_report_request").prop("disabled", false);
            $("#deadline_label_request_report").hide();
         }
         else {
            $("#send_report_request").prop("disabled", true);
            $("#deadline_label_request_report").show();
         }
    }

    if ($('#status_report').val() != 'sr1') {
        $("#resave_nesvaska_report").prop("disabled", true);
        $("#save_nesvaska_report").prop("disabled", true);
    }

    if ($('#status_request_report').val() == 'Новая') {
         if ($('#deadline_admission_report').val() == 'possible') {
            $("#send_report_request").prop("disabled", false);
            $("#deadline_label_request_report").hide();
         }
         else {
            $("#send_report_request").prop("disabled", true);
            $("#deadline_label_request_report").show();
         }
    }

    if ($('#status_request_report').val() == 'Отказано') {
         if ($('#deadline_admission_report').val() == 'possible') {
            $("#send_report_request").prop("disabled", false);
            $("#deadline_label_request_report").hide();
         }
         else {
            $("#send_report_request").prop("disabled", true);
            $("#deadline_label_request_report").show();
         }
    }


    if ($('#deadline_admission').val() == 'possible') {
        $("#deadline_label").hide();
    }
        if ($('#deadline_admission_report').val() == 'possible') {
        $("#deadline_label_request_report").hide();
    }


    if ($('#status_request').val() != 's1') {
        $("#calc_subs").prop("disabled", true);
        $("#clear_fieild").prop("disabled", true);
        $("#save_edit_eight_request").prop("disabled", true);
        $("#reformat_print_eight_request").prop("disabled", true);
        $("#delete_registry").prop("disabled", true);
        $("#docfile").prop("disabled", true);
    }
    else {
        $("#calc_subs").prop("disabled", false);
        $("#clear_fieild").prop("disabled", false);
        $("#save_edit_eight_request").prop("disabled", false);
        $("#reformat_print_eight_request").prop("disabled", false);
        $("#delete_registry").prop("disabled", false);
        $("#docfile").prop("disabled", false);
    }


    if (($('#summ_total').val() == "0.00" || $('#path_registry').val() == "/media/" || (Number.parseFloat($('#registry_excel_volume_milk').val()) != (Number.parseFloat($('#amount_milk_cows').val()) + Number.parseFloat($('#amount_milk_goats').val())))) || $('#status_request').val() != 's1') {
        $("#show_request").hide();
    }
    else {
        $("#show_request").show();
    }

    if ($('#first_in_group').val() == "False") {
        $("#statement").hide()
    }

    if ($('#content_label_sort_ssrs').val() == "По возрастанию") {
        console.log("checked");
        $("#sort_ssrs").prop("checked");
    }

    $('table#ssrs_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_ssrs').val($td.parents("tr").children("td:first").text());
    })

    if ($('#content_label_sort_ssrs').val() == "По убыванию") {
        $("#sort_ssrs").removeAttr("checked");
    }

    $('#filter_article option:contains('+$('#selected_article').val()+')').prop('selected', true);
    $('#filter_status option:contains('+$('#selected_status').val()+')').prop('selected', true);

    if ($('#eight_ssr_f_status').val() == "Принято") {
        $("#edit_ssr_approve").prop("disabled", true);
        $("#eight_ssr_save_changes").prop("disabled", true);
    }

    if ($('#nesvaska_ssr_f_status').val() == "Принято") {
        $("#nesvaska_ssr_approve").prop("disabled", true);
        $("#nesvaska_ssr_save_changes").prop("disabled", true);
    }

    if ($('#status_request').val() == "Выплачено") {
        $("#send_request").prop("disabled", true);
    }

    if ($('#status_request_report').val() == "Принято") {
        $("#send_report_request").prop("disabled", true);
    }

    if (($('#status_request').val() != "Новая") && ($('#status_request').val() != "Отказано") && ($('#status_request').val() != "Выплачено")) {
        $('.deny-click').show()
    }
    else {
        $('.deny-click').hide()
    }

    if (($('#status_request_report').val() != "Новая") && ($('#status_request_report').val() != "Отказано") && ($('#status_request_report').val() != "Принято")  && $('#removed_condition').val() != "True") {
        $('.deny-click-request-report').show()
    }
    else {
        $('.deny-click-request-report').hide()
    }

    $('.label_deny').hide()
    $('.textarea_deny').hide()
    $('.save_deny_request').hide()

    $('#f_period option:contains('+$('#f_period_1').val()+')').prop('selected', true);
    $('#combo_period option:contains('+$('#f_name_period_1').val()+')').prop('selected', true);
    $('#f_reg_area option:contains('+$('#f_reg_area_1').val()+')').prop('selected', true);
    $('#f_bank_name option:contains('+$('#bik_selected_bank').val()+' '+$('#name_selected_bank').val()+')').prop('selected', true);
    $('#f_name_type_org option:contains('+$('#f_name_type_org_1').val()+')').prop('selected', true);
    $('#f_type_user option:contains('+$('#f_type_user_1').val()+')').prop('selected', true);

//    $("#phone").mask("8(999) 999-9999");

    $('table#registration_area_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_registration_area').val($td.parents("tr").children("td:first").text());
    })

    $('table#periodicities_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_periodicity').val($td.parents("tr").children("td:first").text());
    })

    $('table#banks_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td').val($td.parents("tr").children("td:first").text());
    })

    $('table#users_system_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_user_system').val($td.parents("tr").children("td:first").text());
    })

    $('table#organizations_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_organization').val($td.parents("tr").children("td:first").text());
    })

    $('table#articles_law_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_articles_law').val($td.parents("tr").children("td:first").text());
    })

    $('table#reporting_periods_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_reporting_period').val($td.parents("tr").children("td:first").text());
    })

    $('table#type_organization_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_type_organization').val($td.parents("tr").children("td:first").text());
    })

     $('table#reporting_period_table').on('click', 'td', function (e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        $('#selected_td_report_period').val($td.parents("tr").children("td:first").text());
    })

     $('.dropdown-item#dropdown_report_period').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_report_period/"+selected_td;
    })

     $('.dropdown-item#dropdown_edit_ssr').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_ssr/"+selected_td;
    })

     $('.dropdown-item#dropdown_show_ssr').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="show_ssr/"+selected_td;
    })

    $('.dropdown-item#dropdown_edit_registration_area').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_registration_area/"+selected_td;
    })

    $('.dropdown-item#dropdown_edit_type_organization').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_type_organization/"+selected_td;
    })

    $('.dropdown-item#dropdown_show_selected_organization').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="show_selected_organization/"+selected_td;
    })

    $('.dropdown-item#dropdown_edit_organization').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_organization/"+selected_td;
    })

    $('.dropdown-item#dropdown_user_system').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_user_system/"+selected_td;
    })

    $('.dropdown-item#dropdown_periodicity').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_periodicity/"+selected_td;
    })

    $('.dropdown-item#dropdown_edit_bank').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_bank/"+selected_td;
    })

    $('.dropdown-item#dropdown_edit_article_law').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_article_law/"+selected_td;
    })

    $('.dropdown-item#dropdown_reporting_period').click(function(e) {
        e.preventDefault();
        var $td = $(this),
        $tr = $td.parent();
        selected_td = $td.parents("tr").children("td:first").text();
        location.href="edit_reporting_period/"+selected_td;
    })

     //добавление строк в таблице специалистов
    jQuery('.plus').click(function(){
        float_count_1 =  Number.parseFloat($('#count_1').val());
        string_count_1 = (float_count_1 + 1).toString();
        $('#count_1').val(string_count_1);
        jQuery('.information_json_plus').before(
        '<tr>' +
            '<td><input type="text" class="form-control"' + 'name = "'+ string_count_1 +'_1"'+'></td>' +
            '<td><input type="number" class="form-control" step="1" min="0" value="0"' + 'name = "'+ string_count_1 +'_2"'+'></td>' +
            '<td><input type="text" class="form-control" step="1" min="0" value="0"' + 'name = "'+ string_count_1 +'_3"'+'></td>' +
            '<td><input type="text" class="form-control" step="1" min="0" value="0"' + 'name = "'+ string_count_1 +'_4"'+'></td>' +
            '<td><input type="text" class="form-control" step="1" min="0" value="0"' + 'name = "'+ string_count_1 +'_5"'+'></td>' +
            '<td><span class="btn btn-danger minus pull-right">&ndash;</span></td>' +
        '</tr>'
        );
    });

    jQuery(document).on('click', '.minus', function(){
        float_count_1 =  Number.parseFloat($('#count_1').val());
        string_count_1 = (float_count_1 - 1).toString();
        $('#count_1').val(string_count_1);
        jQuery( this ).closest( 'tr' ).remove();
    });


    if ($('#quarantine').val()=="Установлен") {
        $("#label_quarantine_establish_1").show()
        $("#data_quarantine_establish_1").show()

        $("#label_quarantine_establish_2").show()
        $("#data_quarantine_establish_2").show()
    }
    else {
        $("#label_quarantine_establish_1").hide()
        $("#data_quarantine_establish_1").hide()

        $("#label_quarantine_establish_2").hide()
        $("#data_quarantine_establish_2").hide()
    }

    $('#quarantine').change(function (e){
	    e.preventDefault();
	    if ($('#quarantine').val()=="Установлен") {
	        $("#label_quarantine_establish_1").show()
	        $("#data_quarantine_establish_1").show()

	        $("#label_quarantine_establish_2").show()
	        $("#data_quarantine_establish_2").show()
	    }
	    else {
	        $("#label_quarantine_establish_1").hide()
	        $("#data_quarantine_establish_1").hide()

	        $("#label_quarantine_establish_2").hide()
	        $("#data_quarantine_establish_2").hide()
	    }
	 })

    //Очистить поля в блоке расчета субсидии 8
	 $('.clear_fieild').click(function (e) {
        e.preventDefault();
        $('#num_total').val("0");
        $('#num_begin_year_cows').val("0");
        $('#num_begin_month_cows').val("0");
        $('#amount_milk_cows').val("0.000");
        $('#num_begin_year_goats').val("0");
        $('#amount_milk_goats').val("0.000");
        $('#summ_cows').val("0.00");
        $('#summ_goats').val("0.00");
        $('#summ_total').val("0.00");
        $('#data_quarantine_establish_1').val("");
        $('#data_quarantine_establish_2').val("");

        $('.error_1').remove();
        $('.error_2').remove();
        $('.error_3').remove();
        $('.error_4').remove();
        $('.error_5').remove();

        $("#send_request_eight").prop("disabled", true);
    })


    $('.calc_subs').click(function (e) {
        e.preventDefault();
        $('#summ_cows').val("0.00");
        $('#summ_goats').val("0.00");
        $('#summ_total').val("0.00");
        var check_result = check_form();
        if (check_result == 0) {
            calc_subs_amount();
        }

        total_volume_milk = Number.parseFloat($('#amount_milk_cows').val()) + Number.parseFloat($('#amount_milk_goats').val());
        console.log(total_volume_milk)
        console.log($('#registry_excel_volume_milk').val())

        if (($('#summ_total').val() == "0.00" || $('#path_registry').val() == "/media/" || (Number.parseFloat($('#registry_excel_volume_milk').val()) != total_volume_milk)) || $('#status_request').val() != 's1') {
            $("#show_request").prop("disabled", true);

        }
        else {
            $("#show_request").prop("disabled", false);
        }
    })

    //расчет размера субсидии
    function calc_subs_amount() {
        var stavka_cows = Number.parseFloat($('#rate_cows').val())
        var stavka_goats = Number.parseFloat($('#rate_goats').val())

        var amount_milk_cows_num = Number.parseFloat($('#amount_milk_cows').val())
        var amount_milk_goats_num = Number.parseFloat($('#amount_milk_goats').val())

        var summ_milk_cows = stavka_cows * amount_milk_cows_num
        var summ_milk_goats = stavka_goats * amount_milk_goats_num

        var summ_milk_cows_fix = summ_milk_cows.toFixed(2);
        var summ_milk_goats_fix = summ_milk_goats.toFixed(2);

        summ_total = Number.parseFloat(summ_milk_cows_fix) + Number.parseFloat(summ_milk_goats_fix)
        summ_total_fix = summ_total.toFixed(2);
        $('#summ_cows').val(summ_milk_cows_fix);
        $('#summ_goats').val(summ_milk_goats_fix);
        $('#summ_total').val(summ_total_fix);
    }

    function check_form() {
        $('.error_1').remove();
        $('.error_2').remove();
        $('.error_3').remove();
        $('.error_4').remove();
        $('.error_5').remove();

        $("#number_of_livestock_warning").remove();
        $("#num_goals_warning").remove();
        $("#date_quarantine_warning_2").remove();
        $("#date_quarantine_warning_1").remove();
        $("#number_of_livestock_warning").remove();
        $("#registry_warning").remove();
        $("#volume_milk_warning").remove();
        $("#succses_warning").remove();
        $("#point_3_warning").remove();
        $("#point_4_warning").remove();
        $("#point_5_warning").remove();

        var count_error = 0
        var count_error_1 = 0

        if ($('#point_3').val() == "") {
            jQuery('.table_warning').after(
                '<tr id="point_3_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не выбрали значения для 3 пункта заявления!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $("#point_3_warning").remove();
        }

        if ($('#point_4').val() == "") {
            jQuery('.table_warning').after(
                '<tr id="point_4_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не выбрали значения для 4 пункта заявления!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1
        }
        else {
            $("#point_4_warning").remove();
        }

        if ($('#point_5').val() == "") {
            jQuery('.table_warning').after(
                '<tr id="point_5_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не выбрали значения для 5 пункта заявления!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $("#point_5_warning").remove();
        }

        total_volume_milk = Number.parseFloat($('#amount_milk_cows').val()) + Number.parseFloat($('#amount_milk_goats').val());

//        if (round(Number.parseFloat($('#registry_excel_volume_milk').val()) != total_volume_milk && $('#path_registry').val() != "/media/")) {
//            console.log(Number.parseFloat($('#registry_excel_volume_milk').val()));
//            console.log($('#path_registry').val() != "/media/");
//            jQuery('.table_warning').after(
//                '<tr id="volume_milk_warning">' +
//                    '<td class="bg-danger" style="color: #fff;">Введенный объем молока не совпадает с рассчитанным объемом в реестре!</td>' +
//                '</tr>'
//            );
//            count_error_1 = count_error_1 + 1;
//        }
//        else {
//            $("#volume_milk_warning").remove();
//        }

        if ($('#path_registry').val() == "/media/") {
            jQuery('.table_warning').after(
                '<tr id="registry_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Не подгружен реестр документов, подтверждающих реализацию и (или) отгрузку молока!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $("#registry_warning").remove();
        }

        if (Number.parseFloat($('#num_begin_year_cows').val())> Number.parseFloat($('#num_begin_month_cows').val())) {
            $('#num_begin_year_cows').after('<span class="error_1">Количество голов снижено!</span>');
            count_error = count_error + 1;
            jQuery('.table_warning').after(
                '<tr id="num_goals_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Количество голов снижено!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $('.error_1').remove();
            $("#num_goals_warning").remove();
        }

        if (Number.parseFloat($('#num_total').val())==0 || ($('#num_total').val())=="") {
            $('#num_total').after('<p><span class="error_2">Вы не указали значение!</span></p>');
            count_error = count_error + 1;
            jQuery('.table_warning').after(
                '<tr id="number_of_livestock_warning">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не указали общее количество скота!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $('.error_2').remove();
            $("#number_of_livestock_warning").remove();
        }

        if ($('#data_quarantine_establish_1').val()=="" && $('#quarantine').val()=="Установлен") {
             $('#data_quarantine_establish_1').after('<p><span class="error_3">Вы не выбрали дату карантина!</span></p>');
            count_error = count_error + 1;
            jQuery('.table_warning').after(
                '<tr id="date_quarantine_warning_1">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не выбрали первую дату карантина!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $("#date_quarantine_warning_1").remove();
            $('.error_3').remove();
        }

        if ($('#data_quarantine_establish_2').val()=="" && $('#quarantine').val()=="Установлен") {
            $('#data_quarantine_establish_2').after('<p><span class="error_4">Вы не выбрали дату карантина</span></p>');
            count_error = count_error + 1;
            jQuery('.table_warning').after(
                '<tr id="date_quarantine_warning_2">' +
                    '<td class="bg-danger" style="color: #fff;">Вы не выбрали вторую дату карантина!</td>' +
                '</tr>'
            );
            count_error_1 = count_error_1 + 1;
        }
        else {
            $("#date_quarantine_warning_2").remove();
            $('.error_4').remove();
        }
        if (count_error > 0) {
            $("#comments").show();
        }
        if (count_error_1 == 0){
            jQuery('.table_warning').after(
                '<tr id="succses_warning">' +
                    '<td class="bg-success" style="color: #fff;">Замечаний нет!</td>' +
                '</tr>'
            );
        }
        return(count_error)
    }

    $('.block_warning').on('click', '.extremum-click', function() {
		$('.extremum-slide').slideToggle(800);
	});

    $('.deny-click').click(function(e) {
        e.preventDefault();
        $('.deny-slide').slideToggle(800);
        $('.save_deny_request').show()
    })

	$(".deny-slide").hide();
	$(".extremum-slide").hide();


    $('.deny-click-request-report').click(function(e) {
        e.preventDefault();
        $('.deny-slid-request-report').slideToggle(800);
        $('.save_deny_request_report').show()
    })

    $(".deny-slid-request-report").hide();
})




