from project.resources import *
from django.db.models import Q, Sum, F
import datetime
import openpyxl
from datetime import *
from openpyxl.styles import Border, Alignment, Font, Side
from openpyxl.utils import get_column_letter

def get_cumlve_infm_eighth_ssr(list_requests_accrued, list_requests_paid):
    amount_milk_cows = amount_milk_goats = 0
    rate_cows = rate_goats = 0
    sum_accrued_cows = sum_accrued_goats = 0
    sum_paid_cows = sum_paid_goats = 0

    for item in list_requests_accrued:
        obj_eight_subs = Requests_Eight_Subs.objects.get(f_requests=item.id)
        amount_milk_cows = round(amount_milk_cows + obj_eight_subs.amount_milk_cows, 3)
        amount_milk_goats = round(amount_milk_goats + obj_eight_subs.amount_milk_goats, 3)
        rate_cows = obj_eight_subs.rate_cows
        rate_goats = obj_eight_subs.rate_goats
        sum_accrued_cows = round(sum_accrued_cows + obj_eight_subs.sum_cows, 2)
        sum_accrued_goats = round(sum_accrued_goats + obj_eight_subs.sum_goats, 2)

    for item in list_requests_paid:
        obj_eight_subs_paid = Requests_Eight_Subs.objects.get(f_requests=item.id)
        sum_paid_cows = round(sum_paid_cows + obj_eight_subs_paid.sum_cows, 2)
        sum_paid_goats = round(sum_paid_goats + obj_eight_subs_paid.sum_goats, 2)

    return amount_milk_cows, amount_milk_goats, rate_cows, rate_goats, sum_accrued_cows, sum_accrued_goats, sum_paid_cows, sum_paid_goats

# если first_time = True, значит ССР формируется на основании статусов заявок
def create_ssr_eight_subs(first_time, obj_list_name_sxp, num_article, select_year_article, ssr_model_item,
                          eight_ssr_previous, eight_ssr_current):
    if first_time == True:
        for item in obj_list_name_sxp:
            ssr_item_eight_subs = SSR_Eight_Subs()
            obj_list_requests_accrued = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                                num_subsid=int(num_article),
                                                                report_year_subsid=int(select_year_article),
                                                                f_organization__full_name_org=item)

            obj_list_requests_paid = Requests.objects.filter(Q(f_status='s7'),
                                                             num_subsid=int(num_article),
                                                             report_year_subsid=int(select_year_article),
                                                             f_organization__full_name_org=item)

            ssr_item_eight_subs.f_ssr = ssr_model_item
            ssr_item_eight_subs.name_region = obj_list_requests_accrued[0].f_organization.f_reg_area.name_area
            ssr_item_eight_subs.name_organization = item
            ssr_item_eight_subs.type_milk_1 = "Коровье"
            ssr_item_eight_subs.type_milk_2 = "Козье"

            cumulative_inform = get_cumlve_infm_eighth_ssr(obj_list_requests_accrued, obj_list_requests_paid)

            ssr_item_eight_subs.amount_milk_cows = cumulative_inform[0]
            ssr_item_eight_subs.amount_milk_goats = cumulative_inform[1]
            ssr_item_eight_subs.amount_milk = cumulative_inform[0] + cumulative_inform[1]

            ssr_item_eight_subs.rate_cows = cumulative_inform[2]
            ssr_item_eight_subs.rate_goats = cumulative_inform[3]

            ssr_item_eight_subs.sum_accrued_cows = cumulative_inform[4]
            ssr_item_eight_subs.sum_accrued_goats = cumulative_inform[5]
            ssr_item_eight_subs.sum_accrued = cumulative_inform[4] + cumulative_inform[5]

            ssr_item_eight_subs.sum_paid_cows = cumulative_inform[6]
            ssr_item_eight_subs.sum_paid_goats = cumulative_inform[7]
            ssr_item_eight_subs.sum_paid = cumulative_inform[6] + cumulative_inform[7]

            ssr_item_eight_subs.sum_report_cows = cumulative_inform[4] - cumulative_inform[6]
            ssr_item_eight_subs.sum_report_goats = cumulative_inform[5] - cumulative_inform[7]
            ssr_item_eight_subs.sum_report = ssr_item_eight_subs.sum_report_cows + ssr_item_eight_subs.sum_report_goats
            ssr_item_eight_subs.save()
    else:
        i = 1
        ssr_eight_subs_last_id = SSR_Eight_Subs.objects.all().last().id
        for item in eight_ssr_previous:
            ssr_eight_model_item = SSR_Eight_Subs()
            ssr_eight_model_item = item
            ssr_eight_model_item.id = ssr_eight_subs_last_id + i
            i = i + 1
            ssr_eight_model_item.f_ssr = eight_ssr_current
            ssr_eight_model_item.save()

def get_total_sum(model_item):
    amount_milk_cows = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('amount_milk_cows')))
    amount_milk_goats = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('amount_milk_goats')))

    sum_accrued_cows = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_accrued_cows')))
    sum_accrued_goats = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_accrued_goats')))

    sum_paid_cows = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_paid_cows')))
    sum_paid_goats = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_paid_goats')))

    sum_report_cows = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_report_cows')))
    sum_report_goats = SSR_Eight_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_report_goats')))

    total_amount_milk = amount_milk_cows.get('amount_milk_cows__sum') + amount_milk_goats.get('amount_milk_goats__sum')
    total_sum_accrued = sum_accrued_cows.get('sum_accrued_cows__sum') + sum_accrued_goats.get('sum_accrued_goats__sum')
    total_sum_paid = sum_paid_cows.get('sum_paid_cows__sum') + sum_paid_goats.get('sum_paid_goats__sum')
    total_sum_report = sum_report_cows.get('sum_report_cows__sum') + sum_report_goats.get('sum_report_goats__sum')

    return total_amount_milk, total_sum_accrued, total_sum_paid, total_sum_report

def save_excel_eighth_ssr(ssr_model_item):
    start_item_row_table = 5
    str_document_path = "/ssrs/" + "сводная справка - расчет_8_" + str(ssr_model_item.id) + ".xlsx"

    total_sum = get_total_sum(ssr_model_item)

    obj_eight_ssr = SSR_Eight_Subs.objects.filter(f_ssr=ssr_model_item.id).order_by("name_region")
    save_eighth_ssr_to_excel(start_item_row_table,
                             obj_eight_ssr, ssr_model_item, total_sum[0], total_sum[1],
                             total_sum[2], total_sum[3], str_document_path)
    ssr_model_item.excel_document_path = str_document_path
    ssr_model_item.save()

def to_paid_eighth_subs_ssr(eight_subs_ssr_current):
    for item in eight_subs_ssr_current:
        item.sum_paid_cows = item.sum_accrued_cows
        item.sum_paid_goats = item.sum_accrued_goats
        item.sum_report_cows = item.sum_report_goats = 0
        item.sum_paid = item.sum_accrued
        item.sum_report = 0
        item.save()

def rewrite_eighth_subs_ssr(eight_ssr_current, item_1):
    obj_item_ssr_eight_subs = SSR_Eight_Subs.objects.get(f_ssr=eight_ssr_current.id, name_organization = item_1.f_organization.full_name_org)
    obj_item_requests_eight_subs = Requests_Eight_Subs.objects.get(f_requests = item_1.id)
    obj_item_ssr_eight_subs.amount_milk_cows = obj_item_ssr_eight_subs.amount_milk_cows + obj_item_requests_eight_subs.amount_milk_cows
    obj_item_ssr_eight_subs.amount_milk_goats = obj_item_ssr_eight_subs.amount_milk_goats + obj_item_requests_eight_subs.amount_milk_goats
    obj_item_ssr_eight_subs.sum_accrued_cows = obj_item_ssr_eight_subs.sum_accrued_cows + obj_item_requests_eight_subs.sum_cows
    obj_item_ssr_eight_subs.sum_accrued_goats = obj_item_ssr_eight_subs.sum_accrued_goats + obj_item_requests_eight_subs.sum_goats
    obj_item_ssr_eight_subs.sum_report_cows = 0
    obj_item_ssr_eight_subs.sum_report_goats = 0
    obj_item_ssr_eight_subs.amount_milk = obj_item_ssr_eight_subs.amount_milk_cows + obj_item_ssr_eight_subs.amount_milk_goats
    obj_item_ssr_eight_subs.sum_accrued = obj_item_ssr_eight_subs.sum_accrued_cows + obj_item_ssr_eight_subs.sum_accrued_goats
    obj_item_ssr_eight_subs.sum_report = 0
    obj_item_ssr_eight_subs.save()

def create_eighth_subs_ssr(eight_ssr_current, item_1):
    obj_item = SSR_Eight_Subs()
    obj_item.f_ssr = eight_ssr_current
    obj_item.name_region = str(item_1.f_organization.f_reg_area.name_area)
    obj_item.name_organization = str(item_1.f_organization.full_name_org)
    obj_item.type_milk_1 = "Коровье"
    obj_item.type_milk_2 = "Козье"
    obj_item_requests_eight_subs = Requests_Eight_Subs.objects.get(f_requests=int(item_1.id))
    obj_item.amount_milk_cows = obj_item_requests_eight_subs.amount_milk_cows
    obj_item.amount_milk_goats = obj_item_requests_eight_subs.amount_milk_goats
    obj_item.rate_cows = obj_item_requests_eight_subs.rate_cows
    obj_item.rate_goats = obj_item_requests_eight_subs.rate_goats
    obj_item.sum_accrued_cows = obj_item_requests_eight_subs.sum_cows
    obj_item.sum_accrued_goats = obj_item_requests_eight_subs.sum_goats
    obj_item.sum_paid_cows =  obj_item.sum_paid_goats = 0
    obj_item.sum_report_cows = obj_item_requests_eight_subs.sum_cows
    obj_item.sum_report_goats = obj_item_requests_eight_subs.rate_goats
    obj_item.amount_milk = obj_item.amount_milk_cows + obj_item.amount_milk_goats
    obj_item.sum_accrued = obj_item.sum_accrued_cows + obj_item.rate_goats
    obj_item.sum_paid = 0
    obj_item.sum_report = obj_item.sum_report_cows + obj_item.sum_report_goats
    obj_item.save()

def save_eighth_ssr_to_excel(start_item_row_table, obj_eight_ssr, obj_ssr, total_amount_milk,
                             total_sum_accrued, total_sum_paid, total_sum_report,
                             str_document_path):
    my_wb = openpyxl.Workbook()
    my_sheet = my_wb.active
    my_sheet.title = 'Сводная справка - расчет'

    ft = Font(size = 14, name = 'Times New Roman')
    ft_size = Font(size = 10, name = 'Times New Roman')
    ft_b = Font(size = 14, name = 'Times New Roman', bold=True)

    thin_border = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))

    my_sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=8)
    my_sheet.cell(row=1, column=1).font = ft
    my_sheet.row_dimensions[1].height = 80
    c1 = my_sheet.cell(row=1, column=1)
    c1.value = obj_ssr.name_title + " " + obj_ssr.name_period
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')

    item = 1
    for i in range(8):
        my_sheet.cell(row=start_item_row_table - 2, column=item).font = ft
        c1 = my_sheet.cell(row=start_item_row_table - 2, column=item)
        c1.value = obj_ssr.name_title + " " + obj_ssr.name_period
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        item = item + 1

    item = 1
    for i in range(8):
        my_sheet.cell(row=start_item_row_table - 1, column=item).font = ft
        c1 = my_sheet.cell(row=start_item_row_table - 1, column=item)
        c1.value = item
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        item = item + 1

    start_title = start_item_row_table - 2
    end_title = start_title + 1

    while start_title <= end_title:
        start_column = 1
        while start_column <= 8:
            my_sheet.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=1)
    c1.value = "Наименование муниципального района или городского округа"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=2)
    c1.value = "Наименование получателя субсидии"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=3)
    c1.value = "Вид молока"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=4)
    c1.value = "Количество реализовнного молока в физическом весе с нарастающим итогом (тонн)"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=5)
    c1.value = "Ставка субсидирования на одну тонну молока (рублей)"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=6)
    c1.value = "Сумма начисленной субсидии (рублей)"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=7)
    c1.value = "Сумма субсидии выплаченная с начала года (рублей)"

    c1 = my_sheet.cell(row=start_item_row_table - 2, column=8)
    c1.value = "Сумма субсидии причитающаяся к выплате за отчетный период (рублей)"

    start_row_table = start_item_row_table
    i = start_row_table
    offset = 0
    for item in obj_eight_ssr:
        j = 1
        my_sheet.merge_cells(start_row=i + offset, start_column=j, end_row=i + 1 + offset , end_column=j)
        c1 = my_sheet.cell(row=i + offset, column = j)
        c1.value = item.name_region
        c1.alignment = Alignment(wrap_text=True)

        my_sheet.merge_cells(start_row=i + offset, start_column=j + 1, end_row=i + 1 + offset , end_column=j + 1)
        c1 = my_sheet.cell(row=i + offset, column = j + 1)
        c1.alignment = Alignment(wrap_text=True)
        c1.value = item.name_organization

        c1 = my_sheet.cell(row=i + offset, column=j + 2)
        c1.value = item.type_milk_1

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 2)
        c1.value = item.type_milk_2

        c1 = my_sheet.cell(row=i + offset, column=j + 3)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.000'
        c1.value = item.amount_milk_cows

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 3)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.000'
        c1.value = item.amount_milk_goats

        c1 = my_sheet.cell(row=i + offset, column=j + 4)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.rate_cows

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 4)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.rate_goats

        c1 = my_sheet.cell(row=i + offset, column=j + 5)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_accrued_cows

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 5)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_accrued_goats

        c1 = my_sheet.cell(row=i + offset, column=j + 6)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_paid_cows

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 6)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_paid_goats

        c1 = my_sheet.cell(row=i + offset, column=j + 7)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_report_cows

        c1 = my_sheet.cell(row=i + offset + 1, column=j + 7)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_report_goats

        my_sheet.merge_cells(start_row=i + offset + 2, start_column=j, end_row=i + offset + 2, end_column=j + 2)
        c1 = my_sheet.cell(row=i + offset + 2, column=j)

        c1.value = "Итого"

        c1 = my_sheet.cell(row=i + offset + 2, column=j + 3)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.000'
        c1.value = item.amount_milk

        c1 = my_sheet.cell(row=i + offset + 2, column=j + 4)
        c1.alignment = Alignment(horizontal='center')
        c1.value = "X"

        c1 = my_sheet.cell(row=i + offset + 2, column=j + 5)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_accrued

        c1 = my_sheet.cell(row=i + offset + 2, column=j + 6)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_paid

        c1 = my_sheet.cell(row=i + offset + 2, column=j + 7)
        c1.alignment = Alignment(horizontal='center')
        c1.number_format = '0.00'
        c1.value = item.sum_report

        offset = offset + 2
        i = i + 1

    j = 1

    my_sheet.merge_cells(start_row=i + offset, start_column=j, end_row=i + offset, end_column=j + 2)
    c1 = my_sheet.cell(row=i + offset, column=j)
    c1.value = "Всего"

    end_col = 3
    for m in range(5):
        c1 = my_sheet.cell(row=i + offset, column=j + end_col)
        c1.alignment = Alignment(horizontal='center')
        end_col = end_col + 1

    c1 = my_sheet.cell(row=i + offset, column=j + 3)
    c1.number_format = '0.000'
    c1.value = total_amount_milk

    c1 = my_sheet.cell(row=i + offset, column=j + 4)
    c1.value = "X"

    c1 = my_sheet.cell(row=i + offset, column=j + 5)
    c1.number_format = '0.00'
    c1.value = total_sum_accrued

    c1 = my_sheet.cell(row=i + offset, column=j + 6)
    c1.number_format = '0.00'
    c1.value = total_sum_paid

    c1 = my_sheet.cell(row=i + offset, column=j + 7)
    c1.number_format = '0.00'
    c1.value = total_sum_report

    start_row = start_row_table
    end_row = i + offset
    step = start_row + 2
    while start_row <= end_row:
        start_column = 1
        while start_column <= 8:
            if (start_row == step or start_row == end_row):
                my_sheet.cell(row=start_row, column=start_column).font = ft_b
            else:
                my_sheet.cell(row=start_row, column=start_column).font = ft
            my_sheet.cell(row=start_row, column=start_column).border = thin_border
            start_column = start_column + 1

        if (start_row == step):
            step = step + 3
        start_row = start_row + 1

    start_row = 1
    my_sheet.column_dimensions[get_column_letter(start_row)].width = 20
    my_sheet.column_dimensions[get_column_letter(start_row + 1)].width = 40
    my_sheet.column_dimensions[get_column_letter(start_row + 2)].width = 13
    my_sheet.column_dimensions[get_column_letter(start_row + 3)].width = 20

    start_column = 4
    end_column = 8

    while (start_column <= end_column):
        my_sheet.column_dimensions[get_column_letter(start_column)].width = 20
        start_column = start_column + 1

    start_footer = end_row + 2

    my_sheet.merge_cells(start_row=start_footer, start_column=1, end_row=start_footer, end_column=3)
    my_sheet.cell(row=start_footer, column=1).font = ft
    my_sheet.row_dimensions[start_footer].height = 40
    c1 = my_sheet.cell(row=start_footer, column=1)
    c1.value = "Заместитель министра сельского хозяйства и торговли Красноярского края"
    c1.alignment = Alignment(wrap_text=True)

    my_sheet.cell(row=start_footer, column=7).font = ft
    c1 = my_sheet.cell(row=start_footer, column=7)
    c1.value = obj_ssr.fio_deputy_minister

    current_date = datetime.now()
    current_year = current_date.year
    my_sheet.merge_cells(start_row=start_footer + 2, start_column=1, end_row=start_footer + 2, end_column=3)
    my_sheet.cell(row=start_footer + 2, column=1).font = ft
    c1 = my_sheet.cell(row=start_footer + 2, column=1)
    c1.value = "«___» ______________ " + str(current_year) + " г."

    my_sheet.merge_cells(start_row=start_footer + 4, start_column=1, end_row=start_footer + 4, end_column=3)
    my_sheet.cell(row=start_footer + 4, column=1).font = ft_size
    c1 = my_sheet.cell(row=start_footer + 4, column=1)
    c1.value = obj_ssr.fio_spec_oib + ", " + obj_ssr.phone_spec_oib

    my_wb.save("media" + str_document_path)