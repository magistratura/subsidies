from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Pt
from docx import Document
from docx.shared import Inches
from docx2pdf import convert
import pandas as pd
from project.forms import *
from django.shortcuts import render
from project.views_proj.getting_month import *
import datetime
from docxtpl import DocxTemplate
import docx

def toFixed(numObj, digits=0):
    return f"{numObj:.{digits}f}"

def specialist_regisry_to_pdf(id_request_eight_subs, path_name_docx, organization_obj, col_amount_animal, col_cow):
    request_eight_subs_obj = Requests_Eight_Subs.objects.get(id = id_request_eight_subs)
    document = Document()
    section = document.sections[-1]
    section.top_margin = Inches(0.8)
    section.bottom_margin = Inches(0.8)
    section.left_margin = Inches(0.6)
    section.right_margin = Inches(0.6)

    date_current = datetime.datetime.now()
    day_current = date_current.day
    month_current = date_current.month
    if (month_current == 1):
        month_previous = 12
    else:
        month_previous = month_current - 1
    year_current = date_current.year

    data_header = "Справка " + '\n' + "о наличии специалистов, имеющих высшее и (или)" + '\n' + "среднее зоотехническое или ветеринарное образование," + '\n' + "на «01» " +  get_name_month(month_current, 'rod') + " " + str(year_current) + " года"
    paragraph = document.add_paragraph(data_header)
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'

    paragraph = document.add_paragraph(organization_obj.full_name_org + ", " + organization_obj.f_reg_area.name_area)
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'
    paragraph.paragraph_format.space_before = Pt(0)
    paragraph.paragraph_format.space_after = Pt(5)

    list_name_spec_position = []
    list_count_spec = []
    list_actually_working_spec = []
    list_higher_prof = []
    list_secondary_prof = []

    count_record = 0
    cols_count = 6

    for item in request_eight_subs_obj.register_specialists.all():
        list_name_spec_position.append(item.name_spec_position)
        list_count_spec.append(item.count_spec)
        list_actually_working_spec.append(item.actually_working_spec)
        list_higher_prof.append(item.higher_prof)
        list_secondary_prof.append(item.secondary_prof)
        count_record = count_record + 1

    table = document.add_table(rows=count_record + 2, cols=cols_count)

    i = 1
    for item_1 in range(count_record):
        for item_2 in range(cols_count):
            table.cell(i, item_2).vertical_alignment = WD_ALIGN_VERTICAL.BOTTOM
        i = i + 1

    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    table.style = 'TableGrid'

    hdr_cells = table.rows[0].cells

    hdr_cells[0].text = '№ п/п'
    hdr_cells[1].text = 'Наименование должности специалиста'
    hdr_cells[2].text = 'Количество штатных единиц'
    hdr_cells[3].text = 'Фактически работает (человек)'
    hdr_cells[4].text = 'высшее профессиональное'
    hdr_cells[5].text = 'среднее профессиональное (колледжи, техникумы)'

    i = 0
    for item in range(cols_count):
        p = hdr_cells[i].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1
    table.cell(1, 1).width = Inches(4.0)

    hdr_cells = table.rows[1].cells
    i = 0
    for item in range(cols_count):
        hdr_cells[i].text = str(i + 1)
        i = i + 1

    i = 0
    for item in range(cols_count):
        p = hdr_cells[i].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 2
    for item in list_name_spec_position:
        hdr_cells = table.rows[i].cells
        hdr_cells[0].text = str(i - 1)
        p = hdr_cells[0].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 2
    for item in list_name_spec_position:
        hdr_cells = table.rows[i].cells
        hdr_cells[1].text = item
        i = i + 1

    i = 2
    for item in list_count_spec:
        hdr_cells = table.rows[i].cells
        hdr_cells[2].text = str(item)
        p = hdr_cells[2].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 2
    for item in list_actually_working_spec:
        hdr_cells = table.rows[i].cells
        hdr_cells[3].text = str(item)
        p = hdr_cells[3].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 2
    for item in list_higher_prof:
        hdr_cells = table.rows[i].cells
        hdr_cells[4].text = str(item)
        p = hdr_cells[4].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 2
    for item in list_secondary_prof:
        hdr_cells = table.rows[i].cells
        hdr_cells[5].text = str(item)
        p = hdr_cells[5].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1


    paragraph = document.add_paragraph('\t' + 'Общее количество скота(кроме птицы) на первое число месяца подачи заявления в условных головах ' + toFixed(float(col_amount_animal), 0).replace('.', ',') + '.')
    paragraph.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'
    paragraph.paragraph_format.space_before = Pt(10)

    paragraph = document.add_paragraph('\t' + 'Количество коров '+ str(col_cow) +' голов на 01.01.'+ str(year_current) +' года (первое января года предоставления субсидий).')
    paragraph.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'

    document.save(path_name_docx)
    convert(path_name_docx)

def regisry_to_pdf(organization_obj, obj_subsidy, path_name_docx, path_excel):
    m = pd.read_excel(io=path_excel,
                      engine='openpyxl',
                      usecols='A:F',
                      header=4,
                      nrows=654, names=['number_pp', 'name_priem', 'name_doc', 'num_doc', 'date_doc', 'volume_milk'])

    list_name_priem = []
    i = 1
    count_record = 0
    for item in m['name_priem'].dropna().tolist():
        if item != '':
            list_name_priem.insert(i, item)
            count_record = count_record + 1
            i = i + 1

    list_name_doc = []
    i = 1
    for item in m['name_doc'].dropna().tolist():
        if item != '':
            list_name_doc.insert(i, item)
            i = i + 1

    list_num_doc = []
    i = 1
    for item in m['num_doc'].dropna().tolist():
        if item != '':
            list_num_doc.insert(i, item)
            i = i + 1

    list_date_doc = []
    i = 1
    for item in m['date_doc'].dropna().tolist():
        if item != '':
            list_date_doc.insert(i, item)
            i = i + 1

    list_volume_milk = []
    i = 1
    volume_result = 0
    for item in m['volume_milk'].dropna().tolist():
        if item != '':
            list_volume_milk.insert(i, item)
            volume_result = volume_result + item
            i = i + 1

    name_period = str(obj_subsidy.f_subsidies.f_report_period.f_period.name_period)
    year_period = obj_subsidy.f_subsidies.f_report_period.year_sub

    document = Document()

    section = document.sections[-1]
    section.top_margin = Inches(0.8)
    section.bottom_margin = Inches(0.8)
    section.left_margin = Inches(0.6)
    section.right_margin = Inches(0.6)

    data_header = "Реестр " + '\n' + "документов, подтверждающих реализацию и (или) отгрузку на собственную переработку молока за" + '\n' + name_period.lower() + " " + str(year_period) +" года"
    paragraph = document.add_paragraph(data_header)
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'

    paragraph = document.add_paragraph(organization_obj.full_name_org + ", " + organization_obj.f_reg_area.name_area)
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'
    paragraph.paragraph_format.space_before = Pt(0)
    paragraph.paragraph_format.space_after = Pt(0)

    paragraph.style.font.size = Pt(12)
    paragraph.style.font.name = 'Times New Roman'
    paragraph.paragraph_format.space_after = Pt(10)

    table = document.add_table(rows=count_record + 2, cols=6)
    i = 1
    for item_1 in range(count_record):
        for item_2 in range(6):
            table.cell(i, item_2).vertical_alignment = WD_ALIGN_VERTICAL.BOTTOM
        i = i + 1

    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    table.style = 'TableGrid'
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = '№ п/п'
    hdr_cells[1].text = 'Наименование приемщика молока'
    hdr_cells[2].text = 'Наименование документа'
    hdr_cells[3].text = 'Номер документа'
    hdr_cells[4].text = 'дата документа'
    hdr_cells[5].text = 'Объем реализованного и(или) отгруженного на собственную переработку молока в физическом весе(тонн)'

    i = 0
    for item in range(6):
        p = hdr_cells[i].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1
    table.cell(1, 1).width = Inches(3)

    i = 1
    list_name_priem = list(list_name_priem)
    for item in list_name_priem:
        hdr_cells = table.rows[i].cells
        hdr_cells[0].text = str(i)
        p = hdr_cells[0].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 1
    list_name_priem = list(list_name_priem)
    for item in list_name_priem:
        hdr_cells = table.rows[i].cells
        hdr_cells[1].text = item
        i = i + 1

    i = 1
    list_name_doc = list(list_name_doc)
    for item in list_name_doc:
        hdr_cells = table.rows[i].cells
        hdr_cells[2].text = item
        i = i + 1

    i = 1
    list_num_doc = list(list_num_doc)
    for item in list_num_doc:
        hdr_cells = table.rows[i].cells
        hdr_cells[3].text = str(item)
        i = i + 1

    i = 1
    list_date_doc = list(list_date_doc)
    for item in list_date_doc:
        hdr_cells = table.rows[i].cells
        hdr_cells[4].text = item
        i = i + 1

    i = 1
    list_volume_milk = list(list_volume_milk)
    for item in list_volume_milk:
        hdr_cells = table.rows[i].cells
        hdr_cells[5].text = str('{0:,}'.format(item).replace(',', ' ')).replace('.' , ',')
        p = hdr_cells[5].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        i = i + 1

    i = 0

    hdr_cells = table.rows[count_record + 1].cells
    hdr_cells[1].text = "Итого"
    hdr_cells[2].text = "X"
    hdr_cells[3].text = "X"
    hdr_cells[4].text = "X"
    hdr_cells[5].text = str('{0:,}'.format(round(volume_result,3)).replace(',', ' ')).replace('.' , ',')

    i = 2
    for item in range(4):
        p = hdr_cells[i].paragraphs[0]
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        table.cell(count_record + 1, i).vertical_alignment = WD_ALIGN_VERTICAL.BOTTOM
        i = i + 1

    document.save(path_name_docx)
    convert(path_name_docx)