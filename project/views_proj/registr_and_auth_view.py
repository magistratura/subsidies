from django.shortcuts import render
from project.forms import *
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView
from django.contrib.auth.models import User
from django.views.generic import CreateView
from django.contrib.auth.views import LogoutView
from django.contrib.auth import authenticate, login
from django.contrib import messages

class MyprojectLoginView(LoginView):
    template_name = 'registr_and_auth/login.html'
    form_class = AuthUserForm
    success_url = reverse_lazy('login_page')
    def get_success_url(self):
        return reverse_lazy('front_page')

class RegisterUserView(CreateView):
    model = User
    template_name = 'registr_and_auth/register_page.html'
    form_class = RegisterUserForm
    success_url = reverse_lazy('register_page')
    success_msg = 'Пользователь успешно создан'

class MyProjectLogout(LogoutView):
    next_page = reverse_lazy('front_page')