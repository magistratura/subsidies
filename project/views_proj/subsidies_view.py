from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def add_subsidy(request):
    if (request.method == 'POST' and 'save_subsidy' in request.POST):
        try:
            f_report_period = request.POST.get("report_period")
            data_report_period = f_report_period.split(' ')

            name_period = data_report_period[0]
            data_year_sub = data_report_period[1]
            model_name_reporting_period = NameReportingPeriod.objects.get(name_period=name_period)

            model_report_period = ReportingPeriods.objects.get(f_period = int(model_name_reporting_period.id),
                                                               year_sub=int(data_year_sub))

            model_article_law = ArticlesLaw.objects.get(id = int(request.POST.get("f_article_law")))

            item = Subsidies()
            item.f_article_law = model_article_law
            item.f_report_period = model_report_period
            item.rate = request.POST.get("rate")
            if (request.POST.get("data_start_recive")!=""):
                item.data_start_recive = request.POST.get("data_start_recive")
            if (request.POST.get("data_end_recive")!=""):
                item.data_end_recive = request.POST.get("data_end_recive")
            item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except IntegrityError:
            messages.warning(request, "Такая субсидия уже существует!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        # form = SubsidiesForm(request.POST)
        model_item = ReportingPeriods.objects.order_by("year_sub")
        model_article = ArticlesLaw.objects.order_by("name_article")


        return render(request, "guides/Subsidies/add_subsidy.html", {'model_article': model_article, 'model_item': model_item})

def show_subsidies(request):
    model_item = Subsidies.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(model_item, 5)
    try:
        model_item = paginator.page(page)
    except PageNotAnInteger:
        model_item = paginator.page(1)
    except EmptyPage:
        model_item = paginator.page(paginator.num_pages)
    return render(request, "guides/Subsidies/show_subsidies.html", {'model_item': model_item})

def edit_subsidy(request, id):
    model_item = Subsidies.objects.get(id=id)
    if (request.method == 'POST' and 'save_subsidy_edit' in request.POST):
        article_law_select = request.POST.get("article_law_select")
        model_item.f_article_law = ArticlesLaw.objects.get(name_article = article_law_select)

        reporting_periods_select = request.POST.get("reporting_periods_select")
        data_report_period = reporting_periods_select.split(' ')

        name_period = data_report_period[0]
        data_year_sub = data_report_period[1]

        model_report_period = ReportingPeriods.objects.get(f_period__name_period=name_period,
                                                           year_sub=int(data_year_sub))
        model_item.f_report_period = model_report_period

        count_warning = 0

        data_start_recive_region = request.POST.get("data_start_recive_region")
        data_end_recive_region = request.POST.get("data_end_recive_region")
        if (data_start_recive_region < data_end_recive_region):
            model_item.data_start_recive = data_start_recive_region
            model_item.data_end_recive = data_end_recive_region
        else:
            messages.warning(request, "Дата начала приема для района не может быть меньше даты окончания приема!")
            count_warning = count_warning + 1
        data_start_recive_city = request.POST.get("data_start_recive_city")
        data_end_recive_city = request.POST.get("data_end_recive_city")
        if (data_start_recive_city < data_end_recive_city):
            model_item.data_start_recive_city = data_start_recive_city
            model_item.data_end_recive_city = data_end_recive_city
        else:
            messages.warning(request, "Дата начала приема для города не может быть меньше даты окончания приема!")
            count_warning = count_warning + 1
        if count_warning == 0:
            messages.success(request, "Ваше действие успешно выполнено!")
        model_item.save()

    model_articles_law = ArticlesLaw.objects.all()
    model_reporting_periods = ReportingPeriods.objects.all()

    return render(request, "guides/Subsidies/edit_subsidy.html", {'model_item': model_item, 'model_articles_law': model_articles_law, 'model_reporting_periods': model_reporting_periods})