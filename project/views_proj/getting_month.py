def get_index_month(name_month):
    index = 0
    if (name_month == "Январь"):
        index = 1
    elif (name_month == "Февраль"):
        index = 2
    elif (name_month == "Март"):
        index = 3
    elif (name_month == "Апрель"):
        index = 4
    elif (name_month == "Май"):
        index = 5
    elif (name_month == "Июнь"):
        index = 6
    elif (name_month == "Июль"):
        index = 7
    elif (name_month == "Август"):
        index = 8
    elif (name_month == "Сентябрь"):
        index = 9
    elif (name_month == "Октябрь"):
        index = 10
    elif (name_month == "Ноябрь"):
        index = 11
    else:
        index = 12

    return index

def get_name_month(index_month, type_case):
    # im - именительный падеж; rod - именительный падеж
    name_month = ""
    if (index_month == 1):
        if (type_case == 'im'):
            name_month = 'январь'
        else:
            name_month = 'января'
    elif (index_month == 2):
        if (type_case == 'im'):
            name_month = 'февраль'
        else:
            name_month = 'февраля'
    elif (index_month == 3):
        if (type_case == 'im'):
            name_month = 'март'
        else:
            name_month = 'марта'
    elif (index_month == 4):
        if (type_case == 'im'):
            name_month = 'апрель'
        else:
            name_month = 'апреля'
    elif (index_month == 5):
        if (type_case == 'im'):
            name_month = 'май'
        else:
            name_month = 'мая'
    elif (index_month == 6):
        if (type_case == 'im'):
            name_month = 'июнь'
        else:
            name_month = 'июня'
    elif (index_month == 7):
        if (type_case == 'im'):
            name_month = 'июль'
        else:
            name_month = 'июля'
    elif (index_month == 8):
        if (type_case == 'im'):
            name_month = 'август'
        else:
            name_month = 'августа'
    elif (index_month == 9):
        if (type_case == 'im'):
            name_month = 'сентябрь'
        else:
            name_month = 'сентября'
    elif (index_month == 10):
        if (type_case == 'im'):
            name_month = 'октябрь'
        else:
            name_month = 'октября'
    elif (index_month == 11):
        if (type_case == 'im'):
            name_month = 'ноябрь'
        else:
            name_month = 'ноября'
    elif (index_month == 12):
        if (type_case == 'im'):
            name_month = 'декабрь'
        else:
            name_month = 'декабря'
    return name_month