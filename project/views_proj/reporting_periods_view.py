from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError

def add_reporting_period(request):
    if (request.method == 'POST' and 'save_reporting_period' in request.POST):
        try:
            f_period = request.POST.get("f_period")
            model_item_period = NameReportingPeriod.objects.get(id=f_period)
            item = ReportingPeriods(
                f_period = model_item_period,
                year_sub = request.POST.get("year_sub"),
            )
            item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except:
            messages.warning(request, "Такой отчетный период уже существует!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = ReportingPeriodsForm(request.POST)
        return render(request, "guides/Reporting_periods/add_reporting_period.html", {'form': form})

def show_reporting_period(request):
    model_item = ReportingPeriods.objects.all()
    return render(request, "guides/Reporting_periods/show_reporting_periods.html", {'model_item': model_item})

def delete_report_period(request):
    try:
        id_report_period = request.POST.get("selected_td_report_period")
        model_item = ReportingPeriods.objects.get(id=int(id_report_period))
        model_item.delete()
        model_item = ReportingPeriods.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_report_period(request, id):
    try:
        model_item = ReportingPeriods.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            try:
                f_period = request.POST.get("f_period")
                model_item_name_period = NameReportingPeriod.objects.get(name_period=f_period)
                model_item.f_period = model_item_name_period
                model_item.year_sub = request.POST.get("year_sub")
                model_item.save()
                messages.success(request, "Ваше действие успешно выполнено!")
                return redirect(request.META['HTTP_REFERER'])
            except IntegrityError:
                messages.warning(request, "Район регистрации с таким названием уже существует!")
                return redirect(request.META['HTTP_REFERER'])
        else:
            f_period = NameReportingPeriod.objects.order_by("name_period")
            return render(request, "guides/Reporting_periods/edit_reporting_period.html", {'model_item': model_item,
                                                                                           'f_period': f_period})
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])




