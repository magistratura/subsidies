from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages

def add_article_law(request):
    if (request.method == 'POST' and 'save_article_law' in request.POST):
        form = ArticleLawForm(request.POST)
        form.save()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    else:
        form = ArticleLawForm(request.POST)
        return render(request, "guides/Articles_law/add_article_law.html", {'form': form})

def show_articles_law(request):
    model_item = ArticlesLaw.objects.all()
    return render(request, "guides/Articles_law/show_articles_law.html", {'model_item': model_item})

def delete_articles_law(request):
    try:
        id_selected_articles_law = request.POST.get("selected_td_articles_law")
        model_item = ArticlesLaw.objects.get(id=int(id_selected_articles_law))
        model_item.delete()
        model_item = ArticlesLaw.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_article_law(request, id):
    try:
        model_item = ArticlesLaw.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            model_item.name_article = request.POST.get("name_article")
            f_period_article = Periodicity.objects.get(name_period=(request.POST.get("combo_period")))
            model_item.f_period_article = f_period_article
            model_item.num_article = request.POST.get("num_article")
            model_item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        else:
            f_period = Periodicity.objects.all()
            return render(request, "guides/Articles_law/edit_article_law.html", {'model_item': model_item,
                                                                                 'f_period': f_period})
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])
