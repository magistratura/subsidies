import datetime
from docxtpl import DocxTemplate
from docx2pdf import convert
from project.views_proj.getting_month import *

# для пунктов 3 и 4
def determination_item_1(item):
    items = ["", "", "", ""]
    empty_items = ["", "", "", ""]
    if (item == ""):
        items = empty_items
    elif (item == "p1_3"):
        items = ["V", "", "", ""]
    elif (item == "p2_4"):
        items = ["", "V", "", ""]
    elif (item == "p3_5"):
        items = ["", "", "V", ""]
    else:
        items = ["", "", "", "V"]
    return items

# для пункта 5
def determination_item_2(item):
    items = ["", "", ""]
    empty_items = ["", "", "", ""]
    if (item == ""):
        items = empty_items
    elif (item == "p2_3"):
        items = ["V", "", ""]
    elif (item == "p3_4"):
        items = ["", "V", ""]
    else:
        items = ["", "", "V"]
    return items

def toFixed(numObj, digits=0):
    return f"{numObj:.{digits}f}"

def generate_to_word_eight_article(id_request, point_1, point_2, point_3, organization_obj,
                                   month_report, year_report, month_next,
                                   num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                   summ_cows, quarantine_livestock, data_quarantine_establish_1,
                                   data_quarantine_establish_2, num_begin_year_goats,
                                   amount_milk_goats, summ_goats, num_total,
                                   text_kr11, text_kr12, text_kr13,
                                   text_kr21, text_kr22, text_kr23,
                                   text_kr31, text_kr32, print_all, rate_cows, rate_goats):

    mass_items_3 = determination_item_1(point_1)
    mass_items_4 = determination_item_1(point_2)
    mass_items_5 = determination_item_2(point_3)

    date_current = datetime.datetime.now()
    day_current = date_current.day
    month_current = date_current.month
    if (month_current == 1):
        month_previous = 12
    else:
        month_previous = month_current - 1
    year_current = date_current.year

    try:
        if (quarantine_livestock == "Установлен"):
            data_quarantine_establish_1 = str(data_quarantine_establish_1)
            data_divide_1 = data_quarantine_establish_1.split('-')
            data_str_1 = data_divide_1[2] + "." + data_divide_1[1] + "." + data_divide_1[0]

            data_quarantine_establish_2 = str(data_quarantine_establish_2)
            data_divide_2 = data_quarantine_establish_2.split('-')
            data_str_2 = data_divide_2[2] + "." + data_divide_2[1] + "." + data_divide_2[0]
        else:
            data_str_1 = "____________________"
            data_str_2 = "____________________"
    except:
        data_str_1 = "____________________"
        data_str_2 = "____________________"


    if print_all == "True":
        context = {
            'name_sxp': organization_obj.full_name_org,
            'region_sxp': organization_obj.f_reg_area,
            'director_fio': organization_obj.fio_manager_org,
            'date_current': '«' + str(day_current) + "» " + get_name_month(month_current, 'rod') + " " + str(year_current),
            'month_previous': get_name_month(month_previous, 'rod'),
            'year_current': str(year_current),
            'kr11': mass_items_3[0], 'kr12': mass_items_3[1], 'kr13': mass_items_3[2], 'kr14': mass_items_3[3],
            'kr21': mass_items_4[0], 'kr22': mass_items_4[1], 'kr23': mass_items_4[2], 'kr24': mass_items_4[3],
            'kr31': mass_items_5[0], 'kr32': mass_items_5[1], 'kr33': mass_items_5[2],
            'month_report': str(month_report).lower(),
            'year_report': year_report,
            'month_next': month_next,
            'num_begin_year_cows': toFixed(float(num_begin_year_cows), 0).replace('.', ','),
            'num_begin_month_cows': toFixed(float(num_begin_month_cows), 0).replace('.', ','),
            'amount_milk_cows': toFixed(float(amount_milk_cows), 3).replace('.', ','),
            'summ_cows': toFixed(float(summ_cows), 2).replace('.', ','),
            'quarantine': quarantine_livestock.lower(),
            'data_quarantine_establish_1': data_str_1,
            'data_quarantine_establish_2': data_str_2,
            'num_begin_year_goats': toFixed(float(num_begin_year_goats), 0).replace('.', ','),
            'amount_milk_goats': toFixed(float(amount_milk_goats), 3).replace('.', ','),
            'summ_goats': toFixed(float(summ_goats), 2).replace('.', ','),
            'num_total': toFixed(float(num_total), 0).replace('.', ','),
            'month_current': get_name_month(month_current, 'rod'),
            'text_kr11': text_kr11,
            'text_kr12': text_kr12,
            'text_kr13': text_kr13,
            'text_kr21': text_kr21,
            'text_kr22': text_kr22,
            'text_kr23': text_kr23,
            'text_kr31': text_kr31,
            'text_kr32': text_kr32,
            'rate_cows': toFixed(float(rate_cows), 2).replace('.', ','),
            'rate_goats': toFixed(float(rate_goats), 2).replace('.', ',')
        }
    else:
        context = {
            'name_sxp': organization_obj.full_name_org,
            'region_sxp': organization_obj.f_reg_area,
            'director_fio': organization_obj.fio_manager_org,
            'date_current': '«' + str(day_current) + "» " + get_name_month(month_current, 'rod') + " " + str(year_current),
            'year_current': str(year_current),
            'month_report': month_report.lower(),
            'year_report': year_report,
            'month_next': month_next,
            'num_begin_year_cows': toFixed(float(num_begin_year_cows), 0).replace('.', ','),
            'num_begin_month_cows': toFixed(float(num_begin_month_cows), 0).replace('.', ','),
            'amount_milk_cows': toFixed(float(amount_milk_cows), 3).replace('.', ','),
            'summ_cows': toFixed(float(summ_cows), 2).replace('.', ','),
            'quarantine': quarantine_livestock.lower(),
            'data_quarantine_establish_1': data_str_1,
            'data_quarantine_establish_2': data_str_2,
            'num_begin_year_goats': toFixed(float(num_begin_year_goats), 0).replace('.', ','),
            'amount_milk_goats': toFixed(float(amount_milk_goats), 3).replace('.', ','),
            'summ_goats': toFixed(float(summ_goats), 2).replace('.', ','),
            'num_total': toFixed(float(num_total), 0).replace('.', ','),
            'month_current': get_name_month(month_current, 'rod'),
            'rate_cows': toFixed(float(rate_cows), 2).replace('.', ','),
            'rate_goats': toFixed(float(rate_goats), 2).replace('.', ',')
        }

    str_path_docx = "media/uploads/" + str(id_request) + "_" + str(organization_obj.inn_org) + ".docx"
    str_path_pdf = "/uploads/" + str(id_request) + "_" + str(organization_obj.inn_org) + ".pdf"

    if print_all == "True":
        doc_template_8 = DocxTemplate("pattern_subsidies/шаблон_8.docx")
    else:
        doc_template_8 = DocxTemplate("pattern_subsidies/шаблон_8_без_заявления.docx")
    doc_template_8.render(context)
    doc_template_8.save(str_path_docx)
    convert(str_path_docx)

    str_path_pdf_1 = "media/uploads/" + str(id_request) + "_" + str(organization_obj.inn_org) + ".pdf"
    return str_path_pdf_1

def generate_to_word_report_nesvaska(report_nesvaska_obj, organization_obj):
    context = {
        'name_sxp': organization_obj.full_name_org,
        'region_sxp': organization_obj.f_reg_area,
        'cereals': toFixed(report_nesvaska_obj.cereals, 3).replace('.', ','),
        'oilseeds': toFixed(report_nesvaska_obj.oilseeds, 3).replace('.', ','),
        'potato': toFixed(report_nesvaska_obj.potato, 3).replace('.', ','),
        'all_whole_crop': toFixed(report_nesvaska_obj.all_whole_crop, 3).replace('.', ','),
        'vegetables': toFixed(report_nesvaska_obj.vegetables, 3).replace('.', ','),
        'director_fio': organization_obj.fio_manager_org,
        'year_report': str(report_nesvaska_obj.f_reports_requests.current_year)
    }

    str_path_docx = "media/report_nesvaska/" + str(report_nesvaska_obj.id) + "_" + str(organization_obj.inn_org) + ".docx"
    str_path_pdf = "/report_nesvaska/" + str(report_nesvaska_obj.id) + "_" + str(organization_obj.inn_org) + ".pdf"
    doc_template_8 = DocxTemplate("pattern_subsidies/справка о посевной_23.docx")
    doc_template_8.render(context)
    doc_template_8.save(str_path_docx)
    convert(str_path_docx)
    return str_path_pdf

def determination_item_nesv_1(item):
    items = ["", "", ""]
    empty_items = ["", "", ""]
    if (item == ""):
        items = empty_items
    elif (item == "p1"):
        items = ["V", "", ""]
    elif (item == "p2"):
        items = ["", "V", ""]
    elif (item == "p3"):
        items = ["", "", "V"]
    return items

def determination_item_nesv_2(item, organization_obj):
    items = ["", "", ""]
    empty_items = ["", "", ""]
    if (item == ""):
        items = empty_items
    elif (item == "p1"):
        items = [organization_obj.adress_org, "", ""]
    elif (item == "p2"):
        items = ["", "", ""]
    elif (item == "p3"):
        items = ["", "", organization_obj.phone_org]
    return items

def generate_to_word_nesvaska(organization_obj, id_request, request_nesvaska_subs):
    date_current = datetime.datetime.now()
    month_current = date_current.month
    current_date = datetime.datetime.now()
    year_current = current_date.year
    month_current = date_current.month

    if (month_current == 1):
        month_previous = 12
    else:
        month_previous = month_current - 1

    posev_1 = toFixed(float(0), 3).replace('.', ',')
    posev_ins_1 = toFixed(float(0), 3).replace('.', ',')
    sum_pred_1 = toFixed(float(0), 2).replace('.', ',')

    posev_2  = toFixed(float(0), 3).replace('.', ',')
    posev_ins_2 = toFixed(float(0), 3).replace('.', ',')
    sum_pred_2 = toFixed(float(0), 2).replace('.', ',')

    posev_3  = toFixed(float(0), 3).replace('.', ',')
    posev_ins_3 = toFixed(float(0), 3).replace('.', ',')
    sum_pred_3 = toFixed(float(0), 2).replace('.', ',')

    item_nesv_1 = determination_item_nesv_1(request_nesvaska_subs.three_point)
    item_nesv_2 = determination_item_nesv_1(request_nesvaska_subs.fourth_point)

    item_nesv_text_1 = determination_item_nesv_2(request_nesvaska_subs.three_point, organization_obj)
    item_nesv_text_2 = determination_item_nesv_2(request_nesvaska_subs.fourth_point, organization_obj)

    print(item_nesv_text_1)

    context = {
        'name_sxp': organization_obj.full_name_org,
        'region_sxp': organization_obj.f_reg_area,
        'org_phone': organization_obj.phone_org,
        'current_year': str(year_current),
        'month_previous': get_name_month(month_previous, 'rod'),
        'month_current': get_name_month(month_current, 'rod'),
        'cereals': toFixed(float(request_nesvaska_subs.all_whole_cereals_oilseeds), 3).replace('.', ','),
        'vegetables': toFixed(float(request_nesvaska_subs.total_area_vegetables), 3).replace('.', ','),
        'potato': toFixed(float(request_nesvaska_subs.total_area_potato), 3).replace('.', ','),
        'rate_cereals': toFixed(float(request_nesvaska_subs.rate_cereals), 2).replace('.', ','),
        'rate_vegetables': toFixed(float(request_nesvaska_subs.rate_vegetables), 2).replace('.', ','),
        'rate_potato': toFixed(float(request_nesvaska_subs.rate_potato), 2).replace('.', ','),
        'posev_1': posev_1,
        'posev_2': posev_2,
        'posev_3': posev_3,
        'posev_ins_1': posev_ins_1,
        'posev_ins_2': posev_ins_2,
        'posev_ins_3': posev_ins_3,
        'sum_cereals': toFixed(float(request_nesvaska_subs.sum_cereals), 2).replace('.', ','),
        'sum_vegetables': toFixed(float(request_nesvaska_subs.sum_vegetables), 2).replace('.', ','),
        'sum_potato': toFixed(float(request_nesvaska_subs.sum_potato), 2).replace('.', ','),
        'sum_pred_1': sum_pred_1,
        'sum_pred_2': sum_pred_2,
        'sum_pred_3': sum_pred_3,
        'kr11': item_nesv_1[0], 'kr12': item_nesv_1[1], 'kr13': item_nesv_1[2],
        'kr21': item_nesv_2[0], 'kr22': item_nesv_2[1], 'kr23': item_nesv_2[2],
        'text_kr11': item_nesv_text_1[0],'text_kr12': item_nesv_text_1[1], 'text_kr13': item_nesv_text_1[2],
        'text_kr21': item_nesv_text_2[0], 'text_kr22': item_nesv_text_2[1], 'text_kr23': item_nesv_text_2[2],
    }
    str_path_docx = "media/uploads/" + str(id_request) + "_" + str(organization_obj.inn_org) + ".docx"
    str_path_pdf = "/uploads/" + str(id_request) + "_" + str(organization_obj.inn_org) + ".pdf"
    doc_template_8 = DocxTemplate("pattern_subsidies/шаблон_23.docx")
    doc_template_8.render(context)
    doc_template_8.save(str_path_docx)
    convert(str_path_docx)
    return str_path_pdf

