from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from project.views_proj.generate_to_word_view import *
from django.contrib import messages
import datetime
from datetime import date
from project.views_proj.generate_to_pdf_view import *

from django.db.models import Q

def determ_rate(reg_area):
    if (reg_area == 'Таймырский Долгано-Ненецкий округ' or reg_area == 'Эвенкийский район' or reg_area == 'Туруханский район'):
        value_rate = 4600
    elif (reg_area == 'Северо-Енисейский район' or reg_area == 'Енисейский район' or reg_area == 'Богучанский район' or reg_area == 'Мотыгинский район' or reg_area == 'Бирилюсский район' or reg_area == 'Казачинский район' or reg_area == 'Пировский район'):
        value_rate = 3450
    else:
        value_rate = 2300
    return value_rate


def add_eight_request(request, id_sub):
    Subsidies_f = Subsidies.objects.get(id = id_sub)

    id_report_month = get_index_month(Subsidies_f.f_report_period.f_period.name_period)
    if (id_report_month == 12):
        index_report_month = 1
    else:
        index_report_month = id_report_month + 1

    EightSubsForm = RequestsEightSubsForm()
    try:
        authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
        organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)
        reg_area = organization_obj.f_reg_area.name_area
        Rates_Cows = determ_rate(reg_area)
        Rates_Goats = Rate.objects.get(f_subsidies=Subsidies_f, name_rate="Козье")
    except:
        messages.warning(request, "Не верно установлены ставки для субсидии!")
        return redirect(request.META['HTTP_REFERER'])

    first_in_group = check_exist_group_eight(Subsidies_f, request.user)

    current_date = datetime.datetime.now()
    current_year = current_date.year

    report_month_rod = get_name_month(index_report_month, 'rod')
    print(Rates_Cows)

    return render(request, "requests/eight_subs/add_eight_article.html",
                  {'form_eight_subs': EightSubsForm,
                   'month_next': report_month_rod,
                   'current_year': current_year,
                   'subsidies_obj': Subsidies_f,
                   'rate_cows': Rates_Cows,
                   'rate_goats': Rates_Goats.value_rate,
                   'first_in_group': first_in_group[0],
                   'id_requests_eight_subs_exists': first_in_group[1]})

def check_exist_group_eight(Subsidies_f, user):
    authorized_user = OrganizationWithUser.objects.get(f_user__user = user)

    try:
        exist_group_requests = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                       report_year_subsid = Subsidies_f.f_report_period.year_sub,
                                                       f_organization = authorized_user.f_org,
                                                       removed=False)
        first_in_group = False
        last_request = exist_group_requests.last()
        Last_Requests_Eight_Subs = Requests_Eight_Subs.objects.get(f_requests = last_request)
        id_exists = Last_Requests_Eight_Subs.id
    except:
        first_in_group = True
        id_exists = ""
    return first_in_group, id_exists

def set_points_eight_subs_refact(organization_obj, request_eight_subs, three_point, fourth_point, fifth_point):
    text_kr11 = ""
    text_kr12 = ""
    text_kr13 = ""
    text_kr21 = ""
    text_kr22 = ""
    text_kr23 = ""
    text_kr31 = ""
    text_kr32 = ""

    Points_subs_1 = ['вручить лично, предварительно оповестив по телефону*;',
                     'направить по почтовому адресу:',
                     'направить по адресу электронной почты',
                     'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**',
                     '']

    Points_subs_2 = ['вручить лично, предварительно оповестив по телефону*;',
                     'направить по почтовому адресу:',
                     'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**',
                     '']

    if (three_point == Points_subs_1[0]):
        three_point_text = organization_obj.phone_org
        point_1_req = 'p1_3'
        text_kr11 = three_point_text
    elif (three_point == Points_subs_1[1]):
        three_point_text = organization_obj.adress_org
        point_1_req = 'p2_4'
        text_kr12 = three_point_text
    elif (three_point == Points_subs_1[2]):
        three_point_text = organization_obj.email_org
        point_1_req = 'p3_5'
        text_kr13 = three_point_text
    elif (three_point == Points_subs_1[3]):
        three_point_text = ""
        point_1_req = 'p4_6'
    else:
        point_1_req = ""
        three_point_text = ""
        text_kr11 = ""

    if (fourth_point == Points_subs_1[0]):
        fourth_point_text = organization_obj.phone_org
        point_2_req = 'p1_3'
        text_kr21 = fourth_point_text
    elif (fourth_point == Points_subs_1[1]):
        fourth_point_text = organization_obj.adress_org
        point_2_req = 'p2_4'
        text_kr22 = fourth_point_text
    elif (fourth_point == Points_subs_1[2]):
        fourth_point_text = organization_obj.email_org
        point_2_req = 'p3_5'
        text_kr23 = fourth_point_text
    elif (fourth_point == Points_subs_1[3]):
        fourth_point_text = ""
        point_2_req = 'p4_6'
    else:
        fourth_point_text = ""
        point_2_req = ""

    if (fifth_point == Points_subs_2[0]):
        fifth_point_text = organization_obj.phone_org
        point_3_req = 'p2_3'
        text_kr31 = fifth_point_text
    elif (fifth_point == Points_subs_2[1]):
        fifth_point_text = organization_obj.adress_org
        point_3_req = 'p3_4'
        text_kr32 = fifth_point_text
    elif (fifth_point == Points_subs_2[2]):
        fifth_point_text = ""
        point_3_req = 'p4_5'
    else:
        fifth_point_text = ""
        point_3_req = ""

    request_eight_subs.three_point = point_1_req
    request_eight_subs.text_three_point = three_point_text

    request_eight_subs.fourth_point = point_2_req
    request_eight_subs.text_fourth_point = fourth_point_text

    request_eight_subs.fifth_point = point_3_req
    request_eight_subs.text_fifth_point = fifth_point_text

    request_eight_subs.save()
    return point_1_req, point_2_req, point_3_req, text_kr11, text_kr12, text_kr13, text_kr21, text_kr22, text_kr23, text_kr31, text_kr32

def set_points_eight(organization_obj, three_point, fourth_point, fifth_point):
    text_kr11 = ""
    text_kr12 = ""
    text_kr13 = ""
    text_kr21 = ""
    text_kr22 = ""
    text_kr23 = ""
    text_kr31 = ""
    text_kr32 = ""

    point_1 = three_point
    three_point_text = ""
    if (point_1 == 'p1_3'):
        three_point_text = organization_obj.phone_org
        text_kr11 = three_point_text
    elif (point_1 == 'p2_4'):
        three_point_text = organization_obj.adress_org
        text_kr12 = three_point_text
    elif (point_1 == 'p3_5'):
        three_point_text = organization_obj.email_org
        text_kr13 = three_point_text
    else:
        three_point_text = ""

    point_2 = fourth_point
    fourth_point_text = ""
    if (point_2 == 'p1_3'):
        fourth_point_text = organization_obj.phone_org
        text_kr21 = fourth_point_text
    elif (point_2 == 'p2_4'):
        fourth_point_text = organization_obj.adress_org
        text_kr22 = fourth_point_text
    elif (point_2 == 'p3_5'):
        fourth_point_text = organization_obj.email_org
        text_kr23 = fourth_point_text
    else:
        fourth_point_text = ""

    point_3 = fifth_point
    fifth_point_text = ""
    if (point_3 == 'p2_3'):
        fifth_point_text = organization_obj.phone_org
        text_kr31 = fifth_point_text
    elif (point_3 == 'p3_4'):
        fifth_point_text = organization_obj.adress_org
        text_kr32 = fifth_point_text
    else:
        fifth_point_text = ""

    return text_kr11, text_kr12, text_kr13, text_kr21, text_kr22, text_kr23, text_kr31, text_kr32, point_1, three_point_text, point_2, fourth_point_text, point_3, fifth_point_text

def create_requests_eight_subs(current_id_request, rate_cows, rate_goats, num_total,
                               id_requests_eight_subs_exists,
                               organization_obj, three_point, fourth_point, fifth_point, leukemia_KRS,
                               quarantine, data_quarantine_establish_1, data_quarantine_establish_2,
                               num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                               num_begin_year_goats, amount_milk_goats, summ_cows, summ_goats, id_subsidy,
                               upload_file, uploadfile,
                               name_spec_position, count_spec, actually_working_spec,
                               higher_prof, secondary_prof, count_int):
    item_request_eight_subs = Requests_Eight_Subs()
    request_obj = Requests.objects.get(id=current_id_request)

    item_request_eight_subs.rate_cows = float(rate_cows)
    item_request_eight_subs.rate_goats = float(rate_goats)
    item_request_eight_subs.num_total_livestock = float(num_total)

    id_requests_eight_subs = id_requests_eight_subs_exists
    points_eight = set_points_eight(organization_obj, three_point, fourth_point, fifth_point)

    if id_requests_eight_subs != "":
        obj_requests_eight_subs = Requests_Eight_Subs.objects.get(id=id_requests_eight_subs)

        item_request_eight_subs.three_point = obj_requests_eight_subs.three_point
        item_request_eight_subs.text_three_point = obj_requests_eight_subs.text_three_point

        item_request_eight_subs.fourth_point = obj_requests_eight_subs.fourth_point
        item_request_eight_subs.text_fourth_point = obj_requests_eight_subs.text_fourth_point

        item_request_eight_subs.fifth_point = obj_requests_eight_subs.fifth_point
        item_request_eight_subs.text_fifth_point = obj_requests_eight_subs.text_fifth_point
    else:
        try:
            item_request_eight_subs.three_point = points_eight[8]
            item_request_eight_subs.text_three_point = points_eight[9]
        except:
            print("данные не выбраны")
        try:
            item_request_eight_subs.fourth_point = points_eight[10]
            item_request_eight_subs.text_fourth_point = points_eight[11]
        except:
            print("данные не выбраны")
        try:
            item_request_eight_subs.fifth_point = points_eight[12]
            item_request_eight_subs.text_fifth_point = points_eight[13]
        except:
            print("данные не выбраны")

    leukemia_livestock = leukemia_KRS
    if (leukemia_livestock == "Был лейкоз"):
        item_request_eight_subs.leukemia_livestock = 'v1'
    else:
        item_request_eight_subs.leukemia_livestock = 'v2'

    quarantine_livestock = quarantine
    if (quarantine_livestock == "Установлен"):
        item_request_eight_subs.quarantine_livestock = 'q1'
    else:
        item_request_eight_subs.quarantine_livestock = 'q2'

    data_quarantine_establish_1 = data_quarantine_establish_1
    if (data_quarantine_establish_1 != ""):
        item_request_eight_subs.data_quarantine_establish_1 = data_quarantine_establish_1

    data_quarantine_establish_2 = data_quarantine_establish_2
    if (data_quarantine_establish_2 != ""):
        item_request_eight_subs.data_quarantine_establish_2 = data_quarantine_establish_2

    item_request_eight_subs.num_begin_year_cows = num_begin_year_cows
    item_request_eight_subs.num_begin_month_cows = num_begin_month_cows
    item_request_eight_subs.amount_milk_cows = amount_milk_cows
    item_request_eight_subs.num_begin_month_goats = num_begin_year_goats
    item_request_eight_subs.amount_milk_goats = amount_milk_goats
    item_request_eight_subs.f_requests = request_obj

    summ_cows = summ_cows
    item_request_eight_subs.sum_cows = float(summ_cows)
    summ_goats = summ_goats
    item_request_eight_subs.sum_goats = float(summ_goats)

    try:
        if upload_file == True:
            obj_subsidy = Subsidies.objects.get(id=int(id_subsidy))

            path_name_docx_registry = "media/registry_document/" + "registy_" + str(
                current_id_request) + "_" + organization_obj.inn_org + ".docx"
            path_excel = uploadfile
            regisry_to_pdf(organization_obj, obj_subsidy, path_name_docx_registry, path_excel)
    except:
        print("подгрузили не то")

    item_request_eight_subs.save()
    id_request_eight_subs = item_request_eight_subs.id

    i = 0
    for item in range(count_int):
        specialists_register = Specialists_register()
        specialists_register.name_spec_position = name_spec_position[i]
        specialists_register.count_spec = count_spec[i]
        specialists_register.actually_working_spec = actually_working_spec[i]
        specialists_register.higher_prof = higher_prof[i]
        specialists_register.secondary_prof = secondary_prof[i]
        specialists_register.save()
        i = i + 1
        item_request_eight_subs.register_specialists.add(specialists_register)
        item_request_eight_subs.save()

    if i != 0:
        path_name_docx_regisry_spec = "media/registry_specialist/" + "registy_specialist_" + str(item_request_eight_subs.id) + "_" + organization_obj.inn_org + ".docx"
        specialist_regisry_to_pdf(item_request_eight_subs.id, path_name_docx_regisry_spec, organization_obj, item_request_eight_subs.num_total_livestock, item_request_eight_subs.num_begin_year_cows)

    path_name_pdf_registry = "media/registry_document/" + "registy_" + str(current_id_request) + "_" + organization_obj.inn_org + ".pdf"
    path_name_pdf_regisry_spec = "media/registry_specialist/" + "registy_specialist_" + str(item_request_eight_subs.id) + "_" + organization_obj.inn_org + ".pdf"

    return quarantine_livestock, data_quarantine_establish_1, data_quarantine_establish_2, id_request_eight_subs, path_name_pdf_registry, path_name_pdf_regisry_spec

def resave_requests_eight_subs(item_request_eight_subs, num_total,
                                 leukemia_livestock, quarantine_livestock,
                                 data_quarantine_establish_1, data_quarantine_establish_2,
                                 num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                 num_begin_year_goats, amount_milk_goats, summ_cows, summ_goats):

    Leukemia_subs = ['Был лейкоз',
                     'Не было лейкоза']

    Quarantine = ['Установлен',
                  'Не установлен']

    item_request_eight_subs.num_total_livestock = float(num_total)

    if (leukemia_livestock == Leukemia_subs[0]):
        item_request_eight_subs.leukemia_livestock = 'v1'
    else:
        item_request_eight_subs.leukemia_livestock = 'v2'

    if (quarantine_livestock == Quarantine[0]):
        item_request_eight_subs.quarantine_livestock = 'q1'
        if (data_quarantine_establish_1 != ""):
            item_request_eight_subs.data_quarantine_establish_1 = data_quarantine_establish_1

        if (data_quarantine_establish_2 != ""):
            item_request_eight_subs.data_quarantine_establish_2 = data_quarantine_establish_2
    else:
        item_request_eight_subs.quarantine_livestock = 'q2'
        item_request_eight_subs.data_quarantine_establish_1 = None
        item_request_eight_subs.data_quarantine_establish_2 = None

    item_request_eight_subs.num_begin_year_cows = num_begin_year_cows
    item_request_eight_subs.num_begin_month_cows = num_begin_month_cows
    item_request_eight_subs.amount_milk_cows = amount_milk_cows
    item_request_eight_subs.num_begin_month_goats = num_begin_year_goats
    item_request_eight_subs.amount_milk_goats = amount_milk_goats

    item_request_eight_subs.sum_cows = float(summ_cows)
    item_request_eight_subs.sum_goats = float(summ_goats)

    summ_total = float(summ_cows) + float(summ_goats)
    item_request_eight_subs.save()

    return summ_total


def get_volume_milk(path_excel):
    m = pd.read_excel(io=path_excel,
                      engine='openpyxl',
                      usecols='A:F',
                      header=4,
                      nrows=654, names=['number_pp', 'name_priem', 'name_doc', 'num_doc', 'date_doc', 'volume_milk'])
    list_volume_milk = []
    i = 1
    volume_result = 0
    for item in m['volume_milk'].dropna().tolist():
        if item != '':
            list_volume_milk.insert(i, item)
            volume_result = volume_result + item
            i = i + 1
    return volume_result