from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import IntegrityError
from project.views_proj.ssr_eight_article import *
from project.views_proj.ssr_nesvaska_article import *
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger

# если first_time = True, значит ССР формируется на основании статусов заявок
def create_cur_ssr(ssr_approved, ssr_name_period, user, first_time, num_article, select_year_article,
                   name_title, fio_spec_oib, phone_spec_oib, fio_deputy_minister):
    ssr_model_item = SSR()
    if first_time == True:
        ssr_model_item.name_period = ssr_name_period
        ssr_model_item.f_articles_law = ArticlesLaw.objects.get(num_article=int(num_article))
        ssr_model_item.name_title = name_title
        ssr_model_item.fio_spec_oib = fio_spec_oib
        ssr_model_item.phone_spec_oib = phone_spec_oib
        ssr_model_item.fio_deputy_minister = fio_deputy_minister
        ssr_model_item.report_year = int(select_year_article)
    else:
        ssr_model_item = ssr_approved.last()
        ssr_model_item.name_period = ssr_name_period
        id_last_ssr = SSR.objects.last()
        ssr_model_item.id = id_last_ssr.id + 1
        ssr_model_item.data_adopt = None
        ssr_model_item.excel_document_path = ""
    ssr_model_item.f_status = "ss1"
    ssr_model_item.data_create = datetime.now()
    ssr_model_item.change_history = "Дата создания: " + str(ssr_model_item.data_create) + ", " + str(user) + ";" + "\n"
    ssr_model_item.save()
    return ssr_model_item

def create_ssr(request):
    obj_articles_law = ArticlesLaw.objects.all()

    if (request.method == 'POST' and 'create_ssr' in request.POST):
        name_period_ssr = request.POST.get("name_period_ssr")

        ckecked_request_ssr = request.POST.get("used_request_ssr")
        if ckecked_request_ssr == "1":
            used_request_ssr = True
        else:
            used_request_ssr = False

        select_year_article = request.POST.get("select_year_article")
        select_num_article = request.POST.get("select_num_article")
        data_num_article = select_num_article.split(' ')
        num_article = data_num_article[0]

        if (select_year_article=="-----" or select_num_article =="-----"):
            messages.warning(request, "Вы выбрали не все параметры!")
            return redirect(request.META['HTTP_REFERER'])

        # формирование сср по 8 статье
        if (int(num_article) == 8):
            # на основании заявок
            try:
                if (used_request_ssr == True):
                    name_title = "Сводная справка-расчет" + "\n" \
                                 + "субсидии на компенсацию части затрат на производство" \
                                 + "\n" + "и реализацию молока за" + "\n"
                    fio_spec_oib = "Челелетдинова А.М."
                    phone_spec_oib = "211-03-21"
                    fio_deputy_minister = "Белецкая Л.И."

                    ssr_model_item = create_cur_ssr("", name_period_ssr, request.user, True, num_article,
                                                    select_year_article, name_title, fio_spec_oib, phone_spec_oib,
                                                    fio_deputy_minister)

                    obj_distinct_org = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                               num_subsid=int(num_article)).values_list("f_organization__full_name_org", flat=True).distinct()
                    obj_list_name_sxp = []
                    for item in obj_distinct_org:
                        obj_list_name_sxp.append(item)

                    create_ssr_eight_subs(True, obj_list_name_sxp, num_article, select_year_article, ssr_model_item,
                                          "", "")
                    save_excel_eighth_ssr(ssr_model_item)
                    messages.success(request, "Ваше действие успешно выполнено!")
                # на основании ССР в статусе утверждено
                else:
                    ssrs_approved = SSR.objects.filter(report_year=select_year_article,
                                                       f_articles_law__num_article=int(num_article),
                                                       f_status='ss2')

                    ssr_eighth_subs_approved = SSR_Eight_Subs.objects.filter(f_ssr=ssrs_approved.last().id)

                    ssr_model_item = create_cur_ssr(ssrs_approved, name_period_ssr, request.user, False,
                                                       "", "", "", "", "", "")
                    create_ssr_eight_subs(False, "", "", "", "", ssr_eighth_subs_approved, ssr_model_item)

                    to_paid_eighth_subs_ssr(SSR_Eight_Subs.objects.filter(f_ssr=ssr_model_item.id))

                    requests_approved = Requests.objects.filter(f_status='s6',
                                                                num_subsid=int(num_article),
                                                                report_year_subsid=int(select_year_article),
                                                                used_ssr = False)

                    for item in requests_approved:
                        if SSR_Eight_Subs.objects.filter(f_ssr=ssr_model_item.id, name_organization = item.f_organization.full_name_org).exists():
                            rewrite_eighth_subs_ssr(ssr_model_item, item)
                        else:
                            create_eighth_subs_ssr(ssr_model_item, item)
                    save_excel_eighth_ssr(SSR.objects.get(id=ssr_model_item.id))
                    messages.success(request, "Ваше действие успешно выполнено!")
            except IndexError:
                messages.info(request, "Не найдено ни одной заявки!")
            except IntegrityError as e:
                if 'unique constraint':
                    messages.warning(request, "Сводная справка - расчет с таким именем уже существует!")
            except:
                messages.info(request, "Нет сводных в статусе «Утверждено»!")
        elif (int(num_article) == 23):
            if (used_request_ssr == True):
                requests = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                   num_subsid=int(num_article),
                                                   report_year_subsid=int(select_year_article))
                if requests.count() == 0:
                    messages.info(request, "Нет ни одной заявки в статусе «Проверено ОИБ» или «Выплачено»!")
                    return redirect(request.META['HTTP_REFERER'])


                name_title = "Сводная справка-расчет" + "\n" \
                             + "субсидии на возмещение части затрат, направленных на проведение" \
                             + "\n" + "агротехнологических работ, повышение уровня экологической безопасности сельскохозяйственного производства," + \
                             "\n" + "а также на повышение плодородия и качества почв, в"
                fio_spec_oib = "Макота Е.А."
                phone_spec_oib = "212-31-78"
                fio_deputy_minister = "Белецкая Л.И."

                ssr_model_item = create_cur_ssr("", name_period_ssr, request.user, True, num_article,
                                                select_year_article, name_title, fio_spec_oib, phone_spec_oib,
                                                fio_deputy_minister)
                obj_distinct_org = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                           num_subsid=int(num_article), report_year_subsid = int(select_year_article)).values_list("f_organization__full_name_org", flat=True).distinct()
                obj_list_name_sxp = []
                for item in obj_distinct_org:
                    obj_list_name_sxp.append(item)

                create_ssr_nesvaska_subs(True, obj_list_name_sxp, num_article,
                                         select_year_article, ssr_model_item, "", "")
                save_excel_nesvaska_ssr(ssr_model_item)
                messages.success(request, "Ваше действие успешно выполнено!")
            else:
                try:
                    ssrs_approved = SSR.objects.filter(report_year=select_year_article,
                                                       f_articles_law__num_article=int(num_article),
                                                       f_status='ss2')

                    ssr_nesvaska_subs_approved = SSR_Nesvaska_Subs.objects.filter(f_ssr=ssrs_approved.last().id)

                    ssr_model_item = create_cur_ssr(ssrs_approved, name_period_ssr, request.user, False, "", "", "", "", "", "")
                    create_ssr_nesvaska_subs(False, "", "", "", "", ssr_nesvaska_subs_approved, ssr_model_item)
                    to_paid_nesvaska_subs_ssr(SSR_Nesvaska_Subs.objects.filter(f_ssr=ssr_model_item.id))

                    requests_approved = Requests.objects.filter(f_status='s6',
                                                                num_subsid=int(num_article),
                                                                report_year_subsid=int(select_year_article) - 1,
                                                                used_ssr=False)

                    print('requests_approved', requests_approved)
                    for item in requests_approved:
                        if SSR_Nesvaska_Subs.objects.filter(f_ssr=ssr_model_item.id,
                                                         name_organization=item.f_organization.full_name_org).exists():
                            print("exist")
                        else:
                            print("create_nesvaska_subs_ssr")
                            create_nesvaska_subs_ssr(ssr_model_item, item)
                    save_excel_nesvaska_ssr(SSR.objects.get(id=ssr_model_item.id))
                    messages.success(request, "Ваше действие успешно выполнено!")
                except:
                    messages.info(request, "Нет сводных в статусе «Утверждено»!")
        else:
            print("другие")

    return render(request, "ssr/create_ssr.html", {'obj_articles_law': obj_articles_law})

def show_actions_ssrs(request):
    return render(request, "ssr/show_actions_ssrs.html")

def show_removed_ssrs(request):
    content_label_sort_ssrs = "По возрастанию"
    state_sort_ssrs = True
    model_item = SSR.objects.filter(removed = True).order_by('id')
    selected_article = "-----"
    selected_status = "-----"
    num_ssr = ""
    try:
        if (request.method == 'POST' and 'apply_ssr' in request.POST):
            ckecked_sort_ssrs = request.POST.get("sort_ssrs")
            filter_article = request.POST.get("filter_article")
            filter_status = request.POST.get("filter_status")

            selected_article = filter_article
            selected_status = filter_status

            if filter_article != "-----":
                model_item = SSR.objects.filter(f_articles_law__num_article = filter_article, removed = True)

            if filter_status != "-----":
                if filter_status == "Черновик":
                    value_statue = 'ss1'
                else:
                    value_statue = 'ss2'
                model_item = model_item.filter(f_status = value_statue, removed = True)

            if ckecked_sort_ssrs == "1":
                content_label_sort_ssrs = "По возрастанию"
                state_sort_ssrs = True
                model_item = model_item.order_by("id")
            else:
                content_label_sort_ssrs = "По убыванию"
                state_sort_ssrs = False
                model_item = model_item.order_by("-id")
    except:
        messages.warning(request, "Не найдено ни одной записи")
    if (request.method == 'POST' and 'clear_filter_ssr' in request.POST):
        state_sort_ssrs = True
        selected_article = "-----"
        selected_status = "-----"

    if model_item.count() == 0:
        messages.warning(request, "Не найдено ни одной записи")

    count_record = model_item.count()

    page = request.GET.get('page', 1)
    paginator = Paginator(model_item, 10)

    try:
        model_item = paginator.page(page)
    except PageNotAnInteger:
        model_item = paginator.page(1)
    except EmptyPage:
        model_item = paginator.page(paginator.num_pages)

    return render(request, "ssr/show_list_ssrs.html", {'model_item': model_item,
                                                       'content_label_sort_ssrs': content_label_sort_ssrs,
                                                       'state_sort_ssrs': state_sort_ssrs,
                                                       'selected_article': selected_article,
                                                       'selected_status': selected_status,
                                                       'num_ssr': num_ssr,
                                                       'count_record': count_record})

def show_list_ssrs(request):
    content_label_sort_ssrs = "По возрастанию"
    state_sort_ssrs = True
    model_item = SSR.objects.filter(removed = False).order_by('id')
    selected_article = "-----"
    selected_status = "-----"
    num_ssr = ""
    try:
        if (request.method == 'POST' and 'apply_ssr' in request.POST):
            ckecked_sort_ssrs = request.POST.get("sort_ssrs")
            filter_article = request.POST.get("filter_article")
            filter_status = request.POST.get("filter_status")

            selected_article = filter_article
            selected_status = filter_status

            if filter_article != "-----":
                model_item = SSR.objects.filter(f_articles_law__num_article = filter_article, removed = False)

            if filter_status != "-----":
                if filter_status == "Черновик":
                    value_statue = 'ss1'
                else:
                    value_statue = 'ss2'
                model_item = model_item.filter(f_status = value_statue, removed = False)

            if ckecked_sort_ssrs == "1":
                content_label_sort_ssrs = "По возрастанию"
                state_sort_ssrs = True
                model_item = model_item.order_by("id")
            else:
                content_label_sort_ssrs = "По убыванию"
                state_sort_ssrs = False
                model_item = model_item.order_by("-id")
    except:
        messages.warning(request, "Не найдено ни одной записи")
    if (request.method == 'POST' and 'clear_filter_ssr' in request.POST):
        state_sort_ssrs = True
        selected_article = "-----"
        selected_status = "-----"

    if model_item.count() == 0:
        messages.warning(request, "Не найдено ни одной записи")

    count_record = model_item.count()

    page = request.GET.get('page', 1)
    paginator = Paginator(model_item, 10)

    try:
        model_item = paginator.page(page)
    except PageNotAnInteger:
        model_item = paginator.page(1)
    except EmptyPage:
        model_item = paginator.page(paginator.num_pages)

    return render(request, "ssr/show_list_ssrs.html", {'model_item': model_item,
                                                       'content_label_sort_ssrs': content_label_sort_ssrs,
                                                       'state_sort_ssrs': state_sort_ssrs,
                                                       'selected_article': selected_article,
                                                       'selected_status': selected_status,
                                                       'num_ssr': num_ssr,
                                                       'count_record': count_record})

def show_ssr(request, id):
    model_item = SSR.objects.get(id=id)
    if model_item.f_articles_law.num_article == '8':
        obj_eight_ssr = SSR_Eight_Subs.objects.filter(f_ssr = model_item.id).order_by("name_region")
        total_sum = get_total_sum(model_item)
        name_title = model_item.name_title + " " + model_item.name_period

        return render(request, "ssr/eight_subs_ssr/show_eight_ssr.html",
                      {'obj_eight_ssr': obj_eight_ssr,
                       'obj_ssr': model_item,
                       'total_amount_milk': total_sum[0],
                       'total_sum_accrued': total_sum[1],
                       'total_sum_paid': total_sum[2],
                       'total_sum_report': total_sum[3],
                       'name_title': name_title})

    elif model_item.f_articles_law.num_article == '23':
        obj_nesvaska_ssr = SSR_Nesvaska_Subs.objects.filter(f_ssr = model_item.id).order_by("name_region")
        total_sum_nesvaska = get_total_sum_nesvaska(model_item)
        name_title = model_item.name_title + " " + model_item.name_period
        return render(request, "ssr/nesvaska/show_nesvaska_ssr.html",
                      {'obj_nesvaska_ssr': obj_nesvaska_ssr,
                       'obj_ssr': model_item,
                       'name_title': name_title,
                       'total_area': total_sum_nesvaska[0],
                       'total_area_proekt': total_sum_nesvaska[1],
                       'total_area_insurance': total_sum_nesvaska[2],
                       'total_need': total_sum_nesvaska[3],
                       'total_estimated_f': total_sum_nesvaska[4],
                       'total_provided_f': total_sum_nesvaska[5],
                       'total_due_f': total_sum_nesvaska[6],
                       'total_estimated_k': total_sum_nesvaska[7],
                       'total_provided_k': total_sum_nesvaska[8],
                       'total_due_k': total_sum_nesvaska[9]})

    else:
        print("другая")
        return redirect(request.META['HTTP_REFERER'])

def edit_ssr(request, id):
    model_item = SSR.objects.get(id=id)
    if model_item.f_articles_law.num_article == '8':
        obj_eight_ssr = SSR_Eight_Subs.objects.filter(f_ssr = model_item.id).order_by("name_region")
        total_sum = get_total_sum(model_item)
        if (request.method == 'POST' and 'eight_ssr_save_changes' in request.POST):
            for item in obj_eight_ssr:
                item.sum_accrued_cows = float(request.POST.get(str(item.id) + "_sum_accrued_cows_" + str(item.sum_accrued_cows)))
                item.sum_accrued_goats = float(request.POST.get(str(item.id) + "_sum_accrued_goats_" + str(item.sum_accrued_goats)))
                item.sum_accrued = item.sum_accrued_cows + item.sum_accrued_goats

                item.sum_paid_cows = float(request.POST.get(str(item.id) + "_sum_paid_cows_" + str(item.sum_paid_cows)))
                item.sum_paid_goats = float(request.POST.get(str(item.id) + "_sum_paid_goats_" + str(item.sum_paid_goats)))
                item.sum_paid = item.sum_paid_cows + item.sum_paid_goats

                item.sum_report_cows = float(request.POST.get(str(item.id) + "_sum_report_cows_" + str(item.sum_report_cows)))
                item.sum_report_goats = float(request.POST.get(str(item.id) + "_sum_report_goats_" + str(item.sum_report_goats)))
                item.sum_report = item.sum_report_cows + item.sum_report_goats
                item.save()

            start_item_row_table = 5
            str_document_path = "/ssrs/" + "сводная справка - расчет_8_"+str(model_item.id) + ".xlsx"
            save_eighth_ssr_to_excel(start_item_row_table, obj_eight_ssr, model_item, total_sum[0],
                                     total_sum[1], total_sum[2], total_sum[3], str_document_path)

            model_item.excel_document_path = str_document_path
            model_item.name_period = request.POST.get("eight_ssr_name_period")
            model_item.fio_spec_oib  = request.POST.get("eight_ssr_fio_spec_oib")
            model_item.phone_spec_oib = request.POST.get("eight_ssr_phone_spec_oib")
            model_item.fio_deputy_minister = request.POST.get("eight_ssr_fio_deputy_minister")
            model_item.change_history = model_item.change_history + "Внесены изменения в сводную справку - расчет: " + str(datetime.now()) + ", " + str(request.user) + "\n"
            model_item.save()

            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])

        if (request.method == 'POST' and 'edit_ssr_approve' in request.POST):
            model_item.f_status = 'ss2'
            model_item.data_adopt = datetime.now()
            model_item.change_history = model_item.change_history + "Утверждение сводной справки - расчет: " + str(datetime.now()) + ", " + str(request.user) + "\n"
            model_item.save()

            item_requests= Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                   num_subsid = '8', used_ssr = False,
                                                   report_year_subsid = int(model_item.report_year))
            for item in item_requests:
                print(item)
                item.used_ssr = True
                item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])

        return render(request, "ssr/eight_subs_ssr/edit_eight_ssr.html",
                      {'obj_eight_ssr': obj_eight_ssr,
                       'obj_ssr': model_item,
                       'total_amount_milk': total_sum[0],
                       'total_sum_accrued': total_sum[1],
                       'total_sum_paid': total_sum[2],
                       'total_sum_report': total_sum[3]})
    elif model_item.f_articles_law.num_article == '23':
        obj_nesvaska_ssr = SSR_Nesvaska_Subs.objects.filter(f_ssr = model_item.id).order_by("name_region")
        total_sum_nesvaska = get_total_sum_nesvaska(model_item)
        name_title = model_item.name_title + " " + model_item.name_period

        if (request.method == 'POST' and 'nesvaska_ssr_save_changes' in request.POST):
            for item in obj_nesvaska_ssr:
                item.need_subsidy_cereals = float(request.POST.get(str(item.id) + "_need_subsidy_cereals_" + str(item.need_subsidy_cereals)))
                item.need_subsidy_vegetables = float(request.POST.get(str(item.id) + "_need_subsidy_vegetables_" + str(item.need_subsidy_vegetables)))
                item.need_subsidy_potato = float(request.POST.get(str(item.id) + "_need_subsidy_potato_" + str(item.need_subsidy_potato)))

                item.sum_estimated_cereals_f = float(request.POST.get(str(item.id) + "_sum_estimated_cereals_f_" + str(item.sum_estimated_cereals_f)))
                item.sum_estimated_vegetables_f = float(request.POST.get(str(item.id) + "_sum_estimated_vegetables_f_" + str(item.sum_estimated_vegetables_f)))
                item.sum_estimated_potato_f = float(request.POST.get(str(item.id) + "_sum_estimated_potato_f_" + str(item.sum_estimated_potato_f)))

                item.sum_provided_cereals_f = float(request.POST.get(str(item.id) + "_sum_provided_cereals_f_" + str(item.sum_provided_cereals_f)))
                item.sum_provided_vegetables_f = float(request.POST.get(str(item.id) + "_sum_provided_vegetables_f_" + str(item.sum_provided_vegetables_f)))
                item.sum_provided_potato_f = float(request.POST.get(str(item.id) + "_sum_provided_potato_f_" + str(item.sum_provided_potato_f)))

                item.sum_due_cereals_f = float(request.POST.get(str(item.id) + "_sum_due_cereals_f_" + str(item.sum_due_cereals_f)))
                item.sum_due_vegetables_f = float(request.POST.get(str(item.id) + "_sum_due_vegetables_f_" + str(item.sum_due_vegetables_f)))
                item.sum_due_vpotato_f = float(request.POST.get(str(item.id) + "_sum_due_vpotato_f_" + str(item.sum_due_vpotato_f)))

                item.sum_estimated_cereals_k = float(request.POST.get(str(item.id) + "_sum_estimated_cereals_k_" + str(item.sum_estimated_cereals_k)))
                item.sum_estimated_vegetables_k = float(request.POST.get(str(item.id) + "_sum_estimated_vegetables_k_" + str(item.sum_estimated_vegetables_k)))
                item.sum_estimated_potato_k = float(request.POST.get(str(item.id) + "_sum_estimated_potato_k_" + str(item.sum_estimated_potato_k)))

                item.sum_provided_cereals_k = float(request.POST.get(str(item.id) + "_sum_provided_cereals_k_" + str(item.sum_provided_cereals_k)))
                item.sum_provided_vegetables_k = float(request.POST.get(str(item.id) + "_sum_provided_vegetables_k_" + str(item.sum_provided_vegetables_k)))
                item.sum_provided_potato_k = float(request.POST.get(str(item.id) + "_sum_provided_potato_k_" + str(item.sum_provided_potato_k)))

                item.sum_due_cereals_k = float(request.POST.get(str(item.id) + "_sum_due_cereals_k_" + str(item.sum_due_cereals_k)))
                item.sum_due_vegetables_k = float(request.POST.get(str(item.id) + "_sum_due_vegetables_k_" + str(item.sum_due_vegetables_k)))
                item.sum_due_vpotato_k = float(request.POST.get(str(item.id) + "_sum_due_vpotato_k_" + str(item.sum_due_vpotato_k)))
                item.save()

            str_document_path = "/ssrs/" + "сводная справка - расчет_23_"+str(model_item.id) + ".xlsx"
            save_nesvaska_ssr_to_excel(obj_nesvaska_ssr, model_item, str_document_path)

            model_item.excel_document_path = str_document_path
            model_item.name_period = request.POST.get("nesvaska_ssr_name_period")
            model_item.fio_spec_oib  = request.POST.get("nesvaska_ssr_fio_spec_oib")
            model_item.phone_spec_oib = request.POST.get("nesvaska_ssr_phone_spec_oib")
            model_item.fio_deputy_minister = request.POST.get("nesvaska_ssr_fio_deputy_minister")
            model_item.change_history = model_item.change_history + "Внесены изменения в сводную справку - расчет: " + str(datetime.now()) + ", " + str(request.user) + "\n"
            model_item.save()

            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])

        if (request.method == 'POST' and 'nesvaska_ssr_approve' in request.POST):
            model_item.f_status = 'ss2'
            model_item.data_adopt = datetime.now()
            model_item.change_history = model_item.change_history + "Утверждение сводной справки - расчет: " + str(datetime.now()) + ", " + str(request.user) + "\n"
            model_item.save()

            item_requests= Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                   num_subsid = '23', used_ssr = False,
                                                   report_year_subsid = int(model_item.report_year) - 1)
            for item in item_requests:
                print(item)
                item.used_ssr = True
                item.save()


        return render(request, "ssr/nesvaska/edit_nesvaska_ssr.html",
                      {'obj_nesvaska_ssr': obj_nesvaska_ssr,
                       'obj_ssr': model_item,
                       'name_title': name_title,
                       'total_area': total_sum_nesvaska[0],
                       'total_area_proekt': total_sum_nesvaska[1],
                       'total_area_insurance': total_sum_nesvaska[2],
                       'total_need': total_sum_nesvaska[3],
                       'total_estimated_f': total_sum_nesvaska[4],
                       'total_provided_f': total_sum_nesvaska[5],
                       'total_due_f': total_sum_nesvaska[6],
                       'total_estimated_k': total_sum_nesvaska[7],
                       'total_provided_k': total_sum_nesvaska[8],
                       'total_due_k': total_sum_nesvaska[9]})
    else:
        print("другая")
        return redirect(request.META['HTTP_REFERER'])

def delete_ssr(request):
    try:
        id_deleted_ssr = request.POST.get("selected_td_ssrs")
        model_item = SSR.objects.get(id=int(id_deleted_ssr))
        model_item.removed = True
        model_item.change_history = "Дата удаления: " + str(datetime.now()) + ", " + str(request.user) + ";" + "\n"
        model_item.save()
        messages.success(request, "Ваше действие успешно выполнено!")
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])
    return redirect(request.META['HTTP_REFERER'])