from django.shortcuts import render
from django.http import HttpResponseNotFound
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages

def add_user_system(request):
    if (request.method == 'POST' and 'save_user_system' in request.POST):
        try:
            id_type_org = request.POST.get("f_type")
            model_item_type = TypeUser.objects.get(id=int(id_type_org))
            item = UserSystem(
                login_user = request.POST.get("login_user"),
                password_user = request.POST.get("password"),
                last_name_user = request.POST.get("last_name_user"),
                first_name_user = request.POST.get("first_name_user"),
                patronymic_user = request.POST.get("patronymic_user"),
                f_type = model_item_type,
                attached_org = False,
            )
            item.save()
            messages.success(request, "Ваше действие успешно выполнено! Пользователь добавлен!")
            return redirect(request.META['HTTP_REFERER'])
        except:
            messages.success(request, "Пользователь с таким логином уже существует! Попробуйте ввести другой логин!")
            form = UserSystemForm(request.POST)
            return render(request, "Users_system/add_user_system.html", {'form': form})
    else:
        form = UserSystemForm(request.POST)
        return render(request, "Users_system/add_user_system.html", {'form': form})

def show_users_system(request):
    model_item = User.objects.all()
    return render(request, "Users_system/show_users_system.html", {'model_item': model_item})

def delete_user_system(request):
    try:
        id_selected_user_system = request.POST.get("selected_td_user_system")
        model_item = UserSystem.objects.get(id=int(id_selected_user_system))
        model_item.delete()
        model_item = UserSystem.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")

def edit_user_system(request, id):
    try:
        model_item = UserSystem.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            name_type_user = request.POST.get("f_type_user")
            model_item_type_user = TypeUser.objects.get(name_type_user=name_type_user)

            model_item.login_user = request.POST.get("login_user")
            model_item.password_user = request.POST.get("password")
            model_item.last_name_user = request.POST.get("last_name_user")
            model_item.first_name_user = request.POST.get("first_name_user")
            model_item.patronymic_user = request.POST.get("patronymic_user")
            model_item.f_type = model_item_type_user
            model_item.attached_org = False

            model_item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        else:
            type_user = TypeUser.objects.all().order_by("name_type_user")
            return render(request, "Users_system/edit_user_system.html", {'model_item': model_item,
                                                                                 'f_type_user': type_user})
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")

def show_form_users_system(request):
    return render(request, "Users_system/show_form_users_system.html")

def add_user_in_org(request):
    if (request.method == 'POST' and 'save_user_in_org' in request.POST):
        f_org = request.POST.get("f_org")
        data_org = f_org.split(' ')
        data_inn_org = data_org[0]
        model_org = Organization.objects.get(inn_org = data_inn_org)

        f_user = request.POST.get("f_user")
        data_user = f_user.split(' ')
        data_login_user = data_user[0]
        model_user = User.objects.get(username = data_login_user)

        model_user_profile = UserProfile.objects.get(user = str(model_user.id))
        print(model_user_profile)

        model_item = OrganizationWithUser(f_org = model_org, f_user = model_user_profile)
        model_item.save()

        model_user_profile.attached_org = True
        model_user_profile.save()

        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    else:
        f_org = Organization.objects.all().order_by("full_name_org")
        f_user = UserProfile.objects.filter(attached_org = False)

        return render(request, "Users_system/add_user_in_org.html", {'f_org': f_org, 'f_user': f_user})

def show_users_in_org(request):
    model_item = OrganizationWithUser.objects.all()
    return render(request, "Users_system/show_users_in_org.html", {'model_item': model_item})

def search_users_by_org(request):
    if (request.method == 'POST' and 'find_users' in request.POST):
        f_org = request.POST.get("f_org")
        data_org = f_org.split(' ')
        data_inn_org = data_org[0]
        model_org = Organization.objects.get(inn_org = data_inn_org)
        model_users_by_org = OrganizationWithUser.objects.filter(f_org = model_org.id)
        return render(request, "Users_system/show_found_users_by_org.html", {'name_org': model_org.full_name_org,
                                                                             'model_users_by_org': model_users_by_org})
    else:
        f_org = Organization.objects.all().order_by("full_name_org")
        return render(request, "Users_system/search_users_by_org.html", {'f_org': f_org})











