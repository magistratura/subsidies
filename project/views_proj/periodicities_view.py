from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError

def add_periodicity(request):
    if (request.method == 'POST' and 'save_periodicity' in request.POST):
        try:
            form = PeriodicityForm(request.POST)
            form.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except:
            messages.warning(request, "Периодичность с таким названием уже существует! Введите другое название!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = PeriodicityForm(request.POST)
        return render(request, "guides/Periodicities/add_periodicity.html", {'form': form})

def show_periodicities(request):
    model_item = Periodicity.objects.all()
    return render(request, "guides/Periodicities/show_periodicities.html", {'model_item': model_item})

def delete_periodicity(request):
    try:
        id_selected_periodicity = request.POST.get("selected_td_periodicity")
        model_item = Periodicity.objects.get(id=int(id_selected_periodicity))
        model_item.delete()
        model_item = Periodicity.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_periodicity(request, id):
    try:
        model_item = Periodicity.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            try:
                model_item.name_period = request.POST.get("name_period")
                model_item.save()
                messages.success(request, "Ваше действие успешно выполнено!")
                return redirect(request.META['HTTP_REFERER'])
            except IntegrityError:
                messages.warning(request, "Периодичность с таким названием уже существует! Введите другое название!")
                return redirect(request.META['HTTP_REFERER'])
        else:
            return render(request, "guides/Periodicities/edit_periodicity.html", {'model_item': model_item})
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])
