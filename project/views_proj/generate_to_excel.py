import openpyxl
from openpyxl.utils import get_column_letter
from project.views_proj.request_eight_article import *
from django.db.models import Q
from openpyxl.styles import Border, Side, PatternFill, Font, Alignment

def save_nesvaska_to_excel(name_analytics_reports_obj, id):
    my_wb = openpyxl.Workbook()
    my_sheet = my_wb.active
    my_sheet.title = 'Овощи'

    ft = Font(size=12, name='Times New Roman')
    ft_b = Font(size=12, name='Times New Roman', bold=True)

    thin_border = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))

    my_sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=6)
    my_sheet.cell(row=1, column=1).font = ft_b
    my_sheet.row_dimensions[1].height = 52

    c1 = my_sheet.cell(row=1, column=1)
    c1.value = "Перечень сельскохозяйственных товаропроизводителей, претендующих на возмещение части затрат на проведение агротехнологических работ, повышение уровня экологической безопасности сельскохозяйственного производства, а также на повышение плодородия и качества почв (овощи)"
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='top')

    i = 1
    for item in range(6):
        c1 = my_sheet.cell(row=2, column = i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = my_sheet.cell(row=2, column=1)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "№"

    c1 = my_sheet.cell(row=2, column=2)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Муниципальный район"
    my_sheet.column_dimensions[get_column_letter(2)].width = 30

    c1 = my_sheet.cell(row=2, column=3)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    my_sheet.column_dimensions[get_column_letter(3)].width = 43

    c1 = my_sheet.cell(row=2, column=4)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    my_sheet.column_dimensions[get_column_letter(4)].width = 29

    c1 = my_sheet.cell(row=2, column=5)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Ставка субсидии на 1 гектар"
    my_sheet.column_dimensions[get_column_letter(5)].width = 20

    c1 = my_sheet.cell(row=2, column=6)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Размер субсидии, предоставляемый получателю"
    my_sheet.column_dimensions[get_column_letter(6)].width = 20

    nesvaska_reports_obj_vegetables = Nesvaska_Reports.objects.filter(~Q(vegetables = 0),
        f_reports_requests__f_report_request__report_year=name_analytics_reports_obj.report_year,
        f_reports_requests__f_report_request__num_article="23",
        f_reports_requests__status_report='sr6')

    info_nesvaska = Info_Nesvaska_Calc_Koefficient.objects.get(report_year = name_analytics_reports_obj.report_year)

    i = 3
    num = 1
    for item in range(nesvaska_reports_obj_vegetables.count()):
        c1 = my_sheet.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_vegetables:
        c1 = my_sheet.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_vegetables:
        c1 = my_sheet.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_vegetables:
        c1 = my_sheet.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.vegetables
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    for item in range(nesvaska_reports_obj_vegetables.count()):
        c1 = my_sheet.cell(row=i, column=5)
        c1.number_format = '0.00'
        c1.value = info_nesvaska.rate_vegetables
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    total_sum_vegetables = 0
    for item in nesvaska_reports_obj_vegetables:
        c1 = my_sheet.cell(row=i, column=6)
        c1.number_format = '0.00'
        sum = round(item.vegetables * info_nesvaska.rate_vegetables, 2)
        c1.value = sum
        total_sum_vegetables = total_sum_vegetables + sum
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_vegetables.count() + 3

    c1 = my_sheet.cell(row=current_row, column=1)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, vertical='bottom')
    c1.value = "Итого"

    c1 = my_sheet.cell(row=current_row, column=2)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = my_sheet.cell(row=current_row, column=3)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = my_sheet.cell(row=current_row, column=4)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.000'
    c1.value = info_nesvaska.total_area_vegetables

    c1 = my_sheet.cell(row=current_row, column=5)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = my_sheet.cell(row=current_row, column=6)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.00'
    c1.value = total_sum_vegetables

    start_title = 2
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 6:
            my_sheet.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    ws_cereals = my_wb.create_sheet("Зерновые")
    ws_cereals.merge_cells(start_row=1, start_column=1, end_row=1, end_column=6)
    ws_cereals.cell(row=1, column=1).font = ft_b
    ws_cereals.row_dimensions[1].height = 52

    c1 = ws_cereals.cell(row=1, column=1)
    c1.value = "Перечень сельскохозяйственных товаропроизводителей, претендующих на возмещение части затрат на проведение агротехнологических работ, повышение уровня экологической безопасности сельскохозяйственного производства, а также на повышение плодородия и качества почв ((зерновые, зернобобовые, масличные (за исключением рапса и сои))"
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='top')

    i = 1
    for item in range(6):
        c1 = ws_cereals.cell(row=2, column = i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = ws_cereals.cell(row=2, column=1)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "№"

    c1 = ws_cereals.cell(row=2, column=2)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Муниципальный район"
    ws_cereals.column_dimensions[get_column_letter(2)].width = 30

    c1 = ws_cereals.cell(row=2, column=3)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    ws_cereals.column_dimensions[get_column_letter(3)].width = 43

    c1 = ws_cereals.cell(row=2, column=4)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    ws_cereals.column_dimensions[get_column_letter(4)].width = 29

    c1 = ws_cereals.cell(row=2, column=5)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Ставка субсидии на 1 гектар"
    ws_cereals.column_dimensions[get_column_letter(5)].width = 20

    c1 = ws_cereals.cell(row=2, column=6)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Размер субсидии, предоставляемый получателю"
    ws_cereals.column_dimensions[get_column_letter(6)].width = 20

    nesvaska_reports_obj_cereals = Nesvaska_Reports.objects.filter(~Q(cereals = 0),
        f_reports_requests__f_report_request__report_year=name_analytics_reports_obj.report_year,
        f_reports_requests__f_report_request__num_article="23",
        f_reports_requests__status_report='sr6')

    i = 3
    num = 1
    for item in range(nesvaska_reports_obj_cereals.count()):
        c1 = ws_cereals.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.cereals + item.oilseeds
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    for item in range(nesvaska_reports_obj_cereals.count()):
        c1 = ws_cereals.cell(row=i, column=5)
        c1.number_format = '0.00'
        c1.value = info_nesvaska.rate_cereals
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    total_sum_cereals = 0
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals.cell(row=i, column=6)
        c1.number_format = '0.00'
        sum = round((item.cereals + item.oilseeds) * info_nesvaska.rate_cereals, 2)
        c1.value = sum
        total_sum_cereals = total_sum_cereals + sum
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_cereals.count() + 3

    c1 = ws_cereals.cell(row=current_row, column=1)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, vertical='bottom')
    c1.value = "Итого"

    c1 = ws_cereals.cell(row=current_row, column=2)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_cereals.cell(row=current_row, column=3)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_cereals.cell(row=current_row, column=4)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.000'
    c1.value = info_nesvaska.total_area_cereals

    c1 = ws_cereals.cell(row=current_row, column=5)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_cereals.cell(row=current_row, column=6)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.00'
    c1.value = total_sum_cereals

    start_title = 2
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 6:
            ws_cereals.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    ws_potato = my_wb.create_sheet("Картофель")
    ws_potato.merge_cells(start_row=1, start_column=1, end_row=1, end_column=6)
    ws_potato.cell(row=1, column=1).font = ft_b
    ws_potato.row_dimensions[1].height = 52

    c1 = ws_potato.cell(row=1, column=1)
    c1.value = "Перечень сельскохозяйственных товаропроизводителей, претендующих на возмещение части затрат на проведение  агротехнологических работ, повышение уровня экологической безопасности сельскохозяйственного производства, а также на повышение плодородия и качества почв (картофель)"
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='top')

    i = 1
    for item in range(6):
        c1 = ws_potato.cell(row=2, column = i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = ws_potato.cell(row=2, column=1)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "№"

    c1 = ws_potato.cell(row=2, column=2)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Муниципальный район"
    ws_potato.column_dimensions[get_column_letter(2)].width = 30

    c1 = ws_potato.cell(row=2, column=3)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    ws_potato.column_dimensions[get_column_letter(3)].width = 43

    c1 = ws_potato.cell(row=2, column=4)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    ws_potato.column_dimensions[get_column_letter(4)].width = 29

    c1 = ws_potato.cell(row=2, column=5)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Ставка субсидии на 1 гектар"
    ws_potato.column_dimensions[get_column_letter(5)].width = 20

    c1 = ws_potato.cell(row=2, column=6)
    c1.fill = PatternFill("solid", fgColor="c2d69a")
    c1.value = "Размер субсидии, предоставляемый получателю"
    ws_potato.column_dimensions[get_column_letter(6)].width = 20

    nesvaska_reports_obj_potato = Nesvaska_Reports.objects.filter(~Q(potato = 0),
        f_reports_requests__f_report_request__report_year=name_analytics_reports_obj.report_year,
        f_reports_requests__f_report_request__num_article="23",
        f_reports_requests__status_report='sr6')

    i = 3
    num = 1
    for item in range(nesvaska_reports_obj_potato.count()):
        c1 = ws_potato.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 3
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.potato
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    for item in range(nesvaska_reports_obj_potato.count()):
        c1 = ws_potato.cell(row=i, column=5)
        c1.number_format = '0.00'
        c1.value = info_nesvaska.rate_potato
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    i = 3
    total_sum_potato = 0
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato.cell(row=i, column=6)
        c1.number_format = '0.00'
        sum = round(item.potato * info_nesvaska.rate_potato, 2)
        c1.value = sum
        total_sum_potato = total_sum_potato + sum
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_potato.count() + 3

    c1 = ws_potato.cell(row=current_row, column=1)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, vertical='bottom')
    c1.value = "Итого"

    c1 = ws_potato.cell(row=current_row, column=2)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_potato.cell(row=current_row, column=3)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_potato.cell(row=current_row, column=4)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.000'
    c1.value = info_nesvaska.total_area_potato

    c1 = ws_potato.cell(row=current_row, column=5)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.value = "X"

    c1 = ws_potato.cell(row=current_row, column=6)
    c1.font = ft_b
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    c1.number_format = '0.00'
    c1.value = total_sum_potato

    start_title = 2
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 6:
            ws_potato.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    ws_vegetables_order = my_wb.create_sheet("Приложение к приказу (Овощи)")

    i = 1
    for item in range(4):
        c1 = ws_vegetables_order.cell(row=1, column = i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = ws_vegetables_order.cell(row=1, column=1)
    c1.value = "№"

    c1 = ws_vegetables_order.cell(row=1, column=2)
    c1.value = "Муниципальный район"
    ws_vegetables_order.column_dimensions[get_column_letter(2)].width = 30

    c1 = ws_vegetables_order.cell(row=1, column=3)
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    ws_vegetables_order.column_dimensions[get_column_letter(3)].width = 43

    c1 = ws_vegetables_order.cell(row=1, column=4)
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    ws_vegetables_order.column_dimensions[get_column_letter(4)].width = 29

    i = 2
    num = 1
    for item in range(nesvaska_reports_obj_vegetables.count()):
        c1 = ws_vegetables_order.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_vegetables:
        c1 = ws_vegetables_order.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_vegetables:
        c1 = ws_vegetables_order.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_vegetables:
        c1 = ws_vegetables_order.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.vegetables
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_vegetables.count() + 1

    start_title = 1
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 4:
            ws_vegetables_order.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1


    ws_cereals_order = my_wb.create_sheet("Приложение к приказу (Зерновые)")

    i = 1
    for item in range(4):
        c1 = ws_cereals_order.cell(row=1, column = i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = ws_cereals_order.cell(row=1, column=1)
    c1.value = "№"

    c1 = ws_cereals_order.cell(row=1, column=2)
    c1.value = "Муниципальный район"
    ws_cereals_order.column_dimensions[get_column_letter(2)].width = 30

    c1 = ws_cereals_order.cell(row=1, column=3)
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    ws_cereals_order.column_dimensions[get_column_letter(3)].width = 43

    c1 = ws_cereals_order.cell(row=1, column=4)
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    ws_cereals_order.column_dimensions[get_column_letter(4)].width = 29

    i = 2
    num = 1
    for item in range(nesvaska_reports_obj_cereals.count()):
        c1 = ws_cereals_order.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals_order.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals_order.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_cereals:
        c1 = ws_cereals_order.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.all_whole_crop
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_cereals.count() + 1

    start_title = 1
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 4:
            ws_cereals_order.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    ws_potato_order = my_wb.create_sheet("Прил. к приказу (Картофель)")

    i = 1
    for item in range(4):
        c1 = ws_potato_order.cell(row=1, column=i)
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    c1 = ws_potato_order.cell(row=1, column=1)
    c1.value = "№"

    c1 = ws_potato_order.cell(row=1, column=2)
    c1.value = "Муниципальный район"
    ws_potato_order.column_dimensions[get_column_letter(2)].width = 30

    c1 = ws_potato_order.cell(row=1, column=3)
    c1.value = "Наименование сельскохозяйственного товаропроизводителя, претендующего на получение субсидии"
    ws_potato_order.column_dimensions[get_column_letter(3)].width = 43

    c1 = ws_potato_order.cell(row=1, column=4)
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    ws_potato_order.column_dimensions[get_column_letter(4)].width = 29

    i = 2
    num = 1
    for item in range(nesvaska_reports_obj_potato.count()):
        c1 = ws_potato_order.cell(row=i, column=1)
        c1.value = num
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        num = num + 1
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato_order.cell(row=i, column=2)
        c1.value = item.f_reports_requests.f_organization.f_reg_area.name_area
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato_order.cell(row=i, column=3)
        c1.value = item.f_reports_requests.f_organization.full_name_org
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
        i = i + 1

    i = 2
    for item in nesvaska_reports_obj_potato:
        c1 = ws_potato_order.cell(row=i, column=4)
        c1.number_format = '0.000'
        c1.value = item.potato
        c1.font = ft
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
        i = i + 1

    current_row = nesvaska_reports_obj_potato.count() + 1

    start_title = 1
    end_title = current_row

    while start_title <= end_title:
        start_column = 1
        while start_column <= 4:
            ws_potato_order.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1

    str_document_path = "/analytics_reports/" + "несвязная_поддержка_аналитика_" + str(id) + ".xlsx"
    my_wb.save("media" + str_document_path)

    return str_document_path

