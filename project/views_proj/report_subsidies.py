from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import *
import datetime
from project.views_proj.report_for_sub_nesvaska import *
from project.views_proj.generate_to_word_view import *
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger


def choose_report_request(request):
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

    if (request.method == 'POST' and 'choose_report_request' in request.POST):
        select_name_report_request = request.POST.get("combo_name_reports_requests")

        name_reports_requests_obj = Name_Reports_Requests.objects.get(name_report_request = select_name_report_request)
        if (name_reports_requests_obj.num_article == "23"):
            reports_requests_exist = Reports_Requests.objects.filter(f_report_request = name_reports_requests_obj,
                                                                     f_organization = organization_obj,
                                                                     removed = False)
            if reports_requests_exist.count() !=0:
                messages.warning(request, "Отчет по субсидии уже был создан!")
                return redirect(request.META['HTTP_REFERER'])
            return HttpResponseRedirect(reverse('add_nesvaska_report', args=(name_reports_requests_obj.id,)))
        else:
            print("другая статья")

    date_now = date.today()

    if organization_obj.f_reg_area.type_area == "type1":
        name_reports_requests = Name_Reports_Requests.objects.filter(data_start_recive__lte=date_now, data_end_recive__gte=date_now)
    else:
        name_reports_requests = Name_Reports_Requests.objects.filter(data_start_recive_city__lte=date_now, data_end_recive_city__gte=date_now)

    if name_reports_requests.count() == 0:
        messages.warning(request, "Нет ни одного доступного отчета по субсидии для сдачи!")


    # name_reports_requests = Name_Reports_Requests.objects.all()
    return render(request, "reports/choose_report_request.html", {'name_reports_requests': name_reports_requests})

def show_actions_requests_for_subsidies(request):
    return render(request, "reports/show/show_actions_requests_subsidies.html")

def create_reports_requests(user, report_request, organization_obj):
    reports_requests_obj = Reports_Requests()
    datetime_current = datetime.datetime.now()

    reports_requests_obj.history_reports_requests = 'Дата создания: ' + str(datetime_current) + ', пользователь: ' + str(user) + ';'
    reports_requests_obj.data_create = datetime_current
    reports_requests_obj.f_report_request = report_request
    reports_requests_obj.f_organization = organization_obj
    reports_requests_obj.removed = False
    reports_requests_obj.status_report = 'sr1'
    reports_requests_obj.save()
    return reports_requests_obj

import pythoncom
def add_nesvaska_report(request, id):
    name_reports_requests = Name_Reports_Requests.objects.get(id = id)
    if (request.method == 'POST' and 'create_nesvaska_report' in request.POST):
        authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
        organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

        cereals = round(float(request.POST.get("cereals")),3)
        oilseeds = round(float(request.POST.get("oilseeds")),3)
        potato = round(float(request.POST.get("potato")),3)
        vegetables = round(float(request.POST.get("vegetables")),3)

        reports_requests_obj = create_reports_requests(request.user, name_reports_requests, organization_obj)
        report_nesvaska_obj = create_report_nesvaska(cereals, oilseeds, potato, vegetables, reports_requests_obj)
        pythoncom.CoInitializeEx(0)
        print_document_path = generate_to_word_report_nesvaska(report_nesvaska_obj, organization_obj)
        reports_requests_obj.print_document_path = print_document_path
        reports_requests_obj.save()

        messages.success(request, "Ваше действие успешно выполнено!")
        return HttpResponseRedirect(reverse('edit_nesvaska_report', args=(reports_requests_obj.id,)))

    return render(request, "reports/nesvaska_report/add_nesvaska_report.html", {'name_reports_requests': name_reports_requests})

def edit_nesvaska_report(request, id):
    reports_requests_obj = Reports_Requests.objects.get(id = id)
    nesvaska_reports_obj = Nesvaska_Reports.objects.get(f_reports_requests = reports_requests_obj.id)
    str_document_pdf = "/media" + str(reports_requests_obj.print_document_path)
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)
    if (request.method == 'POST' and 'save_nesvaska_report' in request.POST):
        cereals = round(float(request.POST.get("cereals")),3)
        oilseeds = round(float(request.POST.get("oilseeds")),3)
        potato = round(float(request.POST.get("potato")),3)
        vegetables = round(float(request.POST.get("vegetables")),3)
        edit_report_nesvaska(nesvaska_reports_obj, cereals, oilseeds, potato, vegetables)
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    if (request.method == 'POST' and 'resave_nesvaska_report' in request.POST):
        pythoncom.CoInitializeEx(0)
        cereals = round(float(request.POST.get("cereals")),3)
        oilseeds = round(float(request.POST.get("oilseeds")),3)
        potato = round(float(request.POST.get("potato")),3)
        vegetables = round(float(request.POST.get("vegetables")),3)
        edit_report_nesvaska(nesvaska_reports_obj, cereals, oilseeds, potato, vegetables)
        print_document_path = generate_to_word_report_nesvaska(nesvaska_reports_obj, organization_obj)
        nesvaska_reports_obj.print_document_path = print_document_path
        nesvaska_reports_obj.save()
        messages.success(request, "Ваше действие успешно выполнено!")

    return render(request, "reports/nesvaska_report/edit_nesvaska_report.html", {'model_item': nesvaska_reports_obj,
                                                                                 'str_document_pdf': str_document_pdf,
                                                                                 'reports_requests_obj': reports_requests_obj})
def show_report_subsidy(request, id):
    reports_requests_obj = Reports_Requests.objects.get(id = id)
    status_request_report = reports_requests_obj.get_status_report_display()
    status_action = ""

    status_variant = {'Новая': 'Отправить',
                      'Отказано': 'Изменить отчет',
                      'Отправлено': 'Проверить МО',
                      'Проверено МО': 'Согласовать с ОТР',
                      'Проверено ОТР': 'Принять',
                      'Принято': 'Принято'}

    for key, value in status_variant.items():
        if (key == status_request_report):
            status_action = value

    start_city = reports_requests_obj.f_report_request.data_start_recive_city
    end_city = reports_requests_obj.f_report_request.data_end_recive_city

    start_region = reports_requests_obj.f_report_request.data_start_recive
    end_region = reports_requests_obj.f_report_request.data_end_recive

    date_now = date.today()

    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    type_are_org = authorized_user.f_org.f_reg_area.type_area



    if type_are_org == "type1":
        if (date_now >= start_region and date_now <= end_region):
            deadline_admission = "possible"
        else:
            deadline_admission = "unpossible"
    else:
        if (date_now >= start_city and date_now <= end_city):
            deadline_admission = "possible"
        else:
            deadline_admission = "unpossible"

    return render(request, "reports/show/show_report_subsidy.html", {'model_item_reports_requests': reports_requests_obj, 'status_action_report': status_action,
                                                                     'status_request_report': status_request_report, 'deadline_admission_report': deadline_admission,
                                                                     'removed_condition': str(reports_requests_obj.removed)})

def show_reports_subsidies(request):
    active_reports_requests = Reports_Requests.objects.filter(removed = False)
    page = request.GET.get('page', 1)
    paginator = Paginator(active_reports_requests, 5)
    try:
        active_reports_requests = paginator.page(page)
    except PageNotAnInteger:
        active_reports_requests = paginator.page(1)
    except EmptyPage:
        active_reports_requests = paginator.page(paginator.num_pages)
    return render(request, "reports/show/show_reports_subsidies.html", {'model_item': active_reports_requests})

def show_remove_reports_subsidies(request):
    active_reports_requests = Reports_Requests.objects.filter(removed = True)
    page = request.GET.get('page', 1)
    paginator = Paginator(active_reports_requests, 5)
    try:
        active_reports_requests = paginator.page(page)
    except PageNotAnInteger:
        active_reports_requests = paginator.page(1)
    except EmptyPage:
        active_reports_requests = paginator.page(paginator.num_pages)
    return render(request, "reports/show/show_reports_subsidies.html", {'model_item': active_reports_requests})

def delete_report_subsidy(request, id):
    reports_requests_obj = Reports_Requests.objects.get(id = id)
    reports_requests_obj.removed = True
    reports_requests_obj.save()
    messages.success(request, "Ваше действие успешно выполнено!")
    return redirect(request.META['HTTP_REFERER'])

def show_report_sub(request, id):
    reports_requests_obj = Reports_Requests.objects.get(id = id)
    num_article = (request.POST.get("num_article"))
    if num_article == "23":
        return HttpResponseRedirect(reverse('edit_nesvaska_report', args=(reports_requests_obj.id,)))
    else:
        print("другая статья")
    return redirect(request.META['HTTP_REFERER'])




