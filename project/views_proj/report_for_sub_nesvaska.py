from project.forms import *
import datetime

def create_report_nesvaska(cereals, oilseeds, potato, vegetables, reports_requests_obj):
    nesvaska_reports_obj = Nesvaska_Reports()
    nesvaska_reports_obj.cereals = cereals
    nesvaska_reports_obj.oilseeds = oilseeds
    nesvaska_reports_obj.potato = potato
    nesvaska_reports_obj.vegetables = vegetables
    nesvaska_reports_obj.all_whole_crop = round(cereals + oilseeds, 3)
    nesvaska_reports_obj.f_reports_requests = reports_requests_obj
    nesvaska_reports_obj.save()
    return nesvaska_reports_obj

def edit_report_nesvaska(nesvaska_reports_obj, cereals, oilseeds, potato, vegetables):
    nesvaska_reports_obj.cereals = cereals
    nesvaska_reports_obj.oilseeds = oilseeds
    nesvaska_reports_obj.potato = potato
    nesvaska_reports_obj.vegetables = vegetables
    nesvaska_reports_obj.all_whole_crop = round(cereals + oilseeds, 3)
    nesvaska_reports_obj.save()



