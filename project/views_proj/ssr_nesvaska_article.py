from project.resources import *
from django.db.models import Q, Sum, F
import datetime
import openpyxl
from datetime import *
from openpyxl.styles import Border, Alignment, Font, Side
from openpyxl.utils import get_column_letter

def create_nesvaska_subs_ssr(nesvaska_ssr_current, item_1):
    obj_item = SSR_Nesvaska_Subs()
    obj_item.f_ssr = nesvaska_ssr_current
    obj_item.name_region = str(item_1.f_organization.f_reg_area.name_area)
    obj_item.name_organization = str(item_1.f_organization.full_name_org)

    obj_item.name_group_cereals = "Зерновые"
    obj_item.name_group_vegetables = "Овощи"
    obj_item.name_group_potato = "Картофель"

    obj_item_requests_nesvaska_subs = Requests_Nesvaska_Subs.objects.get(f_requests=int(item_1.id))

    obj_item.area_cereals = obj_item_requests_nesvaska_subs.all_whole_cereals_oilseeds
    obj_item.area_vegetables = obj_item_requests_nesvaska_subs.total_area_vegetables
    obj_item.area_potato = obj_item_requests_nesvaska_subs.total_area_potato

    obj_item.rate_cereals = obj_item_requests_nesvaska_subs.rate_cereals
    obj_item.rate_vegetables = obj_item_requests_nesvaska_subs.rate_vegetables
    obj_item.rate_potato = obj_item_requests_nesvaska_subs.rate_potato

    obj_item.area_proekt_cereals = 0
    obj_item.area_proekt_vegetables = 0
    obj_item.area_proekt_potato = 0

    obj_item.area_insurance_cereals = 0
    obj_item.area_insurance_vegetables = 0
    obj_item.area_insurance_potato = 0

    obj_item.need_subsidy_cereals = obj_item_requests_nesvaska_subs.sum_cereals
    obj_item.need_subsidy_vegetables = obj_item_requests_nesvaska_subs.sum_vegetables
    obj_item.need_subsidy_potato = obj_item_requests_nesvaska_subs.sum_potato

    coefficient_а = 0.75
    sum_estimated_cereals_f = round(coefficient_а * obj_item_requests_nesvaska_subs.sum_cereals, 2)
    obj_item.sum_estimated_cereals_f = sum_estimated_cereals_f
    sum_estimated_vegetables_f = round(coefficient_а * obj_item_requests_nesvaska_subs.sum_vegetables, 2)
    obj_item.sum_estimated_vegetables_f = sum_estimated_vegetables_f
    sum_estimated_potato_f = round(coefficient_а * obj_item_requests_nesvaska_subs.sum_potato, 2)
    obj_item.sum_estimated_potato_f = sum_estimated_potato_f

    sum_estimated_cereals_k = round(obj_item_requests_nesvaska_subs.sum_cereals - sum_estimated_cereals_f, 2)
    obj_item.sum_estimated_cereals_k = sum_estimated_cereals_k
    sum_estimated_vegetables_k = round(obj_item_requests_nesvaska_subs.sum_vegetables - sum_estimated_vegetables_f, 2)
    obj_item.sum_estimated_vegetables_k = sum_estimated_vegetables_k
    sum_estimated_potato_k = round(obj_item_requests_nesvaska_subs.sum_potato - sum_estimated_potato_f, 2)
    obj_item.sum_estimated_potato_k = sum_estimated_potato_k

    obj_item.sum_provided_cereals_f = 0
    obj_item.sum_provided_vegetables_f = 0
    obj_item.sum_provided_potato_f = 0

    obj_item.sum_due_cereals_f = sum_estimated_cereals_f
    obj_item.sum_due_vegetables_f = sum_estimated_vegetables_f
    obj_item.sum_due_vpotato_f = sum_estimated_potato_f

    obj_item.sum_provided_cereals_k = 0
    obj_item.sum_provided_vegetables_k = 0
    obj_item.sum_provided_potato_k = 0

    obj_item.sum_due_cereals_k = sum_estimated_cereals_k
    obj_item.sum_due_vegetables_k = sum_estimated_vegetables_k
    obj_item.sum_due_vpotato_k = sum_estimated_potato_k

    obj_item.save()

def to_paid_nesvaska_subs_ssr(nesvaska_subs_ssr_current):
    for item in nesvaska_subs_ssr_current:
        item.sum_provided_cereals_f = item.sum_estimated_cereals_f
        item.sum_provided_vegetables_f = item.sum_estimated_vegetables_f
        item.sum_provided_potato_f = item.sum_estimated_potato_f

        item.sum_provided_cereals_k = item.sum_estimated_cereals_k
        item.sum_provided_vegetables_k = item.sum_estimated_vegetables_k
        item.sum_provided_potato_k = item.sum_estimated_potato_k

        item.sum_due_cereals_f = 0
        item.sum_due_vegetables_f = 0
        item.sum_due_vpotato_f = 0

        item.sum_due_cereals_k = 0
        item.sum_due_vegetables_k = 0
        item.sum_due_vpotato_k = 0

        item.save()

def create_ssr_nesvaska_subs(first_time, obj_list_name_sxp, num_article, select_year_article, ssr_model_item,
                          nesvaska_ssr_previous, nesvaska_ssr_current):
    if first_time == True:
        for item in obj_list_name_sxp:
            ssr_item_nesvaska_subs = SSR_Nesvaska_Subs()
            print("select_year_article")
            print(select_year_article)
            obj_list_requests_accrued = Requests.objects.get(Q(f_status='s6') | Q(f_status='s7'),
                                                                num_subsid=int(num_article),
                                                                report_year_subsid=int(select_year_article),
                                                                f_organization__full_name_org=item)
            print(obj_list_requests_accrued)

            ssr_item_nesvaska_subs.f_ssr = ssr_model_item
            ssr_item_nesvaska_subs.name_region = obj_list_requests_accrued.f_organization.f_reg_area.name_area
            ssr_item_nesvaska_subs.name_organization = item

            ssr_item_nesvaska_subs.name_group_cereals = "Зерновые"
            ssr_item_nesvaska_subs.name_group_vegetables = "Овощи"
            ssr_item_nesvaska_subs.name_group_potato = "Картофель"

            obj_nesvska_subs = Requests_Nesvaska_Subs.objects.get(f_requests=obj_list_requests_accrued.id)
            ssr_item_nesvaska_subs.area_cereals = obj_nesvska_subs.all_whole_cereals_oilseeds
            ssr_item_nesvaska_subs.area_vegetables = obj_nesvska_subs.total_area_vegetables
            ssr_item_nesvaska_subs.area_potato = obj_nesvska_subs.total_area_potato

            ssr_item_nesvaska_subs.rate_cereals = obj_nesvska_subs.rate_cereals
            ssr_item_nesvaska_subs.rate_vegetables = obj_nesvska_subs.rate_vegetables
            ssr_item_nesvaska_subs.rate_potato = obj_nesvska_subs.rate_potato

            ssr_item_nesvaska_subs.area_proekt_cereals = 0
            ssr_item_nesvaska_subs.area_proekt_vegetables = 0
            ssr_item_nesvaska_subs.area_proekt_potato = 0

            ssr_item_nesvaska_subs.area_insurance_cereals = 0
            ssr_item_nesvaska_subs.area_insurance_vegetables = 0
            ssr_item_nesvaska_subs.area_insurance_potato = 0

            ssr_item_nesvaska_subs.need_subsidy_cereals = obj_nesvska_subs.sum_cereals
            ssr_item_nesvaska_subs.need_subsidy_vegetables = obj_nesvska_subs.sum_vegetables
            ssr_item_nesvaska_subs.need_subsidy_potato = obj_nesvska_subs.sum_potato

            coefficient_а = 0.75
            ssr_item_nesvaska_subs.sum_estimated_cereals_f = round(coefficient_а * obj_nesvska_subs.sum_cereals, 2)
            ssr_item_nesvaska_subs.sum_estimated_vegetables_f = round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2)
            ssr_item_nesvaska_subs.sum_estimated_potato_f = round(coefficient_а * obj_nesvska_subs.sum_potato, 2)

            ssr_item_nesvaska_subs.sum_estimated_cereals_k = round(obj_nesvska_subs.sum_cereals - round(coefficient_а * obj_nesvska_subs.sum_cereals, 2), 2)
            ssr_item_nesvaska_subs.sum_estimated_vegetables_k = round(obj_nesvska_subs.sum_vegetables - round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2), 2)
            ssr_item_nesvaska_subs.sum_estimated_potato_k = round(obj_nesvska_subs.sum_potato - round(coefficient_а * obj_nesvska_subs.sum_potato, 2), 2)

            if obj_list_requests_accrued.f_status == 's7':
                ssr_item_nesvaska_subs.sum_provided_cereals_f = round(coefficient_а * obj_nesvska_subs.sum_cereals, 2)
                ssr_item_nesvaska_subs.sum_provided_vegetables_f = round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2)
                ssr_item_nesvaska_subs.sum_provided_potato_f = round(coefficient_а * obj_nesvska_subs.sum_potato, 2)
                ssr_item_nesvaska_subs.sum_due_cereals_f = 0
                ssr_item_nesvaska_subs.sum_due_vegetables_f = 0
                ssr_item_nesvaska_subs.sum_due_vpotato_f = 0

                ssr_item_nesvaska_subs.sum_provided_cereals_k = round(obj_nesvska_subs.sum_cereals - round(coefficient_а * obj_nesvska_subs.sum_cereals, 2), 2)
                ssr_item_nesvaska_subs.sum_provided_vegetables_k = round(obj_nesvska_subs.sum_vegetables - round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2), 2)
                ssr_item_nesvaska_subs.sum_provided_potato_k = round(obj_nesvska_subs.sum_potato - round(coefficient_а * obj_nesvska_subs.sum_potato, 2), 2)
                ssr_item_nesvaska_subs.sum_due_cereals_k = 0
                ssr_item_nesvaska_subs.sum_due_vegetables_k = 0
                ssr_item_nesvaska_subs.sum_due_vpotato_k = 0
            else:
                ssr_item_nesvaska_subs.sum_provided_cereals_f = 0
                ssr_item_nesvaska_subs.sum_provided_vegetables_f = 0
                ssr_item_nesvaska_subs.sum_provided_potato_f = 0
                ssr_item_nesvaska_subs.sum_due_cereals_f = round(coefficient_а * obj_nesvska_subs.sum_cereals, 2)
                ssr_item_nesvaska_subs.sum_due_vegetables_f = round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2)
                ssr_item_nesvaska_subs.sum_due_vpotato_f = round(coefficient_а * obj_nesvska_subs.sum_potato, 2)

                ssr_item_nesvaska_subs.sum_provided_cereals_k = 0
                ssr_item_nesvaska_subs.sum_provided_vegetables_k = 0
                ssr_item_nesvaska_subs.sum_provided_potato_k = 0
                ssr_item_nesvaska_subs.sum_due_cereals_k = round(obj_nesvska_subs.sum_cereals - round(coefficient_а * obj_nesvska_subs.sum_cereals, 2), 2)
                ssr_item_nesvaska_subs.sum_due_vegetables_k = round(obj_nesvska_subs.sum_vegetables - round(coefficient_а * obj_nesvska_subs.sum_vegetables, 2), 2)
                ssr_item_nesvaska_subs.sum_due_vpotato_k = round(obj_nesvska_subs.sum_potato - round(coefficient_а * obj_nesvska_subs.sum_potato, 2), 2)
            ssr_item_nesvaska_subs.save()
    else:
        i = 1
        ssr_nesvaska_subs_last_id = SSR_Nesvaska_Subs.objects.all().last().id
        for item in nesvaska_ssr_previous:
            ssr_nesvaska_model_item = SSR_Nesvaska_Subs()
            ssr_nesvaska_model_item = item
            ssr_nesvaska_model_item.id = ssr_nesvaska_subs_last_id + i
            i = i + 1
            ssr_nesvaska_model_item.f_ssr = nesvaska_ssr_current
            ssr_nesvaska_model_item.save()

def get_total_sum_nesvaska(model_item):
    total_cereals = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('area_cereals')))
    total_vegetables = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('area_vegetables')))
    total_potato = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('area_potato')))

    total_need_cereals = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('need_subsidy_cereals')))
    total_need_vegetables = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('need_subsidy_vegetables')))
    total_need_potato = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('need_subsidy_potato')))

    estimated_cereals_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_cereals_f')))
    estimated_vegetables_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_vegetables_f')))
    estimated_potato_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_potato_f')))

    provided_cereals_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_cereals_f')))
    provided_vegetables_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_vegetables_f')))
    provided_potato_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_potato_f')))

    due_cereals_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_cereals_f')))
    due_vegetables_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_vegetables_f')))
    due_vpotato_f = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_vpotato_f')))

    estimated_cereals_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_cereals_k')))
    estimated_vegetables_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_vegetables_k')))
    estimated_potato_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_estimated_potato_k')))

    provided_cereals_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_cereals_k')))
    provided_vegetables_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_vegetables_k')))
    provided_potato_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_provided_potato_k')))

    due_cereals_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_cereals_k')))
    due_vegetables_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_vegetables_k')))
    due_vpotato_k = SSR_Nesvaska_Subs.objects.filter(f_ssr=model_item.id).aggregate(Sum(F('sum_due_vpotato_k')))


    total_area = total_cereals.get('area_cereals__sum') + total_vegetables.get('area_vegetables__sum') + total_potato.get('area_potato__sum')
    total_area_proekt = 0
    total_area_insurance = 0
    total_need = total_need_cereals.get('need_subsidy_cereals__sum') + total_need_vegetables.get('need_subsidy_vegetables__sum') + total_need_potato.get('need_subsidy_potato__sum')
    total_estimated_f = estimated_cereals_f.get('sum_estimated_cereals_f__sum') + estimated_vegetables_f.get('sum_estimated_vegetables_f__sum') + estimated_potato_f.get('sum_estimated_potato_f__sum')
    total_provided_f = provided_cereals_f.get('sum_provided_cereals_f__sum') + provided_vegetables_f.get('sum_provided_vegetables_f__sum') + provided_potato_f.get('sum_provided_potato_f__sum')
    total_due_f = due_cereals_f.get('sum_due_cereals_f__sum') + due_vegetables_f.get('sum_due_vegetables_f__sum') + due_vpotato_f.get('sum_due_vpotato_f__sum')
    total_estimated_k = estimated_cereals_k.get('sum_estimated_cereals_k__sum') + estimated_vegetables_k.get('sum_estimated_vegetables_k__sum') + estimated_potato_k.get('sum_estimated_potato_k__sum')
    total_provided_k = provided_cereals_k.get('sum_provided_cereals_k__sum') + provided_vegetables_k.get('sum_provided_vegetables_k__sum') + provided_potato_k.get('sum_provided_potato_k__sum')
    total_due_k = due_cereals_k.get('sum_due_cereals_k__sum') + due_vegetables_k.get('sum_due_vegetables_k__sum') + due_vpotato_k.get('sum_due_vpotato_k__sum')

    return total_area, total_area_proekt, total_area_insurance, total_need, total_estimated_f, total_provided_f, total_due_f, total_estimated_k, total_provided_k, total_due_k

def save_nesvaska_ssr_to_excel(obj_nesvaska_ssr, ssr_model_item, str_document_path):
    my_wb = openpyxl.Workbook()
    my_sheet = my_wb.active
    my_sheet.title = 'Сводная справка - расчет'

    ft_title = Font(size=14, name='Times New Roman')
    ft_text = Font(size=11, name='Times New Roman')
    ft_text_12 = Font(size=12, name='Times New Roman')
    ft_text_13 = Font(size=13, name='Times New Roman')


    ft_b = Font(size=12, name='Times New Roman', bold=True)

    thin_border = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))

    thin_border_bottom = Border(bottom=Side(style='thin'))

    my_sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=17)
    my_sheet.cell(row=1, column=1).font = ft_title
    my_sheet.row_dimensions[1].height = 78

    my_sheet.row_dimensions[3].height = 211

    c1 = my_sheet.cell(row=1, column=1)
    c1.value = ssr_model_item.name_title + " " + ssr_model_item.name_period + " году"
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')

    i = 1
    for item in range(17):
        c1 = my_sheet.cell(row=2, column = i)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 1
    for item in range(17):
        c1 = my_sheet.cell(row=3, column = i)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 1
    for item in range(17):
        c1 = my_sheet.cell(row=4, column = i)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    my_sheet.merge_cells(start_row=2, start_column=1, end_row=3, end_column=1)
    c1 = my_sheet.cell(row=2, column=1)
    c1.value = "№ п/п"
    my_sheet.column_dimensions[get_column_letter(1)].width = 7

    my_sheet.merge_cells(start_row=2, start_column=2, end_row=3, end_column=2)
    c1 = my_sheet.cell(row=2, column=2)
    c1.value = "Наименование муниципального района (муниципального округа), городского округа"
    my_sheet.column_dimensions[get_column_letter(2)].width = 15

    my_sheet.merge_cells(start_row=2, start_column=3, end_row=3, end_column=3)
    c1 = my_sheet.cell(row=2, column=3)
    c1.value = "Наименование получателя субсидии"
    my_sheet.column_dimensions[get_column_letter(3)].width = 25

    my_sheet.merge_cells(start_row=2, start_column=4, end_row=3, end_column=4)
    c1 = my_sheet.cell(row=2, column=4)
    c1.value = "Наименование группы сельскохозяйственных культур"
    my_sheet.column_dimensions[get_column_letter(4)].width = 25

    my_sheet.merge_cells(start_row=2, start_column=5, end_row=3, end_column=5)
    c1 = my_sheet.cell(row=2, column=5)
    c1.value = "Посевная площадь сельскохозяйственных культур, га"
    my_sheet.column_dimensions[get_column_letter(5)].width = 13

    my_sheet.merge_cells(start_row=2, start_column=6, end_row=3, end_column=6)
    c1 = my_sheet.cell(row=2, column=6)
    c1.value = "Ставка субсидии, рублей на 1 гектар"
    my_sheet.column_dimensions[get_column_letter(6)].width = 11

    my_sheet.merge_cells(start_row=2, start_column=7, end_row=3, end_column=7)
    c1 = my_sheet.cell(row=2, column=7)
    c1.value = "Посевная площадь сельскохозяйственных культур, отраженная в проектно-сметной документации при проведении работ по фосфоритованию и (или) гипсованию посевных площадей, га"
    my_sheet.column_dimensions[get_column_letter(7)].width = 16

    my_sheet.merge_cells(start_row=2, start_column=8, end_row=3, end_column=8)
    c1 = my_sheet.cell(row=2, column=8)
    c1.value = "k"
    my_sheet.column_dimensions[get_column_letter(8)].width = 6

    my_sheet.merge_cells(start_row=2, start_column=9, end_row=3, end_column=9)
    c1 = my_sheet.cell(row=2, column=9)
    c1.value = "Посевная площадь сельскохозяйственных культур, в отношении которых осуществляется страхование, га"
    my_sheet.column_dimensions[get_column_letter(9)].width = 17

    my_sheet.merge_cells(start_row=2, start_column=10, end_row=3, end_column=10)
    c1 = my_sheet.cell(row=2, column=10)
    c1.value = "kст"
    my_sheet.column_dimensions[get_column_letter(10)].width = 6

    my_sheet.merge_cells(start_row=2, start_column=11, end_row=3, end_column=11)
    c1 = my_sheet.cell(row=2, column=11)
    c1.value = "Потребность в субсидии, рублей"
    my_sheet.column_dimensions[get_column_letter(11)].width = 17

    my_sheet.merge_cells(start_row=2, start_column=12, end_row=2, end_column=14)
    c1 = my_sheet.cell(row=2, column=12)
    c1.value = "В том числе за счет средств федерального бюджета, рублей"

    c1 = my_sheet.cell(row=3, column=12)
    c1.value = "Расчетная сумма субсидии"
    my_sheet.column_dimensions[get_column_letter(12)].width = 16

    c1 = my_sheet.cell(row=3, column=13)
    c1.value = "Сумма субсидии, фактически предоставленная с начала года"
    my_sheet.column_dimensions[get_column_letter(13)].width = 13

    c1 = my_sheet.cell(row=3, column=14)
    c1.value = "Сумма субсидии, причитающаяся к выплате"
    my_sheet.column_dimensions[get_column_letter(14)].width = 16

    my_sheet.merge_cells(start_row=2, start_column=15, end_row=2, end_column=17)
    c1 = my_sheet.cell(row=2, column=15)
    c1.value = "В том числе за счет средств краевого бюджета, рублей"

    c1 = my_sheet.cell(row=3, column=15)
    c1.value = "Расчетная сумма субсидии"
    my_sheet.column_dimensions[get_column_letter(15)].width = 15

    c1 = my_sheet.cell(row=3, column=16)
    c1.value = "Сумма субсидии, фактически предоставленная с начала года"
    my_sheet.column_dimensions[get_column_letter(16)].width = 12

    c1 = my_sheet.cell(row=3, column=17)
    c1.value = "Сумма субсидии, причитающаяся к выплате"
    my_sheet.column_dimensions[get_column_letter(17)].width = 16

    i = 2
    num = 1
    for item in range(16):
        c1 = my_sheet.cell(row=4, column = i)
        c1.value = num
        i = i + 1
        num = num + 1

    row_i = 5
    num_pp = 1

    for item in obj_nesvaska_ssr:
        count_merge = 0
        row_start = row_i
        if item.area_cereals != 0:
            c1 = my_sheet.cell(row=row_i, column=4)
            c1.value = item.name_group_cereals

            c1 = my_sheet.cell(row=row_i, column=5)
            c1.number_format = '0.000'
            c1.value = item.area_cereals

            c1 = my_sheet.cell(row=row_i, column=6)
            c1.number_format = '0.00'
            c1.value = item.rate_cereals

            c1 = my_sheet.cell(row=row_i, column=7)
            c1.number_format = '0.000'
            c1.value = item.area_proekt_cereals

            c1 = my_sheet.cell(row=row_i, column=9)
            c1.number_format = '0.000'
            c1.value = item.area_insurance_cereals

            c1 = my_sheet.cell(row=row_i, column=11)
            c1.number_format = '0.00'
            c1.value = item.need_subsidy_cereals

            c1 = my_sheet.cell(row=row_i, column=12)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_cereals_f

            c1 = my_sheet.cell(row=row_i, column=13)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_cereals_f

            c1 = my_sheet.cell(row=row_i, column=14)
            c1.number_format = '0.00'
            c1.value = item.sum_due_cereals_f

            c1 = my_sheet.cell(row=row_i, column=15)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_cereals_k

            c1 = my_sheet.cell(row=row_i, column=16)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_cereals_k

            c1 = my_sheet.cell(row=row_i, column=17)
            c1.number_format = '0.00'
            c1.value = item.sum_due_cereals_k

            row_i = row_i + 1
            count_merge = count_merge + 1
        if item.area_vegetables != 0:
            c1 = my_sheet.cell(row=row_i, column=4)
            c1.value = item.name_group_vegetables

            c1 = my_sheet.cell(row=row_i, column=5)
            c1.number_format = '0.000'
            c1.value = item.area_vegetables

            c1 = my_sheet.cell(row=row_i, column=6)
            c1.number_format = '0.00'
            c1.value = item.rate_vegetables

            c1 = my_sheet.cell(row=row_i, column=7)
            c1.number_format = '0.000'
            c1.value = item.area_proekt_vegetables

            c1 = my_sheet.cell(row=row_i, column=9)
            c1.number_format = '0.000'
            c1.value = item.area_insurance_vegetables

            c1 = my_sheet.cell(row=row_i, column=11)
            c1.number_format = '0.00'
            c1.value = item.need_subsidy_vegetables

            c1 = my_sheet.cell(row=row_i, column=12)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_vegetables_f

            c1 = my_sheet.cell(row=row_i, column=13)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_vegetables_f

            c1 = my_sheet.cell(row=row_i, column=14)
            c1.number_format = '0.00'
            c1.value = item.sum_due_vegetables_f

            c1 = my_sheet.cell(row=row_i, column=15)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_vegetables_k

            c1 = my_sheet.cell(row=row_i, column=16)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_vegetables_k

            c1 = my_sheet.cell(row=row_i, column=17)
            c1.number_format = '0.00'
            c1.value = item.sum_due_vegetables_k

            row_i = row_i + 1
            count_merge = count_merge + 1
        if item.area_potato != 0:
            c1 = my_sheet.cell(row=row_i, column=4)
            c1.value = item.name_group_potato

            c1 = my_sheet.cell(row=row_i, column=5)
            c1.number_format = '0.000'
            c1.value = item.area_potato

            c1 = my_sheet.cell(row=row_i, column=6)
            c1.number_format = '0.00'
            c1.value = item.rate_potato

            c1 = my_sheet.cell(row=row_i, column=7)
            c1.number_format = '0.000'
            c1.value = item.area_proekt_potato

            c1 = my_sheet.cell(row=row_i, column=9)
            c1.number_format = '0.000'
            c1.value = item.area_insurance_potato

            c1 = my_sheet.cell(row=row_i, column=11)
            c1.number_format = '0.00'
            c1.value = item.need_subsidy_potato

            c1 = my_sheet.cell(row=row_i, column=12)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_potato_f

            c1 = my_sheet.cell(row=row_i, column=13)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_potato_f

            c1 = my_sheet.cell(row=row_i, column=14)
            c1.number_format = '0.00'
            c1.value = item.sum_due_vpotato_f

            c1 = my_sheet.cell(row=row_i, column=15)
            c1.number_format = '0.00'
            c1.value = item.sum_estimated_potato_k

            c1 = my_sheet.cell(row=row_i, column=16)
            c1.number_format = '0.00'
            c1.value = item.sum_provided_potato_k

            c1 = my_sheet.cell(row=row_i, column=17)
            c1.number_format = '0.00'
            c1.value = item.sum_due_vpotato_k

            row_i = row_i + 1
            count_merge = count_merge + 1
        row_end = row_start + count_merge - 1

        my_sheet.merge_cells(start_row=row_start, start_column=1, end_row=row_end, end_column=1)
        c1 = my_sheet.cell(row=row_start, column=1)
        c1.value = num_pp
        num_pp = num_pp + 1

        my_sheet.merge_cells(start_row=row_start, start_column=2, end_row=row_end, end_column=2)
        c1 = my_sheet.cell(row=row_start, column=2)
        c1.value = item.name_region

        my_sheet.merge_cells(start_row=row_start, start_column=3, end_row=row_end, end_column=3)
        c1 = my_sheet.cell(row=row_start, column=3)
        c1.value = item.name_organization

    total_sum_nesvaska = get_total_sum_nesvaska(ssr_model_item)

    my_sheet.merge_cells(start_row=row_i, start_column=2, end_row=row_i, end_column=3)
    c1 = my_sheet.cell(row=row_i, column=2)
    c1.value = 'Итого'

    c1 = my_sheet.cell(row=row_i, column=4)
    c1.value = 'X'

    c1 = my_sheet.cell(row=row_i, column=5)
    c1.number_format = '0.000'
    c1.value = total_sum_nesvaska[0]

    c1 = my_sheet.cell(row=row_i, column=6)
    c1.value = 'X'

    c1 = my_sheet.cell(row=row_i, column=7)
    c1.number_format = '0.000'
    c1.value = total_sum_nesvaska[1]

    c1 = my_sheet.cell(row=row_i, column=8)
    c1.value = 'X'

    c1 = my_sheet.cell(row=row_i, column=9)
    c1.number_format = '0.000'
    c1.value = total_sum_nesvaska[2]

    c1 = my_sheet.cell(row=row_i, column=10)
    c1.value = 'X'

    c1 = my_sheet.cell(row=row_i, column=11)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[3]

    c1 = my_sheet.cell(row=row_i, column=12)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[4]

    c1 = my_sheet.cell(row=row_i, column=13)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[5]

    c1 = my_sheet.cell(row=row_i, column=14)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[6]

    c1 = my_sheet.cell(row=row_i, column=15)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[7]

    c1 = my_sheet.cell(row=row_i, column=16)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[8]

    c1 = my_sheet.cell(row=row_i, column=17)
    c1.number_format = '0.00'
    c1.value = total_sum_nesvaska[9]

    start_title = 2
    end_title = row_i

    while start_title <= end_title:
        start_column = 1
        while start_column <= 17:
            my_sheet.cell(row=start_title, column=start_column).border = thin_border
            start_column = start_column + 1
        start_title = start_title + 1


    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 1)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 2)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 3)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i - 1):
        c1 = my_sheet.cell(row=i, column = 4)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='center')
        i = i + 1

    c1 = my_sheet.cell(row=row_i, column=4)
    c1.font = ft_text
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 5)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 6)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 7)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 8)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 9)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 10)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 11)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 12)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 13)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 14)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 15)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 16)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    i = 5
    for item in range(row_i):
        c1 = my_sheet.cell(row=i, column = 17)
        c1.font = ft_text
        c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='center')
        i = i + 1

    current_row = row_i + 2
    my_sheet.merge_cells(start_row=current_row, start_column=1, end_row=current_row, end_column=3)
    my_sheet.row_dimensions[current_row].height = 35
    c1 = my_sheet.cell(row=current_row, column=1)
    c1.font = ft_text_12
    c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='bottom')
    c1.value = "Заместитель министра сельского хозяйства и торговли края"

    c1 = my_sheet.cell(row=current_row, column=4)
    my_sheet.cell(row=current_row, column=4).border = thin_border_bottom

    c1 = my_sheet.cell(row=current_row + 1, column=4)
    c1.font = ft_text
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='top')
    c1.value = "(подпись)"

    my_sheet.merge_cells(start_row=current_row, start_column=5, end_row=current_row, end_column=6)
    c1 = my_sheet.cell(row=current_row, column=5)
    c1.font = ft_text_13
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='bottom')
    my_sheet.cell(row=current_row, column=5).border = thin_border_bottom
    my_sheet.cell(row=current_row, column=6).border = thin_border_bottom
    c1.value = ssr_model_item.fio_deputy_minister

    my_sheet.merge_cells(start_row=current_row + 1, start_column=5, end_row=current_row + 1, end_column=6)
    c1 = my_sheet.cell(row=current_row + 1, column=5)
    c1.font = ft_text
    c1.alignment = Alignment(wrap_text=True, horizontal='center', vertical='top')
    c1.value = "(И.О. Фамилия)"

    current_date = datetime.now()
    current_year = current_date.year
    my_sheet.merge_cells(start_row=current_row + 2, start_column=1, end_row=current_row + 2, end_column=3)
    my_sheet.cell(row=current_row + 2, column=1).font = ft_text
    c1 = my_sheet.cell(row=current_row + 2, column=1)
    c1.alignment = Alignment(wrap_text=True, horizontal='left', vertical='center')
    c1.value = "«___» ______________ " + str(current_year) + " г."

    my_sheet.merge_cells(start_row=current_row + 6, start_column=1, end_row=current_row + 6, end_column=3)
    my_sheet.cell(row=current_row + 6, column=1).font = ft_text
    c1 = my_sheet.cell(row=current_row + 6, column=1)
    c1.value = ssr_model_item.fio_spec_oib

    my_sheet.merge_cells(start_row=current_row + 7, start_column=1, end_row=current_row + 7, end_column=3)
    my_sheet.cell(row=current_row + 7, column=1).font = ft_text
    c1 = my_sheet.cell(row=current_row + 7, column=1)
    c1.value = ssr_model_item.phone_spec_oib

    my_wb.save("media" + str_document_path)


def save_excel_nesvaska_ssr(ssr_model_item):
    str_document_path = "/ssrs/" + "сводная справка - расчет_23_" + str(ssr_model_item.id) + ".xlsx"

    obj_nesvaska_ssr = SSR_Nesvaska_Subs.objects.filter(f_ssr=ssr_model_item.id).order_by("name_region")
    print("obj_nesvaska_ssr")
    print(ssr_model_item.id)

    save_nesvaska_ssr_to_excel(obj_nesvaska_ssr, ssr_model_item, str_document_path)
    ssr_model_item.excel_document_path = str_document_path
    ssr_model_item.save()



