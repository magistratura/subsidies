from django.http import HttpResponseRedirect
from project.views_proj.request_eight_article import *
from project.views_proj.request_view import *
from django.urls import reverse

def select_period(request, id_article):
    try:
        authorized_user = OrganizationWithUser.objects.get(f_user__user = request.user)
        Subsidies_f = get_subs_for_period(id_article, authorized_user)
    except:
        messages.warning(request, "Пользователь «" + str(request.user) + "» не прикреплен к организации!")
        return redirect(request.META['HTTP_REFERER'])
    return render(request, "requests/additional/select_period.html", {'Subsidies_f': Subsidies_f, 'id_article': id_article})

def get_subs_for_period(id_article, authorized_user):
    org_type = authorized_user.f_org.f_reg_area.type_area
    date_now = date.today()
    if org_type == "type1":
        Subsidies_f = Subsidies.objects.filter(f_article_law = id_article,
                                               data_start_recive__lte=date_now,
                                               data_end_recive__gte=date_now).order_by("f_article_law__name_article")
    else:
        Subsidies_f = Subsidies.objects.filter(f_article_law = id_article,
                                               data_start_recive_city__lte=date_now,
                                               data_end_recive_city__gte=date_now).order_by("f_article_law__name_article")
    return Subsidies_f


def allocation_create_subs(request):
    select_name_period = request.POST.get("name_period")
    id_article = request.POST.get("id_article")
    Article = ArticlesLaw.objects.get(id = id_article)
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)

    try:
        full_name_period = select_name_period.split(' ')
        name_period = full_name_period[0]
        year_period = int(full_name_period[1])
    except:
        messages.warning(request, "Не удалось распознать период!")
        return redirect(request.META['HTTP_REFERER'])

    Subsidy = Subsidies.objects.get(f_article_law = Article,
                                        f_report_period__year_sub = year_period,
                                        f_report_period__f_period__name_period = name_period)

    exist_requests = Requests.objects.filter(f_subsidies = Subsidy.id,
                                             f_organization = authorized_user.f_org,
                                             removed = False)

    if exist_requests.count() != 0:
        messages.warning(request, "Заявка по данной статье и периоду уже созадана!")
        return redirect(request.META['HTTP_REFERER'])

    try:
        id_report_month = get_index_month(name_period)
        if (id_report_month == 12):
            index_report_month = 1
        else:
            index_report_month = id_report_month + 1
    except:
        messages.warning(request, "Не удалось получить индекс месяца отчетного периода!")
        return redirect(request.META['HTTP_REFERER'])

    num_article = Subsidy.f_article_law.num_article

    if (num_article == "8"):
        return HttpResponseRedirect(reverse('add_eight_request', args=(Subsidy.id,)))
    else:
        messages.warning(request, "Выбранная ежемесячная статья не настроена!")


    return redirect(request.META['HTTP_REFERER'])

def select_annual_period(request, id_article):
    try:
        authorized_user = OrganizationWithUser.objects.get(f_user__user = request.user)
        Subsidies_f = get_subs_for_period(id_article, authorized_user)
    except:
        messages.warning(request, "Пользователь «" + str(request.user) + "» не прикреплен к организации!")
        return redirect(request.META['HTTP_REFERER'])

    return render(request, "requests/additional/select_annual_period.html", {'Subsidies_f': Subsidies_f, 'id_article': id_article})

def allocation_annual_create_subs(request):
    id_article = request.POST.get("id_article")
    Article = ArticlesLaw.objects.get(id = id_article)
    year_period = request.POST.get("name_anual_period")
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)

    Subsidy = Subsidies.objects.get(f_article_law = Article, f_report_period__f_period__name_period = year_period)

    exist_requests = Requests.objects.filter(f_subsidies = Subsidy.id,
                                             f_organization = authorized_user.f_org,
                                             removed = False)
    if exist_requests.count() != 0:
        messages.warning(request, "Заявка по данной статье и периоду уже созадана!")
        return redirect(request.META['HTTP_REFERER'])

    num_article = Subsidy.f_article_law.num_article

    if (num_article == "23"):
        Nesvaska_Report = Nesvaska_Reports.objects.filter(f_reports_requests__f_organization__inn_org = authorized_user.f_org.inn_org,
                                                               f_reports_requests__status_report ='sr6',
                                                               f_reports_requests__f_report_request__report_year=int(Subsidy.f_report_period.f_period.name_period))
        if Nesvaska_Report.count() != 0:
            return HttpResponseRedirect(reverse('add_nesvaska', args=(Subsidy.id,)))
        else:
            messages.warning(request, "Организация не сдала справку о размере посевных площадей или справка не принята!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        messages.warning(request, "Выбранная ежегодная статья не настроена!")

    return redirect(request.META['HTTP_REFERER'])