from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime
from project.views_proj.report_for_sub_nesvaska import *
from project.views_proj.generate_to_word_view import *
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger
from datetime import *

def choose_inform_cacl(request):
    if (request.method == 'POST' and 'choose_inform_calc' in request.POST):
        combo_inform_calc = request.POST.get("combo_inform_calc")
        info_nesvaska_obj = Info_Nesvaska_Calc_Koefficient.objects.filter(report_year = int(combo_inform_calc))
        if info_nesvaska_obj.count() > 0:
            messages.warning(request, "Информация по расчету для отчетного года уже создана!")
            return redirect(request.META['HTTP_REFERER'])
        else:
            return render(request, "analytics_reports/information_for_calc/add_inform_calc.html", {'report_year': combo_inform_calc})
    return render(request, "analytics_reports/information_for_calc/choose_inform_cacl.html")


def calculate_total_area(nesvaska_reports_obj):
    total_area_cereals = 0
    total_area_vegetables = 0
    total_area_potato = 0

    for item in nesvaska_reports_obj:
        if item.cereals != 0:
            total_area_cereals = total_area_cereals + item.all_whole_crop
        if item.vegetables != 0:
            print(item.vegetables)
            total_area_vegetables = total_area_vegetables + item.vegetables
        if item.potato != 0:
            total_area_potato = total_area_potato + item.potato
    return total_area_cereals, total_area_vegetables, total_area_potato


def create_inform_cacl_nesvaska(request):
    report_year = int(request.POST.get("report_year"))

    nesvaska_reports_obj = Nesvaska_Reports.objects.filter(f_reports_requests__f_report_request__report_year = report_year,
                                                           f_reports_requests__f_report_request__num_article = "23",
                                                           f_reports_requests__status_report = 'sr6')

    info_nesvaska_obj = Info_Nesvaska_Calc_Koefficient()
    info_nesvaska_obj.report_year = report_year

    consol_budget_funds_limit = round(float(request.POST.get("consol_budget_funds_limit")),6)
    info_nesvaska_obj.consol_budget_funds_limit = consol_budget_funds_limit

    auxiliary_1 = round(float(request.POST.get("auxiliary_1")), 6)
    info_nesvaska_obj.auxiliary_1 = auxiliary_1

    auxiliary_2 = round(float(request.POST.get("auxiliary_2")), 6)
    info_nesvaska_obj.auxiliary_2 = auxiliary_2

    auxiliary_4 = round(float(request.POST.get("auxiliary_4")), 6)
    info_nesvaska_obj.auxiliary_4 = auxiliary_4

    total_area = calculate_total_area(nesvaska_reports_obj)

    total_area_vegetables = total_area[1]
    total_area_cereals = total_area[0]
    total_area_potato = total_area[2]

    info_nesvaska_obj.total_area_vegetables = total_area_vegetables
    info_nesvaska_obj.total_area_cereals = total_area_cereals
    info_nesvaska_obj.total_area_potato = total_area_potato

    if auxiliary_1!=0 and auxiliary_2 !=0 and auxiliary_4 !=0 and consol_budget_funds_limit!=0:
        auxiliary_3 = (1 - auxiliary_1 - auxiliary_2 - auxiliary_4)
        info_nesvaska_obj.auxiliary_3 = auxiliary_3

        budget_funds_limit_cereals = round(consol_budget_funds_limit * auxiliary_1, 2)
        info_nesvaska_obj.budget_funds_limit_cereals = budget_funds_limit_cereals

        budget_funds_limit_vegetables = round(consol_budget_funds_limit * auxiliary_2, 2)
        info_nesvaska_obj.budget_funds_limit_vegetables = budget_funds_limit_vegetables

        insurance = round(consol_budget_funds_limit * auxiliary_4, 2)
        info_nesvaska_obj.insurance = insurance

        budget_funds_limit_potato = round(consol_budget_funds_limit - budget_funds_limit_cereals - budget_funds_limit_vegetables - insurance, 2)
        info_nesvaska_obj.budget_funds_limit_potato = budget_funds_limit_potato

        if (total_area_cereals != 0):
            info_nesvaska_obj.rate_cereals = round(budget_funds_limit_cereals/total_area_cereals, 2)
        else:
            info_nesvaska_obj.rate_cereals = 0

        if (total_area_vegetables != 0):
            info_nesvaska_obj.rate_vegetables = round(budget_funds_limit_vegetables/total_area_vegetables, 2)
        else:
            info_nesvaska_obj.rate_vegetables = 0

        if (total_area_potato != 0):
            info_nesvaska_obj.rate_potato = round(budget_funds_limit_potato/total_area_potato, 2)
        else:
            info_nesvaska_obj.rate_potato = 0

    messages.success(request, "Ваше действие успешно выполнено!")
    info_nesvaska_obj.save()

    return HttpResponseRedirect(reverse('edit_inform_calc', args=(info_nesvaska_obj.id,)))

def edit_inform_calc(request, id):
    info_nesvaska_obj = Info_Nesvaska_Calc_Koefficient.objects.get(id = id)
    if (request.method == 'POST' and 'save_inform_cacl_nesvaska' in request.POST):
        report_year = info_nesvaska_obj.report_year

        nesvaska_reports_obj = Nesvaska_Reports.objects.filter(
            f_reports_requests__f_report_request__report_year=report_year,
            f_reports_requests__f_report_request__num_article="23",
            f_reports_requests__status_report='sr6')

        consol_budget_funds_limit = round(float(request.POST.get("consol_budget_funds_limit")), 6)
        info_nesvaska_obj.consol_budget_funds_limit = consol_budget_funds_limit

        auxiliary_1 = round(float(request.POST.get("auxiliary_1")), 6)
        info_nesvaska_obj.auxiliary_1 = auxiliary_1

        auxiliary_2 = round(float(request.POST.get("auxiliary_2")), 6)
        info_nesvaska_obj.auxiliary_2 = auxiliary_2

        auxiliary_4 = round(float(request.POST.get("auxiliary_4")), 6)
        info_nesvaska_obj.auxiliary_4 = auxiliary_4

        total_area = calculate_total_area(nesvaska_reports_obj)

        total_area_vegetables = total_area[1]
        total_area_cereals = total_area[0]
        total_area_potato = total_area[2]

        info_nesvaska_obj.total_area_vegetables = total_area_vegetables
        info_nesvaska_obj.total_area_cereals = total_area_cereals
        info_nesvaska_obj.total_area_potato = total_area_potato

        if auxiliary_1 != 0 and auxiliary_2 != 0 and auxiliary_4 != 0 and consol_budget_funds_limit != 0:
            auxiliary_3 = (1 - auxiliary_1 - auxiliary_2 - auxiliary_4)
            info_nesvaska_obj.auxiliary_3 = auxiliary_3

            budget_funds_limit_cereals = round(consol_budget_funds_limit * auxiliary_1, 2)
            info_nesvaska_obj.budget_funds_limit_cereals = budget_funds_limit_cereals

            budget_funds_limit_vegetables = round(consol_budget_funds_limit * auxiliary_2, 2)
            info_nesvaska_obj.budget_funds_limit_vegetables = budget_funds_limit_vegetables

            insurance = round(consol_budget_funds_limit * auxiliary_4, 2)
            info_nesvaska_obj.insurance = insurance

            budget_funds_limit_potato = round(
                consol_budget_funds_limit - budget_funds_limit_cereals - budget_funds_limit_vegetables - insurance, 2)
            info_nesvaska_obj.budget_funds_limit_potato = budget_funds_limit_potato

            if (total_area_cereals != 0):
                info_nesvaska_obj.rate_cereals = round(budget_funds_limit_cereals / total_area_cereals, 2)
            else:
                info_nesvaska_obj.rate_cereals = 0

            if (total_area_vegetables != 0):
                info_nesvaska_obj.rate_vegetables = round(budget_funds_limit_vegetables / total_area_vegetables, 2)
            else:
                info_nesvaska_obj.rate_vegetables = 0

            if (total_area_potato != 0):
                info_nesvaska_obj.rate_potato = round(budget_funds_limit_potato / total_area_potato, 2)
            else:
                info_nesvaska_obj.rate_potato = 0
        else:
            info_nesvaska_obj.auxiliary_3 = 0
            info_nesvaska_obj.budget_funds_limit_cereals = 0
            info_nesvaska_obj.budget_funds_limit_vegetables = 0
            info_nesvaska_obj.insurance = 0
            info_nesvaska_obj.budget_funds_limit_potato = 0
            info_nesvaska_obj.rate_cereals = 0
            info_nesvaska_obj.rate_vegetables = 0
            info_nesvaska_obj.rate_potato = 0

        messages.success(request, "Ваше действие успешно выполнено!")
        info_nesvaska_obj.save()

    return render(request, "analytics_reports/information_for_calc/edit_inform_calc.html", {'info_nesvaska_obj': info_nesvaska_obj})


def show_inform_cacls(request):
    info_nesvaska_obj = Info_Nesvaska_Calc_Koefficient.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(info_nesvaska_obj, 5)
    try:
        info_nesvaska_obj = paginator.page(page)
    except PageNotAnInteger:
        info_nesvaska_obj = paginator.page(1)
    except EmptyPage:
        info_nesvaska_obj = paginator.page(paginator.num_pages)

    print(info_nesvaska_obj)
    return render(request, "analytics_reports/information_for_calc/show_inform_cacls.html",
              {'info_nesvaska_obj': info_nesvaska_obj})









