from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime
from project.views_proj.report_for_sub_nesvaska import *
from project.views_proj.generate_to_word_view import *
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger
from datetime import *
from project.views_proj.generate_to_excel import *

def show_actions_analytics_reports(request):
    return render(request, "analytics_reports/show/show_actions_analytics_reports.html")

def create_analitics_rports(name_analytics_reports_obj):
    analitics_rports_obj = Analitics_Reports()
    analitics_rports_obj.f_analitics_reports = name_analytics_reports_obj
    analitics_rports_obj.current_year = name_analytics_reports_obj.report_year
    analitics_rports_obj.save()
    return analitics_rports_obj

def choose_analytics_reports(request):
    if (request.method == 'POST' and 'choose_type_analytic_report' in request.POST):
        select_type_analytics_reports = request.POST.get("combo_type_analytics_reports")
        name_analytics_reports_obj = Name_Analitics_Reports.objects.get(name_analitics_reports = select_type_analytics_reports)
        if (name_analytics_reports_obj.num_article == "23"):
            try:
                analitics_rports_obj = create_analitics_rports(name_analytics_reports_obj)
                excel_document_path = save_nesvaska_to_excel(name_analytics_reports_obj, analitics_rports_obj.id)
                analitics_rports_obj.excel_document_path = excel_document_path
                analitics_rports_obj.save()
                messages.success(request, "Ваше действие успешно выполнено!")
                return HttpResponseRedirect(reverse('show_analytics_reports'))
            except:
                messages.warning(request, "Не удалось создать аналитический отчет, поскольку не рассчитаны ставки!")
                return redirect(request.META['HTTP_REFERER'])
        else:
            print("другая статья")
    if (request.method == 'POST' and 'choose_analytic_report' in request.POST):
        select_name_report_request = request.POST.get("combo_analytics_reports")
        name_reports_requests_obj = Name_Reports_Requests.objects.get(name_report_request = select_name_report_request)
        if (name_reports_requests_obj.num_article == "23"):
            list_name_analitics_reports= []
            for item in name_reports_requests_obj.m_name_analitics_reports.all():
                list_name_analitics_reports.append(item.name_analitics_reports)
            return render(request, "analytics_reports/choose_type_analytics_reports.html", {'list_name_analitics_reports': list_name_analitics_reports})
        else:
            print("другая статья")
    name_reports_requests = Name_Reports_Requests.objects.all()
    return render(request, "analytics_reports/choose_analytics_reports.html", {'name_reports_requests': name_reports_requests})

def show_analytics_reports(request):
    model_item = Analitics_Reports.objects.all().order_by("-id")
    page = request.GET.get('page', 1)
    paginator = Paginator(model_item, 5)
    try:
        model_item = paginator.page(page)
    except PageNotAnInteger:
        model_item = paginator.page(1)
    except EmptyPage:
        model_item = paginator.page(paginator.num_pages)
    return render(request, "analytics_reports/show/show_analytics_reports.html", {'model_item': model_item})