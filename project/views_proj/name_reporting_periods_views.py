from django.shortcuts import render
from django.http import HttpResponseNotFound
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages

def show_reporting_periods(request):
    model_item = NameReportingPeriod.objects.all()
    return render(request, "guides/Name_reporting_periods/show_name_reporting_periods.html", {'model_item': model_item})

def add_name_reporting_period(request):
    if (request.method == 'POST' and 'save_reporting_period' in request.POST):
        form = NameReportingPeriodForm(request.POST)
        form.save()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])

    else:
        form = NameReportingPeriodForm(request.POST)
        return render(request, "guides/Name_reporting_periods/add_name_reporting_period.html", {'form': form})

def delete_name_reporting_period(request):
    try:
        id_selected_reporting_period = request.POST.get("selected_td_reporting_period")
        model_item = NameReportingPeriod.objects.get(id=int(id_selected_reporting_period))
        model_item.delete()
        model_item = NameReportingPeriod.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_name_reporting_period(request, id):
    try:
        model_item = NameReportingPeriod.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            model_item.name_period = request.POST.get("name_period")
            model_item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        else:
            return render(request, "guides/Name_reporting_periods/edit_name_reporting_period.html", {'model_item': model_item})
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])