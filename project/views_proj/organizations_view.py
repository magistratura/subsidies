from django.shortcuts import render
from django.http import HttpResponseNotFound
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from django.http import JsonResponse
from django.db.models import Q

def show_organizations(request):
    full_name_org = ""
    model_item = Organization.objects.all()
    if 'term' in request.GET:
        qs = Organization.objects.filter(Q(inn_org__icontains=request.GET.get('term')) | Q(full_name_org__icontains=request.GET.get('term')))
        inns = list()
        for inn in qs:
            inns.append(inn.full_name_org)
        return JsonResponse(inns, safe=False)

    name_organization = request.POST.get("name_organization")
    if (request.method == 'POST' and name_organization!="" and  'search_organization' in request.POST):
        full_name_org = name_organization
        model_item = Organization.objects.filter(full_name_org=str(name_organization))
        if (model_item.count()==0):
            messages.warning(request, "Не найдено ни одной записи")

    page = request.GET.get('page', 1)
    paginator = Paginator(model_item, 8)
    try:
        model_item = paginator.page(page)
    except PageNotAnInteger:
        model_item = paginator.page(1)
    except EmptyPage:
        model_item = paginator.page(paginator.num_pages)
    return render(request, "guides/Organizations/show_organizations.html", {'model_item': model_item, 'full_name_org': full_name_org})

def add_organization(request):
    if (request.method == 'POST' and 'save_organization' in request.POST):
        try:
            f_reg_area = request.POST.get("f_reg_area")
            model_reg_area = RegistrationArea.objects.get(name_area=f_reg_area)

            f_bank_name = request.POST.get("f_bank_name")
            if (f_bank_name !="" and f_bank_name!="-----"):
                data_bank_name = f_bank_name.split(' ')
                data_bik_bank = data_bank_name[0]
                model_bank = Bank.objects.get(bik_bank = data_bik_bank)
            else:
                model_bank = None

            f_name_type_org = request.POST.get("f_name_type_org")
            model_type_org = TypeOrganization.objects.get(name_type_org=f_name_type_org)

            item = Organization(full_name_org =  request.POST.get("full_name_org"),
                                short_name_org = request.POST.get("short_name_org"),
                                f_reg_area = model_reg_area,
                                adress_org = request.POST.get("adress_org"),
                                phone_org = request.POST.get("phone_org"),
                                email_org = request.POST.get("email_org"),
                                oktmo_org = request.POST.get("oktmo_org"),
                                okpo_org = request.POST.get("okpo_org"),
                                inn_org = request.POST.get("inn_org"),
                                account_number_org = request.POST.get("account_number_org"),
                                m_banks = model_bank,
                                f_type = model_type_org,
                                fio_manager_org = request.POST.get("fio_manager_org"),
                                fio_rod_manager_org = request.POST.get("fio_rod_manager_org"),
                                title_document_org = request.POST.get("title_document_org")
            )

            item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except IntegrityError:
            messages.warning(request, "Организация с таким ИНН уже существует! Введите другой ИНН!")
            form = OrganizationForm(request.POST)
            bank_org = Bank.objects.all()
            bank_org = Bank.objects.order_by("bik_bank")
            type_org = TypeOrganization.objects.order_by("name_type_org")

            adress_org = RegistrationArea.objects.order_by("name_area")
            return render(request, "guides/Organizations/add_organization.html",
                          {'form': form, 'f_adress_org': adress_org,
                           'f_bank_org': bank_org,
                           'f_type_org': type_org})
    else:
        form = OrganizationForm(request.POST)
        bank_org = Bank.objects.all()
        bank_org = Bank.objects.order_by("bik_bank")
        type_org = TypeOrganization.objects.order_by("name_type_org")

        adress_org = RegistrationArea.objects.order_by("name_area")
        return render(request, "guides/Organizations/add_organization.html", {'form': form,
                                                                              'f_adress_org': adress_org,
                                                                              'f_bank_org': bank_org,
                                                                              'f_type_org': type_org})

def delete_organization(request):
    try:
        id_selected_organization = request.POST.get("selected_td_organization")
        model_item = Organization.objects.get(id=int(id_selected_organization))
        model_item.delete()
        model_item = Organization.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")

def show_selected_organization(request, id):
    model_item = Organization.objects.get(id=id)
    bik_selected_bank = model_item.m_banks
    if (bik_selected_bank!=None):
        model_type_bank = Bank.objects.get(bik_bank=bik_selected_bank)
        return render(request, "guides/Organizations/show_organization.html", {'model_item': model_item,
                                                                           'bik_selected_bank': bik_selected_bank,
                                                                           'name_selected_bank': model_type_bank.name_bank})
    else:
        return render(request, "guides/Organizations/show_organization.html", {'model_item': model_item,
                                                                           'bik_selected_bank': bik_selected_bank})

def edit_organization(request, id):
    try:
        model_item = Organization.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            try:
                f_reg_area = request.POST.get("f_reg_area")
                model_reg_area = RegistrationArea.objects.get(name_area=f_reg_area)

                f_bank_name = request.POST.get("f_bank_name")
                if (f_bank_name != "" and f_bank_name != "-----"):
                    data_bank_name = f_bank_name.split(' ')
                    data_bik_bank = data_bank_name[0]
                    model_bank = Bank.objects.get(bik_bank = data_bik_bank)

                f_name_type_org = request.POST.get("f_name_type_org")
                model_type_org = TypeOrganization.objects.get(name_type_org=f_name_type_org)

                model_item.full_name_org = request.POST.get("full_name_org")
                model_item.short_name_org = request.POST.get("short_name_org")
                model_item.f_reg_area = model_reg_area
                model_item.adress_org = request.POST.get("adress_org")
                model_item.phone_org = request.POST.get("phone_org")
                model_item.email_org = request.POST.get("email_org")
                model_item.oktmo_org = request.POST.get("oktmo_org")
                model_item.okpo_org = request.POST.get("okpo_org")
                model_item.inn_org = request.POST.get("inn_org")
                model_item.account_number_org = request.POST.get("account_number_org")
                if (f_bank_name != "" and f_bank_name != "-----"):
                    model_item.m_banks = model_bank
                else: model_item.m_banks = None
                model_item.f_type = model_type_org
                model_item.fio_manager_org = request.POST.get("fio_manager_org")
                model_item.fio_rod_manager_org = request.POST.get("fio_rod_manager_org")
                model_item.title_document_org = request.POST.get("title_document_org")

                model_item.save()
                messages.success(request, "Ваше действие успешно выполнено!")
                return redirect(request.META['HTTP_REFERER'])
            except IntegrityError:
                messages.warning(request, "Организация с таким ИНН уже существует! Введите другой ИНН!")
                return redirect(request.META['HTTP_REFERER'])
        else:
            bik_selected_bank = model_item.m_banks
            type_org = TypeOrganization.objects.order_by("name_type_org")
            adress_org = RegistrationArea.objects.order_by("name_area")

            if (bik_selected_bank==None):
                bik_selected_bank="-----"
                bank_org = Bank.objects.all()
                bank_org = Bank.objects.order_by("bik_bank")
                return render(request, "guides/Organizations/edit_organization.html", {'model_item': model_item,
                                                                                       'f_adress_org': adress_org,
                                                                                       'f_bank_org': bank_org,
                                                                                       'f_type_org': type_org,
                                                                                       'bik_selected_bank': bik_selected_bank})
            else:
                model_type_bank = Bank.objects.get(bik_bank=bik_selected_bank)
                bank_org = Bank.objects.all()
                bank_org = Bank.objects.order_by("bik_bank")
                return render(request, "guides/Organizations/edit_organization.html", {'model_item': model_item,
                                                                                      'f_adress_org': adress_org,
                                                                                      'f_bank_org': bank_org,
                                                                                      'f_type_org': type_org,
                                                                                      'bik_selected_bank': bik_selected_bank,
                                                                                      'name_selected_bank': model_type_bank.name_bank})
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])