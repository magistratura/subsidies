from django.shortcuts import render
from django.http import HttpResponseNotFound
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError

def add_bank(request):
    if (request.method == 'POST' and 'save_bank' in request.POST):
        try:
            form = BankForm(request.POST)
            form.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except:
            messages.warning(request, "Банк с таким ИНН уже существует! Введите другой ИНН!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = BankForm(request.POST)
        return render(request, "guides/Banks/add_bank.html", {'form': form})

def show_banks(request):
    model_item = Bank.objects.all()
    return render(request, "guides/Banks/show_banks.html", {'model_item': model_item})

def delete_bank(request):
    try:
        id_selected_bank = request.POST.get("selected_td")
        model_item = Bank.objects.get(id=int(id_selected_bank))
        model_item.delete()
        model_item = Bank.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_bank(request, id):
    try:
        model_item = Bank.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            try:
                model_item.bik_bank = request.POST.get("bik_bank")
                model_item.name_bank = request.POST.get("name_bank")
                model_item.inn_bank = request.POST.get("inn_bank")
                model_item.correspond_bank = request.POST.get("correspond_bank")
                model_item.kpp_bank = request.POST.get("kpp_bank")
                model_item.save()
                messages.success(request, "Ваше действие успешно выполнено!")
                return redirect(request.META['HTTP_REFERER'])
            except IntegrityError:
                messages.warning(request, "Банк с таким ИНН уже существует! Введите другой ИНН!")
                return redirect(request.META['HTTP_REFERER'])
        else:
            return render(request, "guides/Banks/edit_bank.html", {'model_item': model_item})
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")