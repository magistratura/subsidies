from project.views_proj.request_eight_article import *
from PyPDF2 import PdfFileMerger
from pathlib import Path
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger
from project.views_proj.generate_to_word_view import *
from project.views_proj.getting_month import *
from django.urls import reverse
from project.views_proj.select_subsidies_view import *
import pythoncom

def delete_request(request, id):
    try:
        obj_request = Requests.objects.get(id=id)
        obj_request.removed = True
        obj_request.save()
        messages.success(request, "Заявка удалена!")
    except:
        messages.warning(request, "Не удалось удалить заявку!")

    return redirect(request.META['HTTP_REFERER'])

def get_points_for_edit():
    Points_subs_1 = ['вручить лично, предварительно оповестив по телефону*;',
                     'направить по почтовому адресу:',
                     'направить по адресу электронной почты',
                     'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**',
                     '']

    Points_subs_2 = ['вручить лично, предварительно оповестив по телефону*;',
                     'направить по почтовому адресу:',
                     'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**',
                     '']

    Leukemia_subs = ['Был лейкоз',
                     'Не было лейкоза']

    Quarantine = ['Установлен',
                  'Не установлен']
    return Points_subs_1, Points_subs_2, Leukemia_subs, Quarantine

def show_actions_requests(request):
    return render(request, "requests/show_actions_requests.html")

def report_ability(selected_subsidies, authorized_user):
    ability = False
    start_region = selected_subsidies.data_start_recive
    end_region = selected_subsidies.data_end_recive

    start_city = selected_subsidies.data_start_recive_city
    end_city = selected_subsidies.data_end_recive_city

    date_now = date.today()
    type_are_org = authorized_user.f_org.f_reg_area.type_area
    if type_are_org == "type1":
        if (date_now >= start_region and date_now <= end_region):
            ability = True
        else:
            ability = False
    else:
        if (date_now >= start_city and date_now <= end_city):
            ability = True
        else:
            ability = False
    return ability

def select_article(request):
    list_articles_law = ArticlesLaw.objects.all().order_by("name_article")
    if request.method == "POST" and 'select_name_article' in request.POST:
        name_article = request.POST.get("name_article")
        Article = ArticlesLaw.objects.get(name_article = name_article)

        if Article.f_period_article.name_period != "Ежегодная":
            return HttpResponseRedirect(reverse('select_period', args=(Article.id,)))
        else:
            return HttpResponseRedirect(reverse('select_annual_period', args=(Article.id,)))

    return render(request, "requests/additional/select_article.html", {'list_articles_law': list_articles_law})

def show_requests(request):
    model_item_area = RegistrationArea.objects.all().order_by("name_area")
    model_item_subsidies = Subsidies.objects.all().order_by("f_article_law")

    id_request = request.POST.get("id_request")
    if (id_request == None):
        id_request = ""

    inn_org = request.POST.get("inn_org")
    if (inn_org == None):
        inn_org = ""

    if (request.method == 'POST' and 'clear_search' in request.POST):
        model_item = Requests.objects.filter(removed = False)
        page = request.GET.get('page', 1)
        paginator = Paginator(model_item, 5)
        try:
            model_item = paginator.page(page)
        except PageNotAnInteger:
            model_item = paginator.page(1)
        except EmptyPage:
            model_item = paginator.page(paginator.num_pages)

        inn_org = ""
        id_request = ""

        model_item_num = Requests.objects.filter(removed = False)
        num_records = model_item_num.all().count()
        return render(request, "requests/show_requests.html",
                      {'model_item': model_item, 'model_item_area': model_item_area,
                       'model_item_subsidies': model_item_subsidies,
                       'num_records': num_records, 'id_request': id_request,
                       'inn_org': inn_org})

    if (request.method == 'POST' and 'serach_requests' in request.POST):
        model_item = Requests.objects.filter(removed = False)

        if (request.POST.get("id_request") != ""):
            id_request = request.POST.get("id_request")
            model_item = Requests.objects.filter(id=str(id_request), removed = False)
            num_records = model_item.all().count()

        elif (request.POST.get("inn_org") != ""):
            inn_org = request.POST.get("inn_org")
            model_item = Requests.objects.filter(removed = False, f_organization__inn_org = inn_org)
            if (model_item.count() == 0):
                messages.warning(request, "Не найдено ни одной записи")
            num_records = model_item.all().count()

        elif (request.POST.get("name_article") != "Все субсидии"):
            num_article = request.POST.get("name_article")
            print(num_article)
            data_num_article = num_article.split(' ')
            model_item_num_article = Subsidies.objects.get(id=data_num_article[0])
            try:
                model_item = Requests.objects.filter(f_subsidies=model_item_num_article.id)
            except:
                model_item = Requests.objects.all()

        return render(request, "requests/show_requests.html",
                      {'model_item': model_item, 'model_item_area': model_item_area,
                       'model_item_subsidies': model_item_subsidies,
                       'num_records': num_records, 'id_request': id_request,
                       'inn_org': inn_org})
    else:

        model_item = Requests.objects.filter(removed = False)
        page = request.GET.get('page', 1)
        paginator = Paginator(model_item, 10)

        try:
            model_item = paginator.page(page)
        except PageNotAnInteger:
            model_item = paginator.page(1)
        except EmptyPage:
            model_item = paginator.page(paginator.num_pages)

        model_item_num = Requests.objects.filter(removed = False)
        num_records = model_item_num.all().count()

        return render(request, "requests/show_requests.html",
                      {'model_item': model_item, 'model_item_area': model_item_area,
                       'model_item_subsidies': model_item_subsidies,
                       'num_records': num_records, 'id_request': id_request,
                       'inn_org': inn_org})

# Просмотр лицевой карточки заявки
def show_request(request, id):
    model_item = Requests.objects.get(id=id)
    status_request = model_item.get_f_status_display()

    if request.method == "POST" and 'refactor_request' in request.POST:

        num_subsid = model_item.num_subsid
        id_subsid = model_item.id
        if (num_subsid == 8):
            return HttpResponseRedirect(reverse('save_edit_eight_request', args=(id_subsid,)))
        elif (num_subsid == 23):
            return HttpResponseRedirect(reverse('edit_nesvaska_request', args=(id_subsid,)))
        else:
            print('другая')

    status_action = ""

    status_variant = {'Новая': 'Отправить',
                      'Отказано': 'Изменить заявку',
                      'Отправлено': 'Проверить МО',
                      'Проверено МО': 'Согласовать с ОТР',
                      'Проверено ОТР': 'Согласовать с ОИБ',
                      'Проверено ОИБ': 'Выплачено',
                      'Выплачено': 'Выплачено'}

    for key, value in status_variant.items():
        if (key == status_request):
            status_action = value


    start_city = model_item.f_subsidies.data_start_recive_city
    end_city = model_item.f_subsidies.data_end_recive_city

    start_region = model_item.f_subsidies.data_start_recive
    end_region = model_item.f_subsidies.data_end_recive

    date_now = date.today()

    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    type_are_org = authorized_user.f_org.f_reg_area.type_area

    if type_are_org == "type1":
        if (date_now >= start_region and date_now <= end_region):
            deadline_admission = "possible"
        else:
            deadline_admission = "unpossible"
    else:
        if (date_now >= start_city and date_now <= end_city):
            deadline_admission = "possible"
        else:
            deadline_admission = "unpossible"

    return render(request, "requests/show_request.html", {'model_item': model_item, 'status_action': status_action,
                                                          'status_request': status_request,
                                                          'deadline_admission': deadline_admission})

def save_request(year_report, summ_total, organization_obj, subsidies_obj, report_year_subsid, num_subsid, user):
    datetime_current = datetime.datetime.now()
    item_request = Requests()
    item_request.history_request = 'Дата создания: ' + str(datetime_current) + ', пользователь: ' + str(user) + ';'
    # item_request.data_create = datetime_current
    item_request.year_sub = year_report
    item_request.total_summ = summ_total
    item_request.f_organization = organization_obj
    item_request.f_status = 's1'
    item_request.num_subsid = num_subsid

    if (subsidies_obj.f_report_period.f_period == "Декабрь"):
        item_request.report_year_subsid = year_report + 1
    else:
        item_request.report_year_subsid = year_report

    item_request.f_subsidies = subsidies_obj
    item_request.used_ssr = False
    item_request.removed = False
    item_request.save()
    current_id_request = item_request.id
    return current_id_request


from django.http import HttpResponseRedirect

def save_DB_eight_form(request):
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)
    id_subsidy = request.POST.get("subsidies_obj")
    subsidies_obj = Subsidies.objects.get(id=id_subsidy)
    report_year_subsid = subsidies_obj.f_report_period.year_sub
    num_article = subsidies_obj.f_article_law

    try:
        pythoncom.CoInitializeEx(0)
    except:
        print("вызов не понадобился")

    if request.method == 'POST':
        try:
            current_id_request = save_request(request.POST.get("year_report"),
                                              float(request.POST.get("summ_total")), organization_obj,
                                              subsidies_obj, report_year_subsid, 8, request.user)

            # Если загружен файл с реестром
            try:
                uploadfile = request.FILES['docfile']
                upload_file = True
            except:
                upload_file = False
                uploadfile=""

            three_point = request.POST.get("three_point")
            fourth_point = request.POST.get("fourth_point")
            fifth_point = request.POST.get("fifth_point")
            points_eight = set_points_eight(organization_obj, three_point, fourth_point, fifth_point)

            num_begin_year_cows = request.POST.get("num_begin_year_cows")
            num_begin_month_cows = request.POST.get("num_begin_month_cows")
            amount_milk_cows = request.POST.get("amount_milk_cows")

            summ_cows = request.POST.get("summ_cows")

            num_begin_year_goats = request.POST.get("num_begin_year_goats")
            amount_milk_goats = request.POST.get("amount_milk_goats")
            summ_goats = request.POST.get("summ_goats")

            num_total = request.POST.get("num_total")

            first_in_group = request.POST.get("first_in_group")

            count_int = int(request.POST.get("count_1"))

            name_spec_position = []
            count_spec = []
            actually_working_spec = []
            higher_prof = []
            secondary_prof = []

            for item in range(count_int):
                row_int = item + 1
                row_str = str(row_int)

                name_spec_position.append(request.POST.get(row_str + "_1"))
                count_spec.append(request.POST.get(row_str + "_2"))
                actually_working_spec.append(request.POST.get(row_str + "_3"))
                higher_prof.append(request.POST.get(row_str + "_4"))
                secondary_prof.append(request.POST.get(row_str + "_5"))

            requests_eight_subs = create_requests_eight_subs(current_id_request,
                                                             request.POST.get("rate_cows"),
                                                             request.POST.get("rate_goats"),
                                                             num_total,
                                                             request.POST.get("id_requests_eight_subs_exists"),
                                                             organization_obj, three_point, fourth_point, fifth_point,
                                                             request.POST.get("leukemia_KRS"),
                                                             request.POST.get("quarantine"),
                                                             request.POST.get("data_quarantine_establish_1"),
                                                             request.POST.get("data_quarantine_establish_2"),
                                                             num_begin_year_cows, num_begin_month_cows,
                                                             amount_milk_cows, num_begin_year_goats,
                                                             amount_milk_goats, summ_cows,
                                                             summ_goats, id_subsidy,
                                                             upload_file, uploadfile,
                                                             name_spec_position, count_spec, actually_working_spec,
                                                             higher_prof, secondary_prof, count_int)

            str_path_pdf = generate_to_word_eight_article(current_id_request,
                                                          points_eight[8], points_eight[10], points_eight[12],
                                                          organization_obj,
                                                          request.POST.get("month_report"), request.POST.get("year_report"),
                                                          request.POST.get("month_next"),
                                                          num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                                          summ_cows,
                                                          requests_eight_subs[0], requests_eight_subs[1],
                                                          requests_eight_subs[2],
                                                          num_begin_year_goats, amount_milk_goats, summ_goats, num_total,
                                                          points_eight[0], points_eight[1], points_eight[2],
                                                          points_eight[3], points_eight[4], points_eight[5],
                                                          points_eight[6], points_eight[7], first_in_group,
                                                          request.POST.get("rate_cows"),
                                                          request.POST.get("rate_goats"))

            path_request_pdf = str_path_pdf
            path_name_pdf_regisry_spec = requests_eight_subs[5]
            path_name_pdf_registry = requests_eight_subs[4]

            pdf_merger = PdfFileMerger()

            str_merge_request_pdf = "media/uploads/" + str(requests_eight_subs[3]) + "_" + str(organization_obj.inn_org) + "_.pdf"

            try:
                pdf_merger.append(str(path_request_pdf))
            except:
                print("нет файла")

            try:
                pdf_merger.append(str(path_name_pdf_registry))
            except:
                print("нет файла")

            try:
                pdf_merger.append(str(path_name_pdf_regisry_spec))
            except:
                print("нет файла")

            with Path(str_merge_request_pdf).open(mode="wb") as output_file:
                pdf_merger.write(output_file)

            str_merge_request_pdf_1 = "/uploads/" + str(requests_eight_subs[3]) + "_" + str(organization_obj.inn_org) + "_.pdf"

            obj_request = Requests.objects.get(id = current_id_request)
            obj_request.print_document_path = str_merge_request_pdf_1

            obj_request.save()

            eight_subs_obj = Requests_Eight_Subs.objects.get(id=requests_eight_subs[3])

            try:
                eight_subs_obj.registry_document_excel = uploadfile
                eight_subs_obj.save()

                path_registry = str(eight_subs_obj.registry_document_excel)
                path_registry = "media/"+path_registry

            except:
                print("файл реестра не сохранен в бд")

        except:
            messages.warning(request, "Такая субсидия уже существует!")
            return redirect(request.META['HTTP_REFERER'])

    messages.success(request, "Заявка создана!")
    return HttpResponseRedirect(reverse('save_edit_eight_request', args=(current_id_request,)))

from django.db.models import Q

def resave_request(item_request, summ_total, user):
    datetime_current = datetime.datetime.now()
    item_request.history_request = item_request.history_request + "\n Переформирование заявки: " + str(datetime_current) + ', пользователь: ' + str(user) + ';'
    item_request.total_summ = summ_total
    item_request.save()

def save_edit_eight_request(request, id):

    try:
        pythoncom.CoInitializeEx(0)
    except:
        print("вызов не понадобился")

    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

    obj_request = Requests.objects.get(id = id)
    eight_subs_obj = Requests_Eight_Subs.objects.get(f_requests = obj_request.id)

    index_report_month = get_index_month(obj_request.f_subsidies.f_report_period.f_period.name_period)

    if (index_report_month == 12):
        index_report_month = 1
    else:
        index_report_month = index_report_month + 1

    name_report_month_rod = get_name_month(index_report_month, 'rod')

    current_date = datetime.datetime.now()
    current_year = current_date.year

    if request.method == "POST" and 'send_request_eight' in request.POST:
        obj_request.f_status = 's3'
        datetime_current = datetime.datetime.now()
        obj_request.history_request = obj_request.history_request + '\n Дата отправки: ' + str(datetime_current) + ', пользователь: ' + str(request.user) + ';'
        obj_request.save()

    if request.method == "POST" and 'reformat_print_eight_request' in request.POST:
        three_point = request.POST.get("point_3")
        fourth_point = request.POST.get("point_4")
        fifth_point = request.POST.get("point_5")

        points_eight_subs_refact = set_points_eight_subs_refact(organization_obj, eight_subs_obj, three_point, fourth_point, fifth_point)

        num_begin_year_cows = request.POST.get("num_begin_year_cows")
        num_begin_month_cows = request.POST.get("num_begin_month_cows")
        amount_milk_cows = request.POST.get("amount_milk_cows")

        summ_cows = request.POST.get("summ_cows")

        num_begin_year_goats = request.POST.get("num_begin_year_goats")
        amount_milk_goats = request.POST.get("amount_milk_goats")
        summ_goats = request.POST.get("summ_goats")

        num_total = request.POST.get("num_total")
        leukemia_livestock = request.POST.get("leukemia_KRS")
        quarantine_livestock = request.POST.get("quarantine")
        data_quarantine_establish_1 = request.POST.get("data_quarantine_establish_1")
        data_quarantine_establish_2 = request.POST.get("data_quarantine_establish_2")

        resave_eight_subs = resave_requests_eight_subs(eight_subs_obj, num_total,
                                                       leukemia_livestock, quarantine_livestock,
                                                       data_quarantine_establish_1, data_quarantine_establish_2,
                                                       num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                                       num_begin_year_goats, amount_milk_goats, summ_cows, summ_goats)
        resave_request(obj_request, resave_eight_subs, request.user)


        exists_in_group_requests = Requests.objects.filter(Q(f_status='s6') | Q(f_status='s7'),
                                                           report_year_subsid = obj_request.report_year_subsid,
                                                           f_subsidies = obj_request.f_subsidies,
                                                           f_organization = authorized_user.f_org,
                                                           removed = False)
        print("exists_in_group_requests", exists_in_group_requests)

        if (exists_in_group_requests.count() != 0):
            first_in_group = "False"
        else:
            first_in_group = "True"

        str_path_pdf = generate_to_word_eight_article(obj_request.id,
                                                      points_eight_subs_refact[0], points_eight_subs_refact[1], points_eight_subs_refact[2],
                                                      organization_obj,
                                                      obj_request.f_subsidies.f_report_period.f_period.name_period, obj_request.f_subsidies.f_report_period.year_sub,
                                                      name_report_month_rod,
                                                      num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                                      summ_cows,
                                                      quarantine_livestock, data_quarantine_establish_1,
                                                      data_quarantine_establish_2,
                                                      num_begin_year_goats, amount_milk_goats, summ_goats, num_total,
                                                      points_eight_subs_refact[3], points_eight_subs_refact[4], points_eight_subs_refact[5],
                                                      points_eight_subs_refact[6], points_eight_subs_refact[7], points_eight_subs_refact[8],
                                                      points_eight_subs_refact[9], points_eight_subs_refact[10], first_in_group,
                                                      request.POST.get("rate_cows"),
                                                      request.POST.get("rate_goats"))

        uploadfile = eight_subs_obj.registry_document_excel

        count_int = int(request.POST.get("count_1"))

        id_specialists = []
        for item in eight_subs_obj.register_specialists.all():
            id_specialists.append(item.id)

        for item in id_specialists:
            cell_1 = request.POST.get(str(item) + "_1")
            cell_2 = request.POST.get(str(item) + "_2")
            cell_3 = request.POST.get(str(item) + "_3")
            cell_4 = request.POST.get(str(item) + "_4")
            cell_5 = request.POST.get(str(item) + "_5")
            if (cell_1 != None and cell_2 != None and cell_3 != None and cell_4 != None and cell_5 != None):
                item_specialists_register = Specialists_register.objects.get(id = item)
                item_specialists_register.name_spec_position = cell_1
                item_specialists_register.count_spec = cell_2
                item_specialists_register.actually_working_spec = cell_3
                item_specialists_register.higher_prof = cell_4
                item_specialists_register.secondary_prof = cell_5
                item_specialists_register.save()
            else:
                item_specialists_register = Specialists_register.objects.get(id=item)
                item_specialists_register.delete()

        for item in range(count_int):
            row_int = item + 1
            row_str = str(row_int)
            cell_1 = request.POST.get(row_str + "_1")
            cell_2 = request.POST.get(row_str + "_2")
            cell_3 = request.POST.get(row_str + "_3")
            cell_4 = request.POST.get(row_str + "_4")
            cell_5 = request.POST.get(row_str + "_5")
            if (cell_1 != None and cell_2 != None and cell_3 != None and cell_4 != None and cell_5 != None):
                item_specialists_register = Specialists_register()
                item_specialists_register.name_spec_position = cell_1
                item_specialists_register.count_spec = cell_2
                item_specialists_register.actually_working_spec = cell_3
                item_specialists_register.higher_prof = cell_4
                item_specialists_register.secondary_prof = cell_5
                item_specialists_register.save()
                eight_subs_obj.register_specialists.add(item_specialists_register)
                eight_subs_obj.save()

        path_name_docx_registry = "media/registry_document/" + "registy_" + str(obj_request.id) + "_" + organization_obj.inn_org + ".docx"
        path_name_pdf_registry = "media/registry_document/" + "registy_" + str(obj_request.id) + "_" + organization_obj.inn_org + ".pdf"
        path_name_docx_regisry_spec = "media/registry_specialist/" + "registy_specialist_" + str(obj_request.id) + "_" + organization_obj.inn_org + ".docx"
        path_name_pdf_regisry_spec = "media/registry_specialist/" + "registy_specialist_" + str(obj_request.id) + "_" + organization_obj.inn_org + ".pdf"

        path_excel = uploadfile
        if path_excel!="":
            regisry_to_pdf(organization_obj, obj_request, path_name_docx_registry, path_excel)

        specialist_regisry_to_pdf(eight_subs_obj.id, path_name_docx_regisry_spec, organization_obj,
                                  eight_subs_obj.num_total_livestock,
                                  eight_subs_obj.num_begin_year_cows)

        pdf_merger = PdfFileMerger()
        str_merge_request_pdf = "media/uploads/" + str(eight_subs_obj.id) + "_" + str(organization_obj.inn_org) + "_.pdf"
        pdf_merger.append(str(str_path_pdf))
        if path_excel != "":
            pdf_merger.append(str(path_name_pdf_registry))
        pdf_merger.append(str(path_name_pdf_regisry_spec))

        with Path(str_merge_request_pdf).open(mode="wb") as output_file:
            pdf_merger.write(output_file)

        str_merge_request_pdf_1 = "/uploads/" + str(eight_subs_obj.id) + "_" + str(organization_obj.inn_org) + "_.pdf"

        obj_request.print_document_path = str_merge_request_pdf_1
        messages.success(request, "Ваше действие успешно выполнено!")

    if request.method == "POST" and 'delete_registry' in request.POST:
        eight_subs_obj.registry_document_excel = ""
        eight_subs_obj.registry_excel_volume_milk = 0
        eight_subs_obj.save()
        messages.success(request, "Ваше действие успешно выполнено!")

    if request.method == "POST" and 'save_edit_eight_request' in request.POST:
        count_error_1 = 0

        three_point = request.POST.get("point_3")
        fourth_point = request.POST.get("point_4")
        fifth_point = request.POST.get("point_5")

        if three_point == "":
            count_error_1 = count_error_1 + 1
        if fourth_point == "":
            count_error_1 = count_error_1 + 1
        if fifth_point == "":
            count_error_1 = count_error_1 + 1

        set_points_eight_subs_refact(organization_obj, eight_subs_obj, three_point, fourth_point, fifth_point)

        num_begin_year_cows = request.POST.get("num_begin_year_cows")
        num_begin_month_cows = request.POST.get("num_begin_month_cows")
        amount_milk_cows = request.POST.get("amount_milk_cows")

        if float(num_begin_year_cows) > float(num_begin_month_cows):
            count_error_1 = count_error_1 + 1

        summ_cows = request.POST.get("summ_cows")

        num_begin_year_goats = request.POST.get("num_begin_year_goats")
        amount_milk_goats = request.POST.get("amount_milk_goats")
        summ_goats = request.POST.get("summ_goats")

        total_volume_milk = float(amount_milk_cows) + float(amount_milk_goats)

        num_total = request.POST.get("num_total")
        leukemia_livestock = request.POST.get("leukemia_KRS")
        quarantine_livestock = request.POST.get("quarantine")
        data_quarantine_establish_1 = request.POST.get("data_quarantine_establish_1")
        data_quarantine_establish_2 = request.POST.get("data_quarantine_establish_2")

        if num_total == float(0):
            count_error_1 = count_error_1 + 1

        if data_quarantine_establish_1=="" and quarantine_livestock == "Установлен":
            count_error_1 = count_error_1 + 1
        if data_quarantine_establish_2=="" and quarantine_livestock == "Установлен":
            count_error_1 = count_error_1 + 1

        resave_eight_subs = resave_requests_eight_subs(eight_subs_obj, num_total,
                                                       leukemia_livestock, quarantine_livestock,
                                                       data_quarantine_establish_1, data_quarantine_establish_2,
                                                       num_begin_year_cows, num_begin_month_cows, amount_milk_cows,
                                                       num_begin_year_goats, amount_milk_goats, summ_cows, summ_goats)
        resave_request(obj_request, resave_eight_subs, request.user)

        try:
            uploadfile = request.FILES['docfile']
            upload_file = True
        except:
            upload_file = False
            uploadfile = ""

        warning_count = 0
        try:
            if upload_file == True:
                volume_milk = get_volume_milk(uploadfile)
                eight_subs_obj.registry_excel_volume_milk = volume_milk
                eight_subs_obj.registry_document_excel = uploadfile
                eight_subs_obj.save()
        except:
            warning_count = 1
            messages.warning(request, "Подгружен некорректный файл реестра!")
        if warning_count == 0:
            messages.success(request, "Ваше действие успешно выполнено!")


        # if eight_subs_obj.registry_excel_volume_milk != total_volume_milk:
        #     count_error_1 = count_error_1 + 1
        # if warning_count != 0:
        #     count_error_1 = count_error_1 + 1

        count_int = int(request.POST.get("count_1"))

        id_specialists = []
        for item in eight_subs_obj.register_specialists.all():
            id_specialists.append(item.id)

        for item in id_specialists:
            cell_1 = request.POST.get(str(item) + "_1")
            cell_2 = request.POST.get(str(item) + "_2")
            cell_3 = request.POST.get(str(item) + "_3")
            cell_4 = request.POST.get(str(item) + "_4")
            cell_5 = request.POST.get(str(item) + "_5")
            if (cell_1 != None and cell_2 != None and cell_3 != None and cell_4 != None and cell_5 != None):
                item_specialists_register = Specialists_register.objects.get(id = item)
                item_specialists_register.name_spec_position = cell_1
                item_specialists_register.count_spec = cell_2
                item_specialists_register.actually_working_spec = cell_3
                item_specialists_register.higher_prof = cell_4
                item_specialists_register.secondary_prof = cell_5
                item_specialists_register.save()
            else:
                item_specialists_register = Specialists_register.objects.get(id=item)
                item_specialists_register.delete()

        for item in range(count_int):
            row_int = item + 1
            row_str = str(row_int)
            cell_1 = request.POST.get(row_str + "_1")
            cell_2 = request.POST.get(row_str + "_2")
            cell_3 = request.POST.get(row_str + "_3")
            cell_4 = request.POST.get(row_str + "_4")
            cell_5 = request.POST.get(row_str + "_5")
            if (cell_1 != None and cell_2 != None and cell_3 != None and cell_4 != None and cell_5 != None):
                item_specialists_register = Specialists_register()
                item_specialists_register.name_spec_position = cell_1
                item_specialists_register.count_spec = cell_2
                item_specialists_register.actually_working_spec = cell_3
                item_specialists_register.higher_prof = cell_4
                item_specialists_register.secondary_prof = cell_5
                item_specialists_register.save()
                eight_subs_obj.register_specialists.add(item_specialists_register)
                eight_subs_obj.save()
        if count_error_1 == 0:
            obj_request.ability_to_send = True
        else:
            obj_request.ability_to_send = False
        obj_request.save()

    points_for_edit = get_points_for_edit()
    count_spec = eight_subs_obj.register_specialists.all().count()
    str_merge_request_pdf = "/media/uploads/" + str(eight_subs_obj.id) + "_" + str(organization_obj.inn_org) + "_.pdf"

    try:
        path_registry = str(eight_subs_obj.registry_document_excel)
        path_registry = "/media/" + path_registry
    except:
        print("нет registry")

    return render(request, "requests/eight_subs/edit_eight_request.html",
                  {'model_item_eight_subs': eight_subs_obj,
                   'model_item_request': obj_request,
                   'Points_subs_1': points_for_edit[0],
                   'Points_subs_2': points_for_edit[1],
                   'Leukemia_subs': points_for_edit[2],
                   'current_year': obj_request.year_sub,
                   'name_report_month_rod': name_report_month_rod,
                   'Quarantine': points_for_edit[3],
                   'total_summ': obj_request.total_summ,
                   'str_merge_path_request_pdf': str_merge_request_pdf,
                   'path_registry': path_registry,
                   'count_1': count_spec})