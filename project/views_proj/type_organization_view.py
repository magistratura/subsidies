from django.shortcuts import render
from django.http import HttpResponseNotFound
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages

def add_type_organization(request):
    if (request.method == 'POST' and 'save_type_organization' in request.POST):
            form = TypeOrganizationForm(request.POST)
            form.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = TypeOrganizationForm(request.POST)
        return render(request, "guides/Type_organizations/add_type_organization.html", {'form': form})

def show_type_organizations(request):
    model_item = TypeOrganization.objects.all()
    return render(request, "guides/Type_organizations/show_type_organizations.html", {'model_item': model_item})

def delete_type_organization(request):
    try:
        id_type_organization = request.POST.get("selected_td_type_organization")
        model_item = TypeOrganization.objects.get(id=int(id_type_organization))
        model_item.delete()
        model_item = TypeOrganization.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")

def edit_type_organization(request, id):
    try:
        model_item = TypeOrganization.objects.get(id=id)
        if request.method == "POST" and 'save_change' in request.POST:
            model_item.name_type_org = request.POST.get("name_type_org")
            model_item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        else:
            return render(request, "guides/Type_organizations/edit_type_organization.html", {'model_item': model_item})
    except model_item.DoesNotExist:
        return HttpResponseNotFound("<h2>not found</h2>")

def add_type_organization(request):
    if (request.method == 'POST' and 'save_type_organization' in request.POST):
            form = TypeOrganizationForm(request.POST)
            form.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = TypeOrganizationForm(request.POST)
        return render(request, "guides/Type_organizations/add_type_organization.html", {'form': form})
