from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
from django.contrib import messages
from django.db import IntegrityError

def add_registration_area(request):
    if (request.method == 'POST' and 'save_registration_area' in request.POST):
        try:
            form = RegistrationAreaForm(request.POST)
            form.save()
            messages.success(request, "Ваше действие успешно выполнено!")
            return redirect(request.META['HTTP_REFERER'])
        except:
            messages.warning(request, "Район регистрации с таким названием уже существует!")
            form = RegistrationAreaForm(request.POST)
            return render(request, "guides/Registration_areas/add_registration_area.html", {'form': form})
    else:
        form = RegistrationAreaForm(request.POST)
        return render(request, "guides/Registration_areas/add_registration_area.html", {'form': form})

def show_registration_area(request):
    model_item = RegistrationArea.objects.all()
    return render(request, "guides/Registration_areas/show_registration_area.html", {'model_item': model_item})

def delete_registration_area(request):
    try:
        id_registration_area = request.POST.get("selected_td_registration_area")
        model_item = RegistrationArea.objects.get(id=int(id_registration_area))
        model_item.delete()
        model_item = RegistrationArea.objects.all()
        messages.success(request, "Ваше действие успешно выполнено!")
        return redirect(request.META['HTTP_REFERER'])
    except model_item.DoesNotExist:
        return redirect(request.META['HTTP_REFERER'])

def edit_registration_area(request, id):
    model_item = RegistrationArea.objects.get(id=id)
    if request.method == "POST" and 'save_change' in request.POST:
        model_item.name_area = request.POST.get("name_area")
        type_area = request.POST.get("type_area_select")
        if (type_area != "----------"):
            if (type_area == "район"):
                model_item.type_area = 'type1'
            if (type_area == "город"):
                model_item.type_area = 'type2'
            model_item.save()
            messages.success(request, "Ваше действие успешно выполнено!")
        else:
            messages.warning(request, "Вы заполнили не все поля!")
        return redirect(request.META['HTTP_REFERER'])
    else:
        area_variant = ["----------", "район", "город"]
        return render(request, "guides/Registration_areas/edit_registration_area.html", {'model_item': model_item,
                                                                                             'area_variant': area_variant})