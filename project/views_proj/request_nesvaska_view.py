from project.views_proj.request_eight_article import *
from PyPDF2 import PdfFileMerger
from pathlib import Path
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger
from project.views_proj.generate_to_word_view import *
from project.views_proj.getting_month import *
from django.urls import reverse
import pythoncom
from project.views_proj.request_view import *

def add_nesvaska(request, id):
    selected_subsidies = Subsidies.objects.get(id=id)
    form_nesvaska = RequestsNesvaskaSubsForm()
    return render(request, "requests/nesvaska/add_nesvaska.html", {'selected_subsidies': selected_subsidies,
                                                                   'form_nesvaska': form_nesvaska})

def set_points_nesvaska(three_point, fourth_point, organization_obj):
    if (three_point == 'p1'):
        text_three_point = organization_obj.adress_org
    elif (three_point == 'p2'):
        text_three_point = ""
    elif (three_point == 'p3'):
        text_three_point = organization_obj.phone_org
    else:
        text_three_point = ""

    if (fourth_point == 'p1'):
        text_fourth_point = organization_obj.adress_org
    elif (fourth_point == 'p2'):
        text_fourth_point = ""
    elif (fourth_point == 'p3'):
        text_fourth_point = organization_obj.phone_org
    else:
        text_fourth_point = ""
    return text_three_point, text_fourth_point

def set_points_edit_nesvaska(organization_obj, point_3, point_4):
    if (point_3 == ''):
        text_three_point = ""
        three_point = ""
    elif (point_3 == 'направить по почтовому адресу:'):
        text_three_point = organization_obj.adress_org
        three_point = "p1"
    elif (point_3 == 'направить в личный кабинет получателя субсидии'):
        text_three_point = ""
        three_point = "p2"
    else:
        text_three_point = organization_obj.phone_org
        three_point = "p3"

    if (point_4 == ''):
        text_fourth_point = ""
        fourth_point = ""
    elif (point_4 == 'направить по почтовому адресу:'):
        text_fourth_point = organization_obj.adress_org
        fourth_point = "p1"
    elif (point_4 == 'направить в личный кабинет получателя субсидии'):
        text_fourth_point = ""
        fourth_point = "p2"
    else:
        text_fourth_point = organization_obj.phone_org
        fourth_point = "p3"

    return three_point, text_three_point, fourth_point, text_fourth_point

def create_requests_nesvaska(info_nesvaska_obj, three_point, fourth_point, organization_obj, nesvaska_reports_obj, id_request):
    request_obj = Requests.objects.get(id = id_request)
    try:
        pythoncom.CoInitializeEx(0)
    except:
        print("вызов не понадобился")

    nesvaska_subs_obj = Requests_Nesvaska_Subs()
    nesvaska_subs_obj.rate_cereals = info_nesvaska_obj.rate_cereals
    nesvaska_subs_obj.rate_vegetables = info_nesvaska_obj.rate_vegetables
    nesvaska_subs_obj.rate_potato = info_nesvaska_obj.rate_potato
    nesvaska_subs_obj.three_point = three_point
    nesvaska_subs_obj.fourth_point = fourth_point

    points_nesvaska = set_points_nesvaska(three_point, fourth_point, organization_obj)
    nesvaska_subs_obj.text_three_point = points_nesvaska[0]
    nesvaska_subs_obj.text_fourth_point = points_nesvaska[1]

    nesvaska_subs_obj.total_area_vegetables = nesvaska_reports_obj.vegetables
    nesvaska_subs_obj.total_area_cereals = nesvaska_reports_obj.cereals
    nesvaska_subs_obj.total_area_oilseeds = nesvaska_reports_obj.oilseeds
    nesvaska_subs_obj.total_area_potato = nesvaska_reports_obj.potato
    nesvaska_subs_obj.all_whole_cereals_oilseeds = nesvaska_reports_obj.cereals + nesvaska_reports_obj.oilseeds

    nesvaska_subs_obj.sum_vegetables = round(info_nesvaska_obj.rate_vegetables * nesvaska_reports_obj.vegetables, 2)
    nesvaska_subs_obj.sum_cereals = round(info_nesvaska_obj.rate_cereals * nesvaska_reports_obj.all_whole_crop, 2)
    nesvaska_subs_obj.sum_potato = round(info_nesvaska_obj.rate_potato * nesvaska_reports_obj.potato, 2)

    nesvaska_subs_obj.f_nesvaska_reports = nesvaska_reports_obj
    nesvaska_subs_obj.f_requests = request_obj

    nesvaska_subs_obj.save()
    total_summ = nesvaska_subs_obj.sum_vegetables + nesvaska_subs_obj.sum_cereals + nesvaska_subs_obj.sum_potato
    return total_summ, nesvaska_subs_obj.id

def save_DB_nesvaska(request, id):
    try:
        pythoncom.CoInitializeEx(0)
    except:
        print("вызов не понадобился")

    selected_subsidies = Subsidies.objects.get(id=id)

    year_report = int(selected_subsidies.f_report_period.f_period.name_period)
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

    nesvaska_reports_obj = Nesvaska_Reports.objects.get(
        f_reports_requests__f_organization__inn_org=authorized_user.f_org.inn_org,
        f_reports_requests__status_report='sr6',
        f_reports_requests__f_report_request__report_year=int(selected_subsidies.f_report_period.f_period.name_period))

    info_nesvaska_obj = Info_Nesvaska_Calc_Koefficient.objects.get(report_year = year_report)

    summ_cereals = round(nesvaska_reports_obj.all_whole_crop * info_nesvaska_obj.rate_cereals, 2)
    summ_vegetables = round(nesvaska_reports_obj.vegetables * info_nesvaska_obj.rate_vegetables, 2)
    summ_potato = round(nesvaska_reports_obj.potato * info_nesvaska_obj.rate_potato, 2)
    summ_total = summ_cereals + summ_vegetables + summ_potato

    current_date = datetime.datetime.now()
    current_year = current_date.year
    id_request = save_request(current_year, summ_total, organization_obj, selected_subsidies, year_report, 23, request.user)

    three_point = request.POST.get("three_point")
    fourth_point = request.POST.get("fourth_point")

    request_obj = Requests.objects.get(id = id_request)
    request_obj.ability_to_send = True
    requests_nesvaska = create_requests_nesvaska(info_nesvaska_obj, three_point, fourth_point, organization_obj, nesvaska_reports_obj, id_request)
    request_obj.total_summ = requests_nesvaska[0]

    nesvaska_subs_obj = Requests_Nesvaska_Subs.objects.get(id = requests_nesvaska[1])
    str_path_pdf = generate_to_word_nesvaska(organization_obj, id_request, nesvaska_subs_obj)
    request_obj.print_document_path = str_path_pdf
    request_obj.save()
    messages.success(request, "Ваше действие успешно выполнено!")

    return HttpResponseRedirect(reverse('edit_nesvaska_request', args=(id_request,)))

def edit_nesvaska_request(request, id):
    POINTS_1 = ['направить по почтовому адресу:',
                'направить в личный кабинет получателя субсидии',
                'вручить лично, предварительно оповестив по телефону*;']

    requests_obj = Requests.objects.get(id = id)
    requests_nsvaska = Requests_Nesvaska_Subs.objects.get(f_requests = id)
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)
    str_path_request_pdf = "/media/uploads/" + str(requests_obj.id) + "_" + str(organization_obj.inn_org) + ".pdf"

    return render(request, "requests/nesvaska/edit_nesvaska.html", {'requests_obj': requests_obj, 'POINTS_1': POINTS_1,
                                                                    'requests_nsvaska': requests_nsvaska,
                                                                    'str_path_request_pdf': str_path_request_pdf,
                                                                    'full_name_org': organization_obj.full_name_org})

def save_edit_nesvaska_request(request, id):

    requests_obj = Requests.objects.get(id = id)
    requests_nsvaska = Requests_Nesvaska_Subs.objects.get(f_requests = id)

    point_3 = request.POST.get("point_3")
    point_4 = request.POST.get("point_4")

    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)
    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

    points_edit_nesvaska = set_points_edit_nesvaska(organization_obj, point_3, point_4)

    requests_nsvaska.three_point = points_edit_nesvaska[0]
    requests_nsvaska.text_three_point = points_edit_nesvaska[1]

    requests_nsvaska.fourth_point = points_edit_nesvaska[2]
    requests_nsvaska.text_fourth_point = points_edit_nesvaska[3]

    requests_nsvaska.save()

    messages.success(request, "Ваше действие успешно выполнено!")
    return HttpResponseRedirect(reverse('edit_nesvaska_request', args=(requests_obj.id,)))

def reformat_print_nesvaska_request(request, id):
    try:
        pythoncom.CoInitializeEx(0)
    except:
        print("вызов не понадобился")
    requests_obj = Requests.objects.get(id = id)
    requests_nsvaska = Requests_Nesvaska_Subs.objects.get(f_requests = id)
    authorized_user = OrganizationWithUser.objects.get(f_user__user=request.user)

    organization_obj = Organization.objects.get(inn_org=authorized_user.f_org)

    str_path_pdf = generate_to_word_nesvaska(organization_obj, requests_obj.id, requests_nsvaska)
    requests_obj.print_document_path = str_path_pdf
    requests_obj.save()
    messages.success(request, "Ваше действие успешно выполнено!")
    return HttpResponseRedirect(reverse('edit_nesvaska_request', args=(requests_obj.id,)))
















