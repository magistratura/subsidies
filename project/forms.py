from django import forms
from .models import *
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

class RegistrationAreaForm(forms.ModelForm):
    class Meta:
        model = RegistrationArea
        fields = '__all__'

class TypeOrganizationForm(forms.ModelForm):
    class Meta:
        model = TypeOrganization
        fields = '__all__'

class BankForm(forms.ModelForm):
    class Meta:
        model = Bank
        fields = '__all__'

class TypeUserForm(forms.ModelForm):
    class Meta:
        model = TypeUser
        fields = '__all__'

class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = '__all__'

class PeriodicityForm(forms.ModelForm):
    class Meta:
        model = Periodicity
        fields = '__all__'

class ArticleLawForm(forms.ModelForm):
    class Meta:
        model = ArticlesLaw
        fields = '__all__'

class NameReportingPeriodForm(forms.ModelForm):
    class Meta:
        model = NameReportingPeriod
        fields = '__all__'

class ReportingPeriodsForm(forms.ModelForm):
    class Meta:
        model = ReportingPeriods
        fields = '__all__'

class SubsidiesForm(forms.ModelForm):
    class Meta:
        model = Subsidies
        fields = '__all__'

class RequestsForm(forms.ModelForm):
    class Meta:
        model = Requests
        fields = '__all__'

class OrganizationWithUserForm(forms.ModelForm):
    class Meta:
        model = OrganizationWithUser
        fields = '__all__'

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = '__all__'

class StatusSSRForm(forms.ModelForm):
    class Meta:
        model = Status_SSR
        fields = '__all__'

class SSREightSubsForm(forms.ModelForm):
    class Meta:
        model = SSR_Eight_Subs
        fields = '__all__'

class SSRForm(forms.ModelForm):
    class Meta:
        model = SSR
        fields = '__all__'

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = '__all__'

class AuthUserForm(AuthenticationForm, forms.ModelForm):
    class Meta:
        model = User
        fields = ('username','password')
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

class RegisterUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'is_active')
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        user.is_active = True
        if commit:
            user.save()
            item = UserProfile(user=user, attached_org=False)
            item.save()
        return user

class RequestsEightSubsForm(forms.ModelForm):
    class Meta:
        model = Requests_Eight_Subs
        fields = '__all__'

class NameReportsRequestsForm(forms.ModelForm):
    class Meta:
        model = Name_Reports_Requests
        fields = '__all__'

class NesvaskaReportsForm(forms.ModelForm):
    class Meta:
        model = Nesvaska_Reports
        fields = '__all__'

class ReportsRequestsForm(forms.ModelForm):
    class Meta:
        model = Reports_Requests
        fields = '__all__'

class AnaliticsReportsForm(forms.ModelForm):
    class Meta:
        model = Analitics_Reports
        fields = '__all__'

class NameAnaliticsReportsForm(forms.ModelForm):
    class Meta:
        model = Name_Analitics_Reports
        fields = '__all__'

class InfoNesvaskaCalcKoefficientForm(forms.ModelForm):
    class Meta:
        model = Info_Nesvaska_Calc_Koefficient
        fields = '__all__'


class RequestsNesvaskaSubsForm(forms.ModelForm):
    class Meta:
        model = Requests_Nesvaska_Subs
        fields = '__all__'


class SSRNesvaskaSubsForm(forms.ModelForm):
    class Meta:
        model = SSR_Nesvaska_Subs
        fields = '__all__'


class RateForm(forms.ModelForm):
    class Meta:
        model = Rate
        fields = '__all__'
