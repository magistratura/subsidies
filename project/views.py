from django.shortcuts import render
from django.shortcuts import redirect
from project.forms import *
import datetime
from django.contrib import messages

def send_report_request(request):
    status_request_report = request.POST.get("status_request_report")
    id_reports_requests = request.POST.get("id_reports_requests")
    status_action_report = request.POST.get("status_action_report")

    reports_requests_obj = Reports_Requests.objects.get(id=id_reports_requests)
    if (status_action_report == "Изменить отчет"):
        reports_requests_obj.reason_deny = ""
        reports_requests_obj.status_report = 'sr1'
        reports_requests_obj.save()
    else:
        code_status = ""
        status_report_request = {'Новая': 'Отправлено',
                                  'Отправлено': 'Проверено МО',
                                  'Проверено МО': 'Проверено ОТР',
                                  'Проверено ОТР': 'Принято',
                                  'Отказано': 'Изменить отчет',
                                  'Принято': 'Принято'}
        next_status = ""
        for key, value in status_report_request.items():
            if (key == status_request_report):
                next_status = value

        status_variant = {"Новая": 'sr1', "Отказано": 'sr2',
                          "Отправлено": 'sr3', "Проверено МО": 'sr4',
                          "Проверено ОТР": 'sr5', "Принято": 'sr6'}

        value_history = ""
        for key, value in status_variant.items():
            if (key == next_status):
                code_status = value
                value_history = key

        reports_requests_obj.status_report = code_status

        datetime_current = datetime.datetime.now()
        reports_requests_obj.history_reports_requests = reports_requests_obj.history_reports_requests + '\n' + value_history + ": " + str(datetime_current) + ', пользователь: ' + str(request.user) + ';'
        reports_requests_obj.save()

    messages.success(request, "Ваше действие успешно выполнено!")
    return redirect(request.META['HTTP_REFERER'])


def send_request(request):
    status_request = request.POST.get("status_request")
    id_subsid = request.POST.get("id_subsid")
    num_subsid = request.POST.get("num_subsid")

    request_obj = Requests.objects.get(id=id_subsid)
    status_action = request.POST.get("status_action")
    if (status_action == "Изменить заявку"):
        print(status_action)
        request_obj.reason_deny = ""
        request_obj.f_status = 's1'
        request_obj.save()
    else:
        code_status = ""
        status_request_variant = {'Новая': 'Отправлено',
                                  'Отправлено': 'Проверено МО',
                                  'Проверено МО': 'Проверено ОТР',
                                  'Проверено ОТР': 'Проверено ОИБ',
                                  'Проверено ОИБ': 'Выплачено',
                                  'Отказано': 'Изменить заявку',
                                  'Выплачено': 'Выплачено'}

        next_status = ""
        for key, value in status_request_variant.items():
            if (key == status_request):
                next_status = value

        status_variant = {"Новая": 's1', "Отказано": 's2',
                          "Отправлено": 's3', "Проверено МО": 's4',
                          "Проверено ОТР": 's5', "Проверено ОИБ": 's6',
                          "Выплачено": 's7'}

        value_history = ""
        for key, value in status_variant.items():
            if (key == next_status):
                code_status = value
                value_history = key

        request_obj.f_status = code_status

        full_history = request_obj.history_request
        datetime_current = datetime.datetime.now()
        full_history = full_history + '\n' + value_history + ": " + str(datetime_current) + ', пользователь: ' + str(request.user) + ';'

        request_obj.history_request = full_history
        request_obj.save()

    messages.success(request, "Ваше действие успешно выполнено!")
    return redirect(request.META['HTTP_REFERER'])


def save_deny_request(request, id):
    reason_deny = request.POST.get("textarea_deny")
    if (reason_deny!=""):
        request_obj = Requests.objects.get(id=id)
        datetime_current = datetime.datetime.now()
        full_history = request_obj.history_request + "\n Отказано: " + str(datetime_current) + ", пользователь: " + str(request.user) + ". Причина отказа: " + reason_deny

        request_obj.f_status = 's2'
        request_obj.reason_deny = reason_deny
        request_obj.history_request = full_history

        request_obj.save()
        messages.success(request, "Ваше действие успешно выполнено!")
    else:
        messages.warning(request, "Вы не ввели причину отказа!")
    return redirect(request.META['HTTP_REFERER'])


def save_deny_request_report(request, id):
    reason_deny = request.POST.get("textarea_deny-request-report")
    if (reason_deny!=""):
        reports_requests_obj = Reports_Requests.objects.get(id=id)
        datetime_current = datetime.datetime.now()
        reports_requests_obj.history_reports_requests = reports_requests_obj.history_reports_requests + "\n Отказано: " + str(datetime_current) + ", пользователь: " + str(request.user) + ". Причина отказа: " + reason_deny

        reports_requests_obj.status_report = 'sr2'
        reports_requests_obj.reason_deny = reason_deny

        reports_requests_obj.save()
        messages.success(request, "Ваше действие успешно выполнено!")
    else:
        messages.warning(request, "Вы не ввели причину отказа!")
    return redirect(request.META['HTTP_REFERER'])

def front_page(request):
    return render(request, "front_page.html", locals())

def show_guides(request):
    return render(request, "guides/show_guides.html", locals())






