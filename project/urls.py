from django.urls import path
from . import views
from project.views_proj import articles_law_view
from project.views_proj import banks_view
from project.views_proj import name_reporting_periods_views
from project.views_proj import organizations_view
from project.views_proj import periodicities_view
from project.views_proj import user_system_view
from project.views_proj import registration_area_view
from project.views_proj import type_organization_view
from project.views_proj import request_view
from project.views_proj import registr_and_auth_view
from project.views_proj import reporting_periods_view
from project.views_proj import subsidies_view
from project.views_proj import ssr_view
from project.views_proj import report_subsidies
from project.views_proj import analytics_reports_view
from project.views_proj import informations_calc_view
from project.views_proj import generate_to_excel
from project.views_proj import request_nesvaska_view
from project.views_proj import request_eight_article
from project.views_proj import select_subsidies_view


urlpatterns = [
    path('', views.front_page, name="front_page"),
    path('show_guides', views.show_guides, name="show_guides"),

    path('add_bank', banks_view.add_bank),
    path('show_banks', banks_view.show_banks),
    path('delete_bank', banks_view.delete_bank),
    path('edit_bank/<int:id>/', banks_view.edit_bank),

    path('add_article_law', articles_law_view.add_article_law),
    path('show_articles_law', articles_law_view.show_articles_law),
    path('delete_articles_law', articles_law_view.delete_articles_law),
    path('edit_article_law/<int:id>/', articles_law_view.edit_article_law),

    path('add_reporting_period', name_reporting_periods_views.add_name_reporting_period),
    path('show_reporting_periods', name_reporting_periods_views.show_reporting_periods),
    path('delete_reporting_period', name_reporting_periods_views.delete_name_reporting_period),
    path('edit_reporting_period/<int:id>/', name_reporting_periods_views.edit_name_reporting_period),

    path('add_organization', organizations_view.add_organization),
    path('show_organization', organizations_view.show_organizations, name='show_organizations'),
    path('delete_organization', organizations_view.delete_organization),
    path('edit_organization/<int:id>/', organizations_view.edit_organization),
    path('show_selected_organization/<int:id>/', organizations_view.show_selected_organization),

    path('add_periodicity', periodicities_view.add_periodicity),
    path('show_periodicities', periodicities_view.show_periodicities),
    path('delete_periodicity', periodicities_view.delete_periodicity),
    path('edit_periodicity/<int:id>/', periodicities_view.edit_periodicity),

    path('add_user_system', user_system_view.add_user_system),
    path('show_users_system', user_system_view.show_users_system),
    path('delete_user_system', user_system_view.delete_user_system),
    path('edit_user_system/<int:id>/', user_system_view.edit_user_system),
    path('show_form_users_system', user_system_view.show_form_users_system),
    path('add_user_in_org', user_system_view.add_user_in_org),
    path('show_users_in_org', user_system_view.show_users_in_org),
    path('search_users_by_org', user_system_view.search_users_by_org),

    path('add_registration_area', registration_area_view.add_registration_area),
    path('show_registration_area', registration_area_view.show_registration_area),
    path('delete_registration_area', registration_area_view.delete_registration_area),
    path('edit_registration_area/<int:id>/', registration_area_view.edit_registration_area),

    path('add_type_organization', type_organization_view.add_type_organization),
    path('show_type_organizations', type_organization_view.show_type_organizations),
    path('delete_type_organization', type_organization_view.delete_type_organization),
    path('edit_type_organization/<int:id>/', type_organization_view.edit_type_organization),

    path('add_report_period', reporting_periods_view.add_reporting_period),
    path('show_reporting_period', reporting_periods_view.show_reporting_period),
    path('delete_report_period', reporting_periods_view.delete_report_period),
    path('edit_report_period/<int:id>/', reporting_periods_view.edit_report_period),

    path('add_subsidy', subsidies_view.add_subsidy),
    path('show_subsidies', subsidies_view.show_subsidies),
    path('edit_subsidies/<int:id>/', subsidies_view.edit_subsidy),

    path('select_period/<int:id_article>/', select_subsidies_view.select_period, name='select_period'),
    path('select_annual_period/<int:id_article>/', select_subsidies_view.select_annual_period, name='select_annual_period'),
    path('allocation_annual_create_subs', select_subsidies_view.allocation_annual_create_subs,name='allocation_annual_create_subs'),
    path('allocation_create_subs', select_subsidies_view.allocation_create_subs),

    path('add_eight_request/<int:id_sub>/', request_eight_article.add_eight_request, name = 'add_eight_request'),
    path('select_article', request_view.select_article),
    path('show_requests', request_view.show_requests, name="show_requests"),
    path('send_request', views.send_request),
    path('save_deny_request/<int:id>/', views.save_deny_request),
    path('show_request/<int:id>/', request_view.show_request),
    path('show_actions_requests', request_view.show_actions_requests),
    path('save_edit_eight_request/<int:id>/', request_view.save_edit_eight_request, name="save_edit_eight_request"),
    path('save_DB_eight_form', request_view.save_DB_eight_form),
    path('delete_request/<int:id>/', request_view.delete_request),
    path('add_nesvaska/<int:id>/', request_nesvaska_view.add_nesvaska, name='add_nesvaska'),
    path('save_DB_nesvaska/<int:id>/', request_nesvaska_view.save_DB_nesvaska, name='save_DB_nesvaska'),
    path('edit_nesvaska_request/<int:id>/', request_nesvaska_view.edit_nesvaska_request, name='edit_nesvaska_request'),
    path('save_edit_nesvaska_request/<int:id>/', request_nesvaska_view.save_edit_nesvaska_request, name='save_edit_nesvaska_request'),
    path('reformat_print_nesvaska_request/<int:id>/', request_nesvaska_view.reformat_print_nesvaska_request, name='reformat_print_nesvaska_request'),

    path('login', registr_and_auth_view.MyprojectLoginView.as_view(), name="login_page"),
    path('register', registr_and_auth_view.RegisterUserView.as_view(), name="register_page"),
    path('logout', registr_and_auth_view.MyProjectLogout.as_view(), name="logout_page"),

    path('create_ssr', ssr_view.create_ssr),
    path('show_actions_ssrs', ssr_view.show_actions_ssrs),
    path('show_list_ssrs', ssr_view.show_list_ssrs),
    path('edit_ssr/<int:id>/', ssr_view.edit_ssr),
    path('show_ssr/<int:id>/', ssr_view.show_ssr),
    path('delete_ssr', ssr_view.delete_ssr),
    path('show_removed_ssrs', ssr_view.show_removed_ssrs),

    path('show_actions_requests_for_subsidies', report_subsidies.show_actions_requests_for_subsidies),
    path('choose_report_request', report_subsidies.choose_report_request),
    path('add_nesvaska_report/<int:id>/', report_subsidies.add_nesvaska_report, name="add_nesvaska_report"),
    path('edit_nesvaska_report/<int:id>/', report_subsidies.edit_nesvaska_report, name="edit_nesvaska_report"),
    path('show_report_subsidy/<int:id>/', report_subsidies.show_report_subsidy, name="show_report_subsidy"),
    path('delete_report_subsidy/<int:id>/', report_subsidies.delete_report_subsidy, name="delete_report_subsidy"),
    path('show_reports_subsidies', report_subsidies.show_reports_subsidies, name="show_reports_subsidies"),
    path('show_report_sub/<int:id>/', report_subsidies.show_report_sub, name="show_report_sub"),
    path('send_report_request', views.send_report_request),
    path('save_deny_request_report/<int:id>/', views.save_deny_request_report),
    path('show_remove_reports_subsidies', report_subsidies.show_remove_reports_subsidies),

    path('show_actions_analytics_reports', analytics_reports_view.show_actions_analytics_reports),
    path('choose_analytics_reports', analytics_reports_view.choose_analytics_reports),
    path('show_analytics_reports', analytics_reports_view.show_analytics_reports, name="show_analytics_reports"),

    path('choose_inform_cacl', informations_calc_view.choose_inform_cacl),
    path('create_inform_cacl_nesvaska', informations_calc_view.create_inform_cacl_nesvaska),
    path('edit_inform_calc/<int:id>/', informations_calc_view.edit_inform_calc, name="edit_inform_calc"),
    path('show_inform_cacls', informations_calc_view.show_inform_cacls),

    path('save_nesvaska_to_excel', generate_to_excel.save_nesvaska_to_excel),
]