from django.contrib import admin
from .models import *



admin.site.register(RegistrationArea)
admin.site.register(TypeOrganization)
admin.site.register(Bank)
admin.site.register(TypeUser)
# admin.site.register(UserSystem)
admin.site.register(Organization)
admin.site.register(Periodicity)
admin.site.register(ArticlesLaw)
admin.site.register(NameReportingPeriod)
admin.site.register(Subsidies)
admin.site.register(Requests)
admin.site.register(OrganizationWithUser)
admin.site.register(ReportingPeriods)
admin.site.register(UserProfile)
admin.site.register(Status)
admin.site.register(SSR)
admin.site.register(Status_SSR)
admin.site.register(Requests_Eight_Subs)
admin.site.register(SSR_Eight_Subs)
admin.site.register(Specialists_register)
admin.site.register(Name_Reports_Requests)
admin.site.register(Nesvaska_Reports)
admin.site.register(Reports_Requests)
admin.site.register(Analitics_Reports)
admin.site.register(Name_Analitics_Reports)
admin.site.register(Info_Nesvaska_Calc_Koefficient)
admin.site.register(Requests_Nesvaska_Subs)
admin.site.register(SSR_Nesvaska_Subs)
admin.site.register(Rate)







