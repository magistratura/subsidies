from django.db import models
from django.contrib.auth.models import User

#Район регистрации
class RegistrationArea(models.Model):
    TYPE_AREA_VARIANT = (
        ('type1', "район"),
        ('type2', "город"),
    )
    name_area = models.CharField("Район регистрации", max_length=200, unique=True, null=True)
    type_area = models.CharField("Тип района регистрации", max_length=30, choices=TYPE_AREA_VARIANT, blank=True, null=True)

    def __str__(self):
        return self.name_area
    class Meta:
        verbose_name = "Район"
        verbose_name_plural = "Районы"

#Тип организации
class TypeOrganization(models.Model):
    name_type_org = models.CharField("Тип организации", max_length=100)
    def __str__(self):
        return self.name_type_org
    class Meta:
        verbose_name = "Тип организации"
        verbose_name_plural = "Типы организаций"

#Банк
class Bank(models.Model):
    bik_bank = models.CharField("БИК банка", max_length=9, unique=True)
    name_bank = models.CharField("Наименование банка", max_length=100)
    inn_bank = models.CharField("ИНН банка", max_length=12)
    correspond_bank = models.CharField("Корреспондентский счёт", max_length=20)
    kpp_bank = models.CharField("КПП банка", max_length=9)
    def __str__(self):
        return self.bik_bank
    class Meta:
        verbose_name = "Банковский реквизит"
        verbose_name_plural = "Банковские реквизиты"

#Тип пользователя
class TypeUser(models.Model):
    name_type_user = models.CharField("Тип пользователя", max_length=100)
    def __str__(self):
        return self.name_type_user
    class Meta:
        verbose_name = "Тип пользователя"
        verbose_name_plural = "Типы пользователей"

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    attached_org = models.BooleanField(default=False)
    def __unicode__(self):
        return str(self.id)
    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

#Организация
class Organization(models.Model):
    full_name_org = models.CharField("Полное название организации", max_length=500)
    short_name_org = models.CharField("Сокращенное название организации", max_length=120)
    f_reg_area = models.ForeignKey(RegistrationArea, verbose_name="Район регистрации", on_delete=models.SET_NULL, null=True)
    adress_org = models.CharField("Адрес", max_length=500)
    phone_org = models.CharField("Телефон", max_length=11, blank=True)
    email_org = models.EmailField("Email", blank=True)
    oktmo_org = models.CharField("ОКТМО", max_length=11)
    okpo_org = models.CharField("ОКПО", max_length=10)
    inn_org = models.CharField("ИНН", max_length=13, unique=True)
    account_number_org = models.CharField("Номер расчетного счета", max_length=20, blank=True)
    m_banks = models.ForeignKey(Bank, verbose_name="Банки", on_delete=models.SET_NULL, null=True, blank=True)
    f_type = models.ForeignKey(TypeOrganization, verbose_name="Тип организации", on_delete=models.SET_NULL, null=True)
    fio_manager_org = models.CharField("ФИО руководителя", max_length=200)
    fio_rod_manager_org = models.CharField("ФИО руководителя в родительном падеже", max_length=200, blank=True)
    title_document_org = models.CharField("Правоустанавливающий документ", max_length=600, blank=True)
    def __str__(self):
        return self.inn_org
    class Meta:
        verbose_name = "Организация"
        verbose_name_plural = "Организации"

#Организации и пользователи
class OrganizationWithUser(models.Model):
    f_org = models.ForeignKey(Organization, verbose_name="Организация", on_delete=models.SET_NULL, null=True)
    f_user = models.ForeignKey(UserProfile, verbose_name="Сельхозтоваропроизводитель", on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        unique_together = (('f_org', 'f_user'),)
        verbose_name = "Организация и пользователь"
        verbose_name_plural = "Организации и пользователи"

#Периодичность предоставления (ежемесячно, ежегодно, ежеквартально...)
class Periodicity(models.Model):
    name_period = models.CharField("Периодичность", max_length=50, unique=True)
    def __str__(self):
        return self.name_period
    class Meta:
        verbose_name = "Переодичность"
        verbose_name_plural = "Периодичности"

#Статья закона
class ArticlesLaw(models.Model):
    name_article = models.CharField("Название статей", max_length=300)
    f_period_article = models.ForeignKey(Periodicity, verbose_name="Периодичность", on_delete=models.SET_NULL, null=True)
    num_article = models.CharField("Номер статьи", max_length=10)
    def __str__(self):
        return self.name_article
    class Meta:
        verbose_name = "Название статьи"
        verbose_name_plural = "Название статей"

#Наименование отчетных период (январь, февраль, I квартал... )
class NameReportingPeriod(models.Model):
    name_period = models.CharField("Наименование периодов", max_length=100)
    def __str__(self):
        return self.name_period
    class Meta:
        verbose_name = "Наименование отчетного периода"
        verbose_name_plural = "Наименование отчетных периодов"

class ReportingPeriods(models.Model):
    f_period = models.ForeignKey(NameReportingPeriod, verbose_name="Отчетный период", on_delete=models.SET_NULL,
                                 null=True)
    year_sub = models.PositiveSmallIntegerField("Отчетный год", null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        unique_together = (('f_period', 'year_sub'),)
        verbose_name = "Отчетный период"
        verbose_name_plural = "Отчетные периоды"

#Субсидии
class Subsidies(models.Model):
    f_article_law = models.ForeignKey(ArticlesLaw, verbose_name="Статья закона", on_delete=models.SET_NULL, null=True)
    f_report_period = models.ForeignKey(ReportingPeriods, verbose_name="Отчетный период", on_delete=models.SET_NULL, null=True, blank=True)
    data_start_recive = models.DateField("Дата начала приема", null=True, blank=True)
    data_end_recive = models.DateField("Дата окончания приема", null=True, blank=True)
    used_ssr = models.BooleanField(default=False)
    data_start_recive_city = models.DateField("Дата начала приема (город)", null=True, blank=True)
    data_end_recive_city = models.DateField("Дата окончания приема (город)", null=True, blank=True)

    def __str__(self):
        return str(self.id)
    class Meta:
        unique_together = (('f_article_law', 'f_report_period'),)
        verbose_name = "Субсидия"
        verbose_name_plural = "Субсидии"

#Статусы заявок
class Status(models.Model):
    status_name = models.CharField("Наименование статуса", max_length=50)
    def __str__(self):
        return self.status_name
    class Meta:
        verbose_name = "Статус заявки"
        verbose_name_plural = "Статусы заявок"

#Заявка
class Requests(models.Model):
    STATUS_VARIANT = (
        ('s1', "Новая"),
        ('s2', "Отказано"),
        ('s3', "Отправлено"),
        ('s4', "Проверено МО"),
        ('s5', "Проверено ОТР"),
        ('s6', "Проверено ОИБ"),
        ('s7', "Выплачено"),
    )
    history_request = models.TextField("История заявки", null=True, blank=True)
    data_create = models.DateTimeField("Дата создания заявки", auto_now=True)
    date_send = models.DateTimeField("Дата отправки районным специалстом", null=True, blank=True)
    data_reg_spec = models.DateTimeField("Дата согласования районным специалстом", null=True, blank=True)
    data_otr_spec = models.DateTimeField("Дата согласования отраслевым специалстом", null=True, blank=True)
    data_oib_spec = models.DateTimeField("Дата согласования ОИБ специалстом", null=True, blank=True)
    data_done = models.DateTimeField("Дата отправки средств СХП", null=True, blank=True)
    year_sub = models.PositiveSmallIntegerField("Отчетный год", default=2021, null=True, blank=True)
    total_summ = models.FloatField("Сумма субсидии", default=0, null=True)
    f_organization = models.ForeignKey(Organization, verbose_name="Организация", on_delete=models.SET_NULL,
                            null=True)
    print_document_path = models.FileField(upload_to='uploads/', verbose_name="Документ", null=True, blank=True)
    f_status = models.CharField(max_length=30, choices=STATUS_VARIANT)
    num_subsid = models.PositiveSmallIntegerField("Номер статьи", null=True)
    report_year_subsid = models.PositiveSmallIntegerField("Отчетный год", null=True, blank = True)
    f_subsidies = models.ForeignKey(Subsidies,  verbose_name="Статья закона", on_delete=models.SET_NULL, null=True)
    used_ssr = models.BooleanField(default=False)
    reason_deny = models.CharField("Причина отказа", max_length=1000, null=True, blank=True)
    removed = models.BooleanField(default=False, null=True, blank=True)
    ability_to_send = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Заявка"
        verbose_name_plural = "Заявки"

class Status_SSR(models.Model):
    name_status = models.CharField("Наименование статуса", max_length=100, blank=True)
    def __str__(self):
        return self.name_status
    class Meta:
        verbose_name = "Статус ССР"
        verbose_name_plural = "Статусы ССР"

class SSR(models.Model):
    STATUS_SSR_VARIANT = (
        ('ss1', "Черновик"),
        ('ss2', "Принято"),
    )
    f_articles_law = models.ForeignKey(ArticlesLaw, verbose_name="Статья закона", on_delete=models.SET_NULL, null=True)
    f_status = models.CharField("Статус ССР", max_length=30, choices=STATUS_SSR_VARIANT, blank=True, null=True)
    name_title = models.TextField("Наименование шапки ССР", max_length=1200, blank=True)
    name_period = models.CharField("Наименование периода", max_length=300, blank=True)
    fio_spec_oib = models.CharField("ФИО специалиста ОИБ", max_length=80, blank=True)
    phone_spec_oib = models.CharField("Телефон специалиста ОИБ", max_length=80, blank=True)
    fio_deputy_minister = models.CharField("ФИО заместителя министра", max_length=80, blank=True)
    change_history = models.TextField("История изменений", blank=True)
    data_create = models.DateTimeField("Дата создания", null=True, blank=True)
    data_adopt = models.DateTimeField("Дата принятия", null=True, blank=True)
    excel_document_path = models.FileField(upload_to='ssrs/', verbose_name="Excel документ", null=True, blank=True)
    report_year = models.PositiveSmallIntegerField("Отчетный год", null=True, blank = True)
    removed = models.BooleanField(default=False, null=True, blank = True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Сводная справка - расчет"
        verbose_name_plural = "Сводные справки - расчет"

class SSR_Eight_Subs(models.Model):
    f_ssr = models.ForeignKey(SSR, verbose_name="Сводная справка - расчет", on_delete=models.SET_NULL, null=True)
    name_region = models.CharField("Муниципальный округ", max_length=500, null=True, blank=True)
    name_organization = models.CharField("Получатель", max_length=200, null=True, blank=True)
    type_milk_1 = models.CharField("Вид молока_1", max_length=30, null=True, blank=True)
    type_milk_2 = models.CharField("Вид молока_2", max_length=30, null=True, blank=True)
    amount_milk_cows = models.FloatField("Количество произведенного и реализованного коровьего молока", default = 0)
    amount_milk_goats = models.FloatField("Количество произведенного и реализованного козьего молока", default = 0)
    rate_cows = models.FloatField("Ставка коровьего молока", default = 0)
    rate_goats = models.FloatField("Ставка козьего молока", default = 0)
    sum_accrued_cows = models.FloatField("Сумма начисленной субсидии коровьего молока", default = 0)
    sum_accrued_goats = models.FloatField("Сумма начисленной субсидии козьего молока", default = 0)
    sum_paid_cows = models.FloatField("Сумма выплаченная с начала года коровьего молока", default = 0)
    sum_paid_goats = models.FloatField("Сумма выплаченная с начала года козьего молока", default = 0)
    sum_report_cows = models.FloatField("Сумма за отчетный период коровьего молока", default = 0)
    sum_report_goats = models.FloatField("Сумма за отчетный период козьего молока", default = 0)
    amount_milk = models.FloatField("Итоговое количество молока", default=0, null=True, blank=True)
    sum_accrued = models.FloatField("Итоговая сумма начисленной субсидии", default = 0, null=True, blank=True)
    sum_paid = models.FloatField("Итоговая сумма выплаченная с начала года", default = 0, null=True, blank=True)
    sum_report = models.FloatField("Итоговая сумма за отчетный период", default = 0, null=True, blank=True)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Сводная справка - расчет 8 статьи"
        verbose_name_plural = "Сводные справки - расчет 8 статьи"


class Specialists_register(models.Model):
    name_spec_position = models.CharField("Наименование должности", max_length=60, null=True, blank=True)
    count_spec = models.PositiveSmallIntegerField("Количество специалистов", null=True, blank=True)
    actually_working_spec = models.PositiveSmallIntegerField("Фактически работает человек", null=True, blank=True)
    higher_prof = models.PositiveSmallIntegerField("Имеют высшее образование", null=True, blank=True)
    secondary_prof = models.PositiveSmallIntegerField("Имеют среднее профессиональное образование", null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Реестр специалистов"
        verbose_name_plural = "Реестры специалистов"

class Requests_Eight_Subs(models.Model):
    POINTS_SUBS_1 = (
        ('p1_3', 'вручить лично, предварительно оповестив по телефону*;'),
        ('p2_4', 'направить по почтовому адресу:'),
        ('p3_5', 'направить по адресу электронной почты'),
        ('p4_6', 'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**'),
    )
    POINTS_SUBS_2 = (
        ('p2_3', 'вручить лично, предварительно оповестив по телефону*;'),
        ('p3_4', 'направить по почтовому адресу:'),
        ('p4_5', 'направить в личный кабинет в государственной информационной системе «Субсидия АПК 24»**'),
    )
    LEUKEMIA_VARIANT = (
        ('v1', "Был лейкоз"),
        ('v2', "Не было лейкоза"),
    )

    QUARANTINE_VARIANT = (
        ('q1', "Установлен"),
        ('q2', "Не установлен"),
    )

    rate_cows = models.FloatField("Ставка коровьего молока", default = 0)
    rate_goats = models.FloatField("Ставка козьего молока", default = 0)

    three_point = models.CharField(max_length=30, choices=POINTS_SUBS_1, null=True, blank=True)
    text_three_point = models.CharField(max_length=70, null=True, blank=True)
    fourth_point = models.CharField(max_length=30, choices=POINTS_SUBS_1, null=True, blank=True)
    text_fourth_point = models.CharField(max_length=70, null=True, blank=True)
    fifth_point = models.CharField(max_length=30, choices=POINTS_SUBS_2, null=True, blank=True)
    text_fifth_point = models.CharField(max_length=70, null=True, blank=True)

    num_total_livestock = models.PositiveSmallIntegerField("Общее количество скота", null=True, blank=True)

    leukemia_livestock = models.CharField(max_length=30, choices=LEUKEMIA_VARIANT, null=True, blank=True)
    quarantine_livestock = models.CharField(max_length=30, choices=QUARANTINE_VARIANT, null=True, blank=True)
    data_quarantine_establish_1 = models.DateField("Дата установления карантина и иных ограничений", null=True, blank=True)
    data_quarantine_establish_2 = models.DateField("Дата снятия карантина и иных ограничений", null=True, blank=True)

    num_begin_year_cows = models.PositiveSmallIntegerField("Поголовье коров на 1 января 2021 года предоставления субсидии", null=True, blank=True)
    num_begin_month_cows = models.PositiveSmallIntegerField("Поголовье коров на 1-е число месяца, следующего за отчетным периодом", null=True, blank=True)
    amount_milk_cows = models.FloatField("Количество произведенного и реализованного коровьего молока", default = 0, null=True, blank=True)

    num_begin_month_goats = models.PositiveSmallIntegerField("Поголовье коз на 1 -е число месяца, следующего за отчетным периодом", null=True, blank=True)
    amount_milk_goats = models.FloatField("Количество произведенного и реализованного козьего молока", default = 0, null=True, blank=True)

    f_requests = models.OneToOneField(Requests, on_delete = models.CASCADE)
    sum_cows = models.FloatField("Сумма по коровьему молоку", default = 0, null=True, blank=True)
    sum_goats = models.FloatField("Сумма по козьему молоку", default = 0, null=True, blank=True)

    register_specialists = models.ManyToManyField(Specialists_register, null=True, blank=True)
    registry_document_excel = models.FileField(upload_to='registry_document_excel/', verbose_name="Реестр", null=True, blank=True)
    registry_excel_volume_milk = models.FloatField("Объем молока из реестра", default = 0, null=True, blank=True)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Заявка по 8 статье"
        verbose_name_plural = "Заявки по 8 статье"

class Name_Analitics_Reports(models.Model):
    name_analitics_reports = models.CharField("Наименование аналитических отчетов", max_length=400, unique=True, null=True)
    report_year = models.PositiveSmallIntegerField("Отчетный год", null=True, blank=True)
    num_article = models.CharField("Номер статьи", max_length=10, null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Наименование аналитических отчетов"
        verbose_name_plural = "Наименование аналитических отчетов"

class Name_Reports_Requests(models.Model):
    name_report_request = models.CharField("Наименование отчетов", max_length=400, unique=True, null=True)
    report_year = models.PositiveSmallIntegerField("Отчетный год", default=2020, null=True, blank=True)
    num_article = models.CharField("Номер статьи", max_length=10, null=True, blank=True)
    data_start_recive = models.DateField("Дата начала приема", null=True, blank=True)
    data_end_recive = models.DateField("Дата окончания приема", null=True, blank=True)
    data_start_recive_city = models.DateField("Дата начала приема (город)", null=True, blank=True)
    data_end_recive_city = models.DateField("Дата окончания приема (город)", null=True, blank=True)
    m_name_analitics_reports = models.ManyToManyField(Name_Analitics_Reports, null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Наименование отчетов для субсидии"
        verbose_name_plural = "Наименование отчетов для субсидий"

class Reports_Requests(models.Model):
    STATUS_REPORT_VARIANT = (
        ('sr1', "Новая"),
        ('sr2', "Отказано"),
        ('sr3', "Отправлено"),
        ('sr4', "Проверено МО"),
        ('sr5', "Проверено ОТР"),
        ('sr6', "Принято"),
    )
    history_reports_requests = models.TextField("История отчета по субсидии", null=True, blank=True)
    data_create = models.DateTimeField("Дата создания отчета по субсидии", null=True, blank=True)
    date_region = models.DateTimeField("Дата согласования районным специалстом", null=True, blank=True)
    data_otr_spec = models.DateTimeField("Дата согласования отраслевым специалстом", null=True, blank=True)
    f_report_request = models.ForeignKey(Name_Reports_Requests, verbose_name="Отчет", on_delete=models.SET_NULL,
                                         null=True)
    f_organization = models.ForeignKey(Organization, verbose_name="Организация", on_delete=models.SET_NULL,
                            null=True)
    print_document_path = models.FileField(upload_to='report_nesvaska/', verbose_name="Справка о размерах посевных", null=True, blank=True)
    reason_deny = models.CharField("Причина отказа", max_length=1000, null=True, blank=True)
    removed = models.BooleanField(default=False, null=True, blank=True)
    current_year = models.PositiveSmallIntegerField("Год предоставления", default=2020, null=True, blank=True)
    status_report = models.CharField(max_length=30, choices=STATUS_REPORT_VARIANT, null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Отчет для субсидии"
        verbose_name_plural = "Отчеты для субсидий"

class Nesvaska_Reports(models.Model):
    cereals = models.FloatField("Зерновые", default = 0, null=True, blank=True)
    oilseeds = models.FloatField("Масличные", default = 0, null=True, blank=True)
    potato = models.FloatField("Картофель", default = 0, null=True, blank=True)
    all_whole_crop = models.FloatField("Вся посевная площадь", default = 0, null=True, blank=True)
    vegetables = models.FloatField("Овощи", default = 0, null=True, blank=True)
    f_reports_requests = models.OneToOneField(Reports_Requests, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Справка о размерах посевных площадей"
        verbose_name_plural = "Справки о размерах посевных площадей"

class Analitics_Reports(models.Model):
    f_analitics_reports = models.ForeignKey(Name_Analitics_Reports, verbose_name="Аналитический отчет", on_delete=models.SET_NULL,
                                         null=True)
    excel_document_path = models.FileField(upload_to='analytics_reports/', verbose_name="Excel документ", null=True, blank=True)
    current_year = models.PositiveSmallIntegerField("Текущий год", null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Аналитический отчет"
        verbose_name_plural = "Аналитические отчеты"

class Info_Nesvaska_Calc_Koefficient(models.Model):
    report_year = models.PositiveSmallIntegerField("Отчетный год", null=True, blank=True, unique=True)

    consol_budget_funds_limit = models.FloatField("Лимит средств консолидированного бюджета", default=0)
    budget_funds_limit_cereals = models.FloatField("Лимит средств бюджета (зерно)", default=0)
    budget_funds_limit_vegetables = models.FloatField("Лимит средств бюджета (овощи)", default=0)
    budget_funds_limit_potato = models.FloatField("Лимит средств бюджета (картофель)", default=0)
    insurance = models.FloatField("Страхование", default=0)

    rate_cereals = models.FloatField("Ставка по зерновым", default=0)
    rate_vegetables = models.FloatField("Ставка по овощам", default=0)
    rate_potato = models.FloatField("Ставка по картофелю", default=0)

    auxiliary_1 = models.FloatField("Вспомогательная 1", default=0)
    auxiliary_2 = models.FloatField("Вспомогательная 2", default=0)
    auxiliary_3 = models.FloatField("Вспомогательная 3", default=0)
    auxiliary_4 = models.FloatField("Вспомогательная 4", default=0)

    total_area_vegetables = models.FloatField("Общая площадь по овощам", default=0)
    total_area_cereals = models.FloatField("Общая площадь по зерновым", default=0)
    total_area_potato = models.FloatField("Общая площадь по картофелю", default=0)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Информация для коэффициента по несвязной поддержке"
        verbose_name_plural = "Информация для коэффициента по несвязной поддержке"

class Requests_Nesvaska_Subs(models.Model):
    POINTS_SUBS_1 = (
        ('p1', 'направить по почтовому адресу:'),
        ('p2', 'направить в личный кабинет получателя субсидии'),
        ('p3', 'вручить лично, предварительно оповестив по телефону*;'),
    )
    POINTS_SUBS_2 = (
        ('p1', 'направить по почтовому адресу:'),
        ('p2', 'направить в личный кабинет получателя субсидии'),
        ('p3', 'вручить лично, предварительно оповестив по телефону*;'),
    )

    rate_cereals = models.FloatField("Ставка по зерновым", default=0)
    rate_vegetables = models.FloatField("Ставка по овощам", default=0)
    rate_potato = models.FloatField("Ставка по картофелю", default=0)

    three_point = models.CharField(max_length=30, choices=POINTS_SUBS_1, null=True)
    text_three_point = models.CharField(max_length=70, null=True)

    fourth_point = models.CharField(max_length=30, choices=POINTS_SUBS_2, null=True, blank=True)
    text_fourth_point = models.CharField(max_length=70, null=True, blank=True)

    total_area_vegetables = models.FloatField("Общая площадь по овощам", default=0)
    total_area_cereals = models.FloatField("Общая площадь по зерновым", default=0)
    total_area_oilseeds = models.FloatField("Общая площадь по масличным", default=0)
    total_area_potato = models.FloatField("Общая площадь по картофелю", default=0)
    all_whole_cereals_oilseeds = models.FloatField("Вся посевная площадь", default=0)

    sum_vegetables = models.FloatField("Сумма по овощам", default=0)
    sum_cereals = models.FloatField("Сумма по зерновым", default=0)
    sum_potato = models.FloatField("Сумма по картофелю", default=0)

    f_nesvaska_reports = models.OneToOneField(Nesvaska_Reports, on_delete = models.CASCADE)
    f_requests = models.OneToOneField(Requests, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Заявка по 23 статье"
        verbose_name_plural = "Заявки по 23 статье"

class SSR_Nesvaska_Subs(models.Model):
    f_ssr = models.ForeignKey(SSR, verbose_name="Сводная справка - расчет", on_delete=models.SET_NULL, null=True)
    name_region = models.CharField("Муниципальный округ", max_length=500, null=True, blank=True)
    name_organization = models.CharField("Получатель", max_length=200, null=True, blank=True)

    name_group_cereals = models.CharField("Наименование группы сельскохозяйственных культур зерно", max_length=50, null=True, blank=True)
    name_group_vegetables = models.CharField("Наименование группы сельскохозяйственных культур овощи", max_length=50, null=True, blank=True)
    name_group_potato = models.CharField("Наименование группы сельскохозяйственных культур картошка", max_length=50, null=True, blank=True)

    area_cereals = models.FloatField("Посевная площадь по зерну", default=0, null=True, blank=True)
    area_vegetables = models.FloatField("Посевная площадь по овощам", default=0, null=True, blank=True)
    area_potato = models.FloatField("Посевная площадь по картошке", default=0, null=True, blank=True)

    rate_cereals = models.FloatField("Ставка по зерну", default=0, null=True, blank=True)
    rate_vegetables = models.FloatField("Ставка по овощам", default=0, null=True, blank=True)
    rate_potato = models.FloatField("Ставка по картошке", default=0, null=True, blank=True)

    area_proekt_cereals = models.FloatField("Посевная площадь проектная по зерну", default=0, null=True, blank=True)
    area_proekt_vegetables = models.FloatField("Посевная площадь проектная по овощам", default=0, null=True, blank=True)
    area_proekt_potato = models.FloatField("Посевная площадь проектная по картошке", default=0, null=True, blank=True)

    area_insurance_cereals = models.FloatField("Посевная площадь страхование по зерну", default=0, null=True, blank=True)
    area_insurance_vegetables = models.FloatField("Посевная площадь страхование по овощам", default=0, null=True, blank=True)
    area_insurance_potato = models.FloatField("Посевная площадь страхование по картошке", default=0, null=True, blank=True)

    need_subsidy_cereals = models.FloatField("Потребность субсидии по зерну", default=0, null=True, blank=True)
    need_subsidy_vegetables = models.FloatField("Потребность субсидии по овощам", default=0, null=True, blank=True)
    need_subsidy_potato = models.FloatField("Потребность субсидии по картошке", default=0, null=True, blank=True)

    sum_estimated_cereals_f = models.FloatField("Расчетная сумма субсидии по зерну федерация", default=0, null=True, blank=True)
    sum_estimated_vegetables_f = models.FloatField("Расчетная сумма субсидии по овощам федерация", default=0, null=True, blank=True)
    sum_estimated_potato_f = models.FloatField("Расчетная сумма субсидии по картошке федерация", default=0, null=True, blank=True)

    sum_provided_cereals_f = models.FloatField("Сумма фактически предоставленная с начала года по зерну федерация", default=0, null=True, blank=True)
    sum_provided_vegetables_f = models.FloatField("Сумма фактически предоставленная с начала года по овощам федерация", default=0, null=True, blank=True)
    sum_provided_potato_f = models.FloatField("Сумма фактически предоставленная с начала года по картошке федерация", default=0, null=True, blank=True)

    sum_due_cereals_f = models.FloatField("Сумма причитающаяся к выплате по зерну федерация", default=0, null=True, blank=True)
    sum_due_vegetables_f = models.FloatField("Сумма причитающаяся к выплате по овощам федерация", default=0, null=True, blank=True)
    sum_due_vpotato_f = models.FloatField("Сумма причитающаяся к выплате по картошке федерация", default=0, null=True, blank=True)

    sum_estimated_cereals_k = models.FloatField("Расчетная сумма субсидии по зерну край", default=0, null=True, blank=True)
    sum_estimated_vegetables_k = models.FloatField("Расчетная сумма субсидии по овощам край", default=0, null=True, blank=True)
    sum_estimated_potato_k = models.FloatField("Расчетная сумма субсидии по картошке край", default=0, null=True, blank=True)

    sum_provided_cereals_k = models.FloatField("Сумма фактически предоставленная с начала года по зерну край", default=0, null=True, blank=True)
    sum_provided_vegetables_k = models.FloatField("Сумма фактически предоставленная с начала года по овощам край", default=0, null=True, blank=True)
    sum_provided_potato_k = models.FloatField("Сумма фактически предоставленная с начала года по картошке край", default=0, null=True, blank=True)

    sum_due_cereals_k = models.FloatField("Сумма причитающаяся к выплате по зерну край", default=0, null=True, blank=True)
    sum_due_vegetables_k = models.FloatField("Сумма причитающаяся к выплате по овощам край", default=0, null=True, blank=True)
    sum_due_vpotato_k = models.FloatField("Сумма причитающаяся к выплате по картошке край", default=0, null=True, blank=True)

    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = "Сводная справка - расчет 23 статье"
        verbose_name_plural = "Сводные справки - расчет 23 статье"

class Rate(models.Model):
    name_rate = models.CharField("Наименование показателя", max_length=200, null=True, blank = True)
    value_rate = models.FloatField("Значение ставки", default=0, null=True, blank = True)
    f_subsidies = models.ForeignKey(Subsidies, verbose_name="Статья закона", on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return str(self.id)
    class Meta:
        unique_together = (('name_rate', 'f_subsidies'),)
        verbose_name = "Ставка по субсидии"
        verbose_name_plural = "Ставки по субсидии"


