# Generated by Django 2.2.1 on 2021-03-27 17:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0116_auto_20210326_2130'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('f_subsidies', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='project.Subsidies', verbose_name='Статья закона')),
            ],
            options={
                'verbose_name': 'Ставка',
                'verbose_name_plural': 'Ставки',
            },
        ),
        migrations.CreateModel(
            name='Rate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_article', models.CharField(blank=True, max_length=10, null=True, verbose_name='Номер статьи')),
                ('report_year', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Отчетный год')),
                ('name_rate', models.CharField(blank=True, max_length=200, null=True, unique=True, verbose_name='Наименование показателя')),
                ('value_rate', models.FloatField(blank=True, default=0, null=True, verbose_name='Значение ставки')),
                ('f_rates', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='project.Rates', verbose_name='Ставка')),
            ],
            options={
                'verbose_name': 'Ставка по субсидии',
                'verbose_name_plural': 'Ставка по субсидии',
            },
        ),
    ]
