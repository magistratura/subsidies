# Generated by Django 2.2.1 on 2021-05-16 03:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0123_auto_20210423_0656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationarea',
            name='type_area',
            field=models.CharField(blank=True, choices=[('type1', 'район'), ('type2', 'город')], max_length=30, null=True, verbose_name='Тип района регистрации'),
        ),
    ]
