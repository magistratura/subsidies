# Generated by Django 2.2.1 on 2021-02-15 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0034_delete_requests_law'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ssr',
            name='f_status',
            field=models.CharField(blank=True, choices=[('ss1', 'Черновик'), ('ss2', 'Принято')], max_length=30, null=True, verbose_name='Статус ССР'),
        ),
    ]
