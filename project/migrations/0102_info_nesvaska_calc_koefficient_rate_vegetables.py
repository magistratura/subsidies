# Generated by Django 2.2.1 on 2021-03-23 03:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0101_remove_info_nesvaska_calc_koefficient_rate_oilseeds'),
    ]

    operations = [
        migrations.AddField(
            model_name='info_nesvaska_calc_koefficient',
            name='rate_vegetables',
            field=models.FloatField(default=0, verbose_name='Ставка по овощам'),
        ),
    ]
