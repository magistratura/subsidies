# Generated by Django 2.2.1 on 2021-03-21 13:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0079_nesvaska_reports_current_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='name_reports_requests',
            name='num_article',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Номер статьи'),
        ),
    ]
