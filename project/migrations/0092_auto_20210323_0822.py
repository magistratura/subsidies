# Generated by Django 2.2.1 on 2021-03-23 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0091_analitics_reports'),
    ]

    operations = [
        migrations.CreateModel(
            name='Name_Analitics_Reports',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_analitics_reports', models.CharField(max_length=400, null=True, unique=True, verbose_name='Наименование аналитических отчетов')),
                ('report_year', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Отчетный год')),
                ('num_article', models.CharField(blank=True, max_length=10, null=True, verbose_name='Номер статьи')),
            ],
            options={
                'verbose_name': 'Наименование аналитических отчетов',
                'verbose_name_plural': 'Наименование аналитических отчетов',
            },
        ),
        migrations.RemoveField(
            model_name='analitics_reports',
            name='report_year',
        ),
        migrations.AddField(
            model_name='name_reports_requests',
            name='m_name_analitics_reports',
            field=models.ManyToManyField(blank=True, null=True, to='project.Name_Analitics_Reports'),
        ),
    ]
