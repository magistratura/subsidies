# Generated by Django 2.2.1 on 2021-03-13 08:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0066_specialists_register'),
    ]

    operations = [
        migrations.AddField(
            model_name='requests_eight_subs',
            name='register_specialists',
            field=models.ManyToManyField(to='project.Specialists_register'),
        ),
    ]
