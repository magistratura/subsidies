# Generated by Django 2.2.1 on 2021-03-21 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0074_name_reports_requests_nesvaska_reports'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nesvaska_reports',
            name='vegetables',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='Картофель'),
        ),
    ]
