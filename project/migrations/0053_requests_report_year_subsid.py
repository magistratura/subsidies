# Generated by Django 2.2.1 on 2021-02-28 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0052_auto_20210228_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='requests',
            name='report_year_subsid',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Отчетный год'),
        ),
    ]
