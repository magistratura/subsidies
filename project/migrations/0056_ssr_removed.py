# Generated by Django 2.2.1 on 2021-03-06 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0055_ssr_report_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='ssr',
            name='removed',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
